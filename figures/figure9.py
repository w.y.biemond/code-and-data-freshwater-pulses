#create figure 9

runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water
import matplotlib as mpl

def plotdom(inp_chn):
    #meaning input:
        #inp_p: physical parameters: (Ut, soc, Q, L, bs, H)
        #inp_n: numerical paramters: (dx, N, Lsc)
        #init: initial guess for solutions, in the right format - TO INCLUDE LATER
        #term: which terms of the equation are taken into account
        
    Ln, b0, bs, dxn = inp_chn
    if np.any(Ln%dxn!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln) != len(bs) or len(Ln)!=len(dxn) : print('ERROR: number of domains is not correct')
    
    dln = dxn
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]
    

    #plotting parameters
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    
    #build parameters: convergence length
    bn = np.zeros(len(Ln))
    bn[0] = 9e99 if bs[0] == b0 else Ln[0]/np.log(bs[0]/b0)
    for i in range(1,len(Ln)):
        bn[i] = 9e99 if bs[i] == bs[i-1] else Ln[i]/np.log(bs[i]/bs[i-1])
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))
    print('The maximum width  is ' , int(np.round(np.max(b))), 'm')
    
    px=px+Ln[-1]/1000
    
    #plot width
    fig,ax = plt.subplots(figsize = (7,4))
    
    #Mc07 domain
    '''
    ax.plot(np.linspace(-110,0,101),150*np.exp((np.linspace(-110,0,101)+110)/75)/2000,'red')
    ax.plot(np.linspace(-110,0,101),-150*np.exp((np.linspace(-110,0,101)+110)/75)/2000,'red',label = 'MC07')
    print(np.max(150*np.exp((np.linspace(-110,0,101)+110)/75)))
    '''
    #my model
    ax.plot(px[:di[-2]],b[:di[-2]]/2000,'black',label = 'BI22')
    ax.plot(px[:di[-2]],-b[:di[-2]]/2000,'black')
    ax.plot(px[di[-2]:di[-1]],b[di[-2]:di[-1]]/2000,'black')
    ax.plot(px[di[-2]:di[-1]],-b[di[-2]:di[-1]]/2000,'black')
    for x in range(len(di)-1):  plt.plot(np.linspace(px[di[x]-1],px[di[x]-1],101) ,np.linspace(-b[di[x]-1]/2000,b[di[x]-1]/2000,101) , linestyle='dashed' , c= 'gray'  )
    plt.plot(np.linspace(-140,-140,101) ,np.linspace(-0.5,0.5,101)  , linestyle='dashed' , c= 'gray' )
    #ax.legend()

    
    #ax.axes.get_yaxis().set_visible(False)
    #ax.spines["right"]. set_visible(False) , ax.spines["top"]. set_visible(False)#, ax.spines["left"]. set_visible(False)
    ax.set_xlabel('$x$ [km]'), ax.set_ylabel('$y$ [km]')
    ax.set_xlim(-150,11),ax.set_ylim(-5,5)
    #plt.xticks([]) , plt.yticks([])
    '''
    #Grionde
    ax.vlines(px[di[2]],-10,-b[di[2]]/2000)
    ax.text(-125,-10,'Dordogne',c='C0')
    #'''
    #'''
    #Guadalquivir
    ax.vlines(px[di[2]],-1.4,-b[di[2]]/2000)
    ax.text(px[di[2]]+2,-1.4,'El Gergal',c='C0')
    ax.vlines(px[di[3]],1.4,b[di[3]]/2000)
    ax.text(px[di[3]]+2,1.4,'Guadaíra',c='C0')
    ax.vlines(px[di[4]],-1.4,-b[di[4]]/2000)
    ax.text(px[di[4]]+2,-1.4,'Torre del Águila',c='C0')
    ax.vlines(px[di[5]],1.4,b[di[5]]/2000)
    ax.text(px[di[5]]+2,1.4,'Aznalcázar',c='C0')
    ax.vlines(-110,0.15/2,-0.15/2,color='navy',linewidth = 4)
    ax.text(-118,-0.55,'Alcalá del \n Rio dam',c='navy')
    ax.set_xlim(-120,11),ax.set_ylim(-2,2)
    #'''
    #plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Verslagen voor extern/Paper 1 - volwassen/figs/dom_exp3.jpg', dpi=300)



    # read image file
    with mpl.cbook.get_sample_data(r"/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs/map_inset_gua.png") as file:
        arr_image = plt.imread(file, format='png')
    
    # Draw image
    axin = ax.inset_axes([-118,-0.37,35,3],transform=ax.transData)    # create new inset axes in data coordinates
    axin.imshow(arr_image)
    axin.set_xticks([]) , axin.set_yticks([])


    #ax.axis('off')
    #xaxis.set_visible(False)
    plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/dom_Gua5.jpg', dpi=300)
    plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/dom_Gua5.jpg', dpi=1200)
    plt.show()
    
    
    #plt.vlines(-10,-10000,10000,'orange')
    #plt.vlines(-110,-10000,10000,'m')
    #plt.vlines(0,-10000,10000,'crimson')
    #plt.text(-120,20000,' river \n Q and s prescribed',c='m')
    
    return



#Guadalquivir - simple
Ln = np.array([100000,12100,18500,10800,30300,17800,4000,25000])
b0 = 150
bs = np.array([150,190,300,450,500,800,3000,3000*np.exp(10)])
dxn = np.array([1000,484,500,540,300,356,250,500]) #quite rude
plotdom((Ln, b0, bs, dxn))