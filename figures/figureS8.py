#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 11:06:55 2022

@author: biemo004
"""

#create figure 1


runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

# =============================================================================
# fig 1
# =============================================================================

dat_mc = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/'+str('MC07 comp/')+ str('guadal1.txt'))
T2b,nx2b,nz2b = 100,441,21
dat_mc = np.reshape(dat_mc,(T2b*4,nx2b,nz2b))

#load gen 2model
fold, run = 'Guadalquivir' ,112 #124
#load files
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/raw_out.txt')

#extract paramaters
Ut, soc, sri, Q, sf, u_w, d_w, = par_v #Av, Kv #deze laatste termen alleen als laatste fashion
b0, H, N, Lsc,  dt, T = par_c
#b0, H, N, Lsc = par_c
Ln, bn, dxn = par_nd
T=len(time)

#rebuild x-coordinates
nxn = np.array(Ln/dxn+1,dtype=int)
M,T,nx = int(N+1) , int(T) , np.sum(nxn)-1
nz=20 #vertical step - only for plot
px = np.zeros(np.sum(nxn))
px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
px=px/1000
s_min =np.zeros(T)
L_int, salt = np.zeros(T), np.zeros((T, np.sum(nxn),nz+1))


for j in range(T):
    sss=outp[j]
   
    s_b = np.transpose([np.reshape(sss,(nx+1,M))[:,0]]*(nz+1))
    
    sn = np.reshape(sss,(nx+1,M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for k in range(nx+1)])
    s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    s_min[j] = np.min(s)
    salt[j]=s

Li_r1=L_int
Qq_r1=Q
Tt_r1=time/24
Uu_r1=Ut
    
print('run ', run, ' is loaded')
print(np.min(s_min))

fig,ax = plt.subplots(3,1,figsize=(6,9))

#calculate stratification
Cv = 0.000125
strat = salt[:,:,0] - salt[:,:,-1]
strat[np.where((strat<0) & (strat>-1e-5))] = 0 #remove numerical effects
Ri = g*Be*strat*H/(Ut[:,np.newaxis]**2)
Av = Cv*Ut[:,np.newaxis]*H*(1+10*Ri)**(-1/2)
Kv = Cv*Ut[:,np.newaxis]*H*(1+10/3*Ri)**(-3/2)

ind_mouth = np.where(px==-25.0)[0][0]
fig,ax = plt.subplots(4,1,figsize = (6,10))

#at the mouth
ax[0].plot(Tt_r1-16,salt[:,ind_mouth,0],':',c='b',label='bottom')
ax[0].plot(Tt_r1-16,salt[:,ind_mouth,-1],'--',c='b',label='surface')
ax[0].plot(Tt_r1-16,strat[:,ind_mouth],'b',label='difference')
ax[1].plot(Tt_r1-16,Ri[:,ind_mouth],'b')
ax[2].plot(Tt_r1-16,Av[:,ind_mouth],'b')
ax[3].plot(Tt_r1-16,Kv[:,ind_mouth],'b')

#values used in the model
ax[2].plot(Tt_r1-16,cv*Ut*H,'green')
ax[3].plot(Tt_r1-16,cv*Ut*H/2.2,'g')


ax[0].set_ylim(0,35)
for i in range(4):
    ax[i].grid()
    ax[i].set_xlim(-16,Tt_r1[-1]-16)
    if i!=3:ax[i].xaxis.set_ticklabels([])
    ax[i].tick_params(axis='both', which='major', labelsize=14) 

ax[2].ticklabel_format(style='sci', axis='y',scilimits=(0,0))
ax[2].yaxis.major.formatter._useMathText = True
ax[3].ticklabel_format(style='sci', axis='y',scilimits=(0,0))
ax[3].yaxis.major.formatter._useMathText = True

ax[i].set_xlabel('Time [doy 2009]',fontsize=15)

ax[0].set_ylabel('$s$ [psu]',fontsize=15)
ax[1].set_ylabel('$Ri_L$ ',fontsize=15)
ax[2].set_ylabel('$A_v$ [m$^2$/s]',fontsize=15)
ax[3].set_ylabel('$K_v$ [m$^2$/s]',fontsize=15)

ax[0].text(-15,31,'(a)',fontsize=14)
ax[1].text(-15,1.05,'(b)',fontsize=14)
ax[2].text(-15,0.00059,'(c)',fontsize=14)
ax[3].text(-15,0.00048,'(d)',fontsize=14)

#ax[0].legend(loc=2)
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/'+str('strat_1loc')+'.jpg',bbox_inches='tight', dpi=300)
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/'+str('strat_1loc')+'.jpg',bbox_inches='tight', dpi=1200)

plt.show()

