#create figure 7
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water


def transport(par_v, par_c, par_nd,xloc,ds,de):
    Ut, soc, sri, Q, sf, u_w, d_w= par_v
    b0, H, N, Lsc, dt, T = par_c
    Ln, bn, dxn = par_nd

    DT=(np.sum(Ln)*H*b0*dt)/(3600*Q[0])
    u_w, d_w = 0,0
    
    dln = dxn/Lsc
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]
    
    N= int(N)
    M=N+1 
    #plotting parameters
    nz=50 #vertical step - only for plot
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    pz = np.linspace(-H,0,nz+1)
    
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))

    #time vector
    ts,te = np.where(time/24==min(time/24, key=lambda x:abs(x-ds)))[0][0] , np.where(time/24==min(time/24, key=lambda x:abs(x-de)))[0][0]

    #lists of ks and ns and pis
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    z= np.linspace(-H,0,101)
    USB,USP,KH = np.zeros(te-ts),np.zeros(te-ts),np.zeros(te-ts)
    Lint = np.zeros(te-ts)
    xind = np.where(px==-25)[0][0]-1
    
    for t in range(ts,te):
        #build parameters
        u_bar = Q[t]/(H*b)
        Kh = ch*Ut[t]*b
        Av = cv*Ut[t]*np.array([H]*np.sum(nxn))
        Kv = Av/Sc
        alf = g*Be*H**3/(48*Av)
        
        #partial slip coefficients
        g1 = -1 + (1.5+3*Av/(sf[t]*H)) / (1+ 3 *Av/(sf[t]*H))
        g2 =  -3 / (2+6*Av/(sf[t]*H))
        g3 = (1+4*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H)) * (9+18*Av/(sf[t]*H)) - 8 - 24*Av/(sf[t]*H)
        g4 = -9 * (1+4*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H))
        g5 = - 8
        g6 = 4+4*Av/(sf[t]*H) -12*(0.5+Av/(sf[t]*H))**2/(1+3*Av/(sf[t]*H))
        g7 = 4
        g8 = (3+6*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H))
        
        sb = np.transpose(np.reshape(outp[t],(np.sum(nxn),M))[:,0])*soc[t]
        sn = np.reshape(outp[t],(np.sum(nxn),M))[:,1:]*soc[t]

        s_b = np.transpose([np.reshape(outp[t],(np.sum(nxn),M))[:,0]]*len(z))
        s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*z) for n in range(1,M)],0) for k in range(np.sum(nxn))])
        s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
        #salt intrusion length
        Lint[t] = px[np.where(s[:,0]>2)[0][0]]+Ln[-1]/1000

    
        term_build = {
            'usb': lambda sb, sn, x, z2 : b0*H*u_bar[x] * sb[x],
            'usp': lambda sb, sn, x, z2 : b0*H* np.mean( (u_bar[x]*(g1[x]+g2[x]*(z/H)**2) + alf[x] * (sb[x+1]-sb[x-1])/(2*dx[x]) * (g3[x] + g4[x]*(z/H)**2 + g5*(z/H)**3)) *
                                          np.sum([ sn[x,n] *np.cos(nnp[n]*z/H) for n in range(N)],0)),
            'kh': lambda sb, sn, x, z2 : -b0*H*Kh[x] * (sb[x+1]-sb[x-1])/(2*dx[x])
                        }
        
        dx=dl*Lsc
        USB[t-ts], USP[t-ts], KH[t-ts] = term_build['usb'](sb,sn, xind,0), term_build['usp'](sb,sn, xind, 0), term_build['kh'](sb,sn,xind, 0)

    tt = time[ts:te]/24
    
    print('The value of T_sa is', 0.014 * (Q[0]/(b[0]*H*np.sqrt(g*Be*H*soc[0])) )**(-4/3) * H**2/(cv*Ut[0]*H) /3600/24 , 'days' )
    print('The value of T_sp is', 0.083 * H**2/(cv*Ut[0]*H) /3600/24 , 'days' )
    
    fig,ax= plt.subplots(figsize=(8,5))
    ax.plot(tt,-USB,label=r'$\bar u \bar s$',linewidth=2)
    ax.plot(tt,-USP,label=r'$u^{\prime} s^{\prime}$',linewidth=2)
    ax.plot(tt,-KH,label='Kh',linewidth=2)
    ax.plot(tt,-KH+-USP+-USB,label='sum',linewidth=2)
    #ax.set_ylim(-0.00005,0.00005)
    ax.legend()
    #ax2 = ax.twinx()
    #ax2.plot(np.arange(0,25,0.25),Q[:100],'--',c='black',linewidth=2)
    ax.grid(), ax.set_xlim(tt[0],tt[-1])
    ax.set_xlabel('Time [days]',fontsize=13), ax.set_ylabel('Salt transport ',fontsize=13)#,ax2.set_ylabel('Q [m$^3$ s$^{-1}$]')
    #ax.set_ylim(0,0.00002)
    plt.tight_layout()
    
    #plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Verslagen voor extern/Paper 1 - ruw/figs/HDE_sbar.png', dpi=1200)
    plt.show()
    
    #print(np.max(-KH+-USP+-USB) , USB[0]-USB[-1])
    return tt,-USB ,-USP,-KH, Q[ts:te], Lint

run,fold = 20 , 'defi_insight'

par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/raw_out.txt')

term4 = transport(par_v, par_c, par_nd,-25.5,0,250)

run,fold = 21, 'defi_insight'

par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/raw_out.txt')

term5 = transport(par_v, par_c, par_nd,-25.5,0,250)

run,fold = 22 , 'defi_insight'

par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run)+'/raw_out.txt')

term6 = transport(par_v, par_c, par_nd,-25.5,0,250)

import matplotlib.patches as mpatches

#plot
fig,ax= plt.subplots(2,1,figsize=(8,7))
font0,font1,font2 = 13,14,18

#plot 0,0
ax[0].plot(term4[0]-10,term4[4],'black',linewidth=1.5)
ax2 = ax[0].twinx()
ax2.plot(term4[0]-10,-term4[5],'b',linewidth=1.5,label='$Q_p=1000$ m$^3$ s$^{-1}$')
#ax2.plot(term5[0]-10,-term5[5],'r',linewidth=1.5,label='$Q_p=2000$ m$^3$ s$^{-1}$')
ax2.plot(term6[0]-10,-term6[5],'r',linewidth=1.5,label='$Q_p=4000$ m$^3$ s$^{-1}$')
ax[0].grid(),ax[0].set_xlim(-5,30) 
ax2.set_ylabel('$X_2$ [km]',fontsize=font2),ax[0].set_ylabel('Q [m$^3$ s$^{-1}$]',fontsize=font2)

#ax[0].tick_params(axis='y', colors='black'),ax2.tick_params(axis='y', colors='darkblue')
#ax[0].yaxis.label.set_color('black'), ax2.yaxis.label.set_color('darkblue')
ax[0].xaxis.set_ticklabels([])

ax2.legend(loc=4,fontsize=font1)


#plot 1,0
ax[1].plot(term4[0]-10,term4[1],label=r'$\bar u \bar s$',c='b',linewidth=1.5,ls='dashed')
ax[1].plot(term4[0]-10,term4[2],label=r'$u^{\prime} s^{\prime}$',c='b',linewidth=1.5,ls='dotted')
ax[1].plot(term4[0]-10,term4[3],label='Kh',c='m',linewidth=1.5,ls='dashdot')
#ax[1].plot(term4[0]-10,term4[1]+term4[2]+term4[3],label='sum',c='b',linewidth=1.5)

#ax[1].plot(term5[0]-10,term5[1],label=r'$\bar u \bar s$',c='r',linewidth=1.5,ls='dashed')
#ax[1].plot(term5[0]-10,term5[2],label=r'$u^{\prime} s^{\prime}$',c='r',linewidth=1.5,ls='dotted')
#ax[1].plot(term5[0]-10,term5[3],label='Kh',c='r',linewidth=1.5,ls='dashdot')
#ax[1].plot(term5[0]-10,term5[1]+term5[2]+term5[3],label='sum',c='r',linewidth=1.5)

ax[1].plot(term6[0]-10,term6[1],label=r'$\bar u \bar s$',c='r',linewidth=1.5,ls='dashed')
ax[1].plot(term6[0]-10,term6[2],label=r'$u^{\prime} s^{\prime}$',c='r',linewidth=1.5,ls='dotted')
ax[1].plot(term6[0]-10,term6[3],label='Kh',c='r',linewidth=1.5,ls='dashdot')
#ax[1].plot(term6[0]-10,term6[1]+term6[2]+term6[3],label='sum',c='g',linewidth=1.5)

ax[1].grid(), ax[1].set_xlim(-5,30)
ax[1].set_xlabel('Time [days]',fontsize=font2), ax[1].set_ylabel('Salt transport [kg s$^{-1}]$ ',fontsize=font2)#,ax[1,1]2.set_ylabel('Q [m$^3$ s$^{-1}$]')

plt.tight_layout()

#'''
# legend
#marker = ['solid','dashed', 'dotted', 'dashdot',]
#label_column = ['sum',r'$\bar{u} \bar{s}$',r'$\overline{u^\prime s^\prime}$',r'$K_h \frac{\partial \bar{s}}{\partial x}$',]
#label_column = ['$S_1$','$S_2$','$S_3$','$S_4$']
marker = ['dashed', 'dotted', 'dashdot',]
label_column = ['$S_2$','$S_3$','$S_4$']

columns = [ax[1].plot([], [], ls=marker[i],c='black')[0] for i in range(3)]
ax[1].legend(columns,  label_column, loc=4,fontsize=font1)
#ax[1,1].legend(columns,  label_column, loc=4)
#'''
#ig.text(0.065,0.93,'(a)',fontsize=15),fig.text(0.565,0.93,'(c)',fontsize=15)
#fig.text(0.065,0.47,'(b)',fontsize=15),fig.text(0.565,0.47,'(d)',fontsize=15)
ax[0].text(-4.5,950,'(a)',fontsize=font2), ax[1].text(-4.5,0.72e5,'(b)',fontsize=font2)

ax[0].yaxis.set_tick_params(labelsize=font1), ax2.yaxis.set_tick_params(labelsize=12.5),ax[1].yaxis.set_tick_params(labelsize=font1)
ax[1].xaxis.set_tick_params(labelsize=font1)
ax[1].ticklabel_format(style='sci', axis='y',scilimits=(0,0))
ax[1].yaxis.major.formatter._useMathText = True
plt.tight_layout()
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/hterm_pe5.jpg', dpi=300)
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/hterm_pe5.jpg', dpi=1200)
plt.show()