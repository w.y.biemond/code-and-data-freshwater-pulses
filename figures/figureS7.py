#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 11:06:55 2022

@author: biemo004
"""

#create figure 1


runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

# =============================================================================
# fig 1
# =============================================================================

dat_mc = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/'+str('MC07 comp/')+ str('guadal1.txt'))
T2b,nx2b,nz2b = 100,441,21
dat_mc = np.reshape(dat_mc,(T2b*4,nx2b,nz2b))

#load gen 2model
fold, run = 'Guadalquivir' ,112 #124
#load files
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/raw_out.txt')

#extract paramaters
Ut, soc, sri, Q, sf, u_w, d_w, = par_v #Av, Kv #deze laatste termen alleen als laatste fashion
b0, H, N, Lsc,  dt, T = par_c
#b0, H, N, Lsc = par_c
Ln, bn, dxn = par_nd
T=len(time)

#rebuild x-coordinates
nxn = np.array(Ln/dxn+1,dtype=int)
M,T,nx = int(N+1) , int(T) , np.sum(nxn)-1
nz=20 #vertical step - only for plot
px = np.zeros(np.sum(nxn))
px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
px=px/1000
s_min =np.zeros(T)
L_int, salt = np.zeros(T), np.zeros((T, np.sum(nxn),nz+1))


for j in range(T):
    sss=outp[j]
   
    s_b = np.transpose([np.reshape(sss,(nx+1,M))[:,0]]*(nz+1))
    
    sn = np.reshape(sss,(nx+1,M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for k in range(nx+1)])
    s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    s_min[j] = np.min(s)
    salt[j]=s

Li_r1=L_int
Qq_r1=Q
Tt_r1=time/24
Uu_r1=Ut
    
print('run ', run, ' is loaded')
print(np.min(s_min))

#calculate stratification
Cv = 0.000125
strat = salt[:,:,0] - salt[:,:,-1]
strat[np.where((strat<0) & (strat>-1e-5))] = 0 #remove numerical effects
Ri = g*Be*strat*H/(Ut[:,np.newaxis]**2)
Av = Cv*Ut[:,np.newaxis]*H*(1+10*Ri)**(-1/2)
Kv = Cv*Ut[:,np.newaxis]*H*(1+10/3*Ri)**(-3/2)

#make figure
fig,ax = plt.subplots(4,1,figsize = (8,11))
a0 = ax[0].contourf(Tt_r1-16,px+25,strat.T,cmap='Spectral_r',levels=np.linspace(0,30,16))
a1 = ax[1].contourf(Tt_r1-16,px+25,Ri.T,cmap='Spectral_r',levels=np.linspace(0,1.25,21))
a2 = ax[2].contourf(Tt_r1-16,px+25,Av.T,cmap='Spectral_r',levels=np.linspace(0,0.0011,12))
a3 = ax[3].contourf(Tt_r1-16,px+25,Kv.T,cmap='Spectral_r',levels=np.linspace(0,0.0011,12))

cb0 = fig.colorbar(a0, ax=ax[0],orientation='vertical')
cb0.set_label(label='$\Delta s$ [psu]',fontsize=17)
cb0.ax.tick_params(labelsize=14)
#cb3.formatter.set_powerlimits((0, 0))

cb1 = fig.colorbar(a1, ax=ax[1],orientation='vertical')
cb1.set_label(label='$Ri_L$ ',fontsize=17)
cb1.ax.tick_params(labelsize=14)
cb1._get_ticker_locator_formatter()[0].set_params(nbins=6)
cb1.update_ticks()

cb2 = fig.colorbar(a2, ax=ax[2],orientation='vertical')
cb2.set_label(label='$A_v$ [m$^2$/s]',fontsize=17)
cb2.ax.tick_params(labelsize=14)
cb2.formatter.set_powerlimits((0, 0))
cb2.formatter.set_useMathText(True)
cb2.update_ticks()


cb3 = fig.colorbar(a3, ax=ax[3],orientation='vertical')
cb3.set_label(label='$K_v$ [m$^2$/s]',fontsize=17)
cb3.ax.tick_params(labelsize=14)
cb3.formatter.set_powerlimits((0, 0))
cb3.formatter.set_useMathText(True)
cb3.update_ticks()


for i in range(4): 
    ax[i].set_ylim(-50,0)
    ax[i].set_ylabel('x [km]',fontsize=17) 
    if i!=3:ax[i].xaxis.set_ticklabels([])
    ax[i].tick_params(axis='both', which='major', labelsize=14) 

ax[i].set_xlabel('Time [doy 2009]',fontsize=17)

ax[0].text(-14,-6,'(a)',fontsize=17)
ax[1].text(-14,-6,'(b)',fontsize=17)
ax[2].text(-14,-6,'(c)',fontsize=17)
ax[3].text(-14,-6,'(d)',fontsize=17)

plt.tight_layout()
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/'+str('strat_allloc')+'.jpg',bbox_inches='tight', dpi=300)
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/'+str('strat_allloc')+'.jpg',bbox_inches='tight', dpi=1200)



