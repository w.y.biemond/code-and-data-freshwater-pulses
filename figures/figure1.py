#create figure 1


runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

# =============================================================================
# fig 1
# =============================================================================

dat_mc = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/'+str('MC07 comp/')+ str('guadal1.txt'))
T2b,nx2b,nz2b = 100,441,21
dat_mc = np.reshape(dat_mc,(T2b*4,nx2b,nz2b))

#load gen 2model
fold, run = 'Guadalquivir' ,112
#load files
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/raw_out.txt')

#extract paramaters
Ut, soc, sri, Q, sf, u_w, d_w= par_v
b0, H, N, Lsc,  dt, T = par_c
#b0, H, N, Lsc = par_c
Ln, bn, dxn = par_nd
T=len(time)

#rebuild x-coordinates
nxn = np.array(Ln/dxn+1,dtype=int)
M,T,nx = int(N+1) , int(T) , np.sum(nxn)-1
nz=20 #vertical step - only for plot
px = np.zeros(np.sum(nxn))
px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
px=px/1000
s_min =np.zeros(T)
L_int, salt = np.zeros(T), np.zeros((T, np.sum(nxn),nz+1))


for j in range(T):
    sss=outp[j]
   
    s_b = np.transpose([np.reshape(sss,(nx+1,M))[:,0]]*(nz+1))
    
    sn = np.reshape(sss,(nx+1,M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for k in range(nx+1)])
    s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    s_min[j] = np.min(s)
    salt[j]=s

Li_r1=L_int
Qq_r1=Q
Tt_r1=time/24
Uu_r1=Ut
    
print('run ', run, ' is loaded')
print(np.min(s_min))

fig,ax = plt.subplots(3,1,figsize=(6,9))

a0 = ax[0].plot(time/24-15,par_v[3],'black',linewidth = 1.5)
ax[0].grid(), ax[0].set_xlim(-15,84)
ax[0].set_ylabel('$Q$ [m$^3$ s$^{-1}$]',fontsize=15)
ax[0].xaxis.set_ticklabels([]),ax[1].xaxis.set_ticklabels([])
ax[0].tick_params(axis='both', which='major', labelsize=13),ax[1].tick_params(axis='both', which='major', labelsize=13),ax[2].tick_params(axis='both', which='major', labelsize=13)
#a2 = ax[0].twinx()
#a2.plot(ti-365,X2_meas,'navy',linewidth = 1.5,label='obs')
#a2.plot(np.linspace(0,T2b,dat_mc.shape[0])-15,-Li_mc,'dodgerblue',linewidth = 1.5,label='MC07')#, linestyle='dotted')

#ax[0].tick_params(axis='y', colors='red')#,a2.tick_params(axis='y', colors='b')
#ax[0].yaxis.label.set_color('red')#, a2.yaxis.label.set_color('b')
#a2.legend(loc=3), a2.set_ylim(10,65) ,a2.set_ylabel('$X_2$ [km]',fontsize=13)

a1=ax[1].contourf(np.linspace(0,T2b,dat_mc.shape[0])-16,np.linspace(-110,0,dat_mc.shape[1]),dat_mc[:,:,0].transpose(),cmap='Spectral_r',levels = np.linspace(0,35,15)) 
a2=ax[2].contourf(Tt_r1-16, px+25, salt[:,:,-1].transpose(),cmap='Spectral_r',levels = np.linspace(0,35,15))
ax[1].set_ylim(-50,0) , ax[2].set_ylim(-50,0) 
ax[1].set_ylabel('x [km]',fontsize=15) ,ax[2].set_ylabel('x [km]',fontsize=15) , ax[2].set_xlabel('Time [doy 2009]',fontsize=15)


fig.subplots_adjust(right=0.95)
cbar_ax = fig.add_axes([1, 0.391, 0.025, 0.277])
cb=fig.colorbar(a1, cax=cbar_ax,orientation='vertical')
cb.set_label(label='$s_{sur}$ [psu]',fontsize=15)    
cb.ax.tick_params(labelsize=13)

cbar_ax = fig.add_axes([1, 0.076, 0.025, 0.277])
cb=fig.colorbar(a2, cax=cbar_ax,orientation='vertical')
cb.set_label(label='$s_{sur}$ [psu]',fontsize=15)  
cb.ax.tick_params(labelsize=13)

ax[0].text(-14,830,'(a)',fontsize=14) , ax[1].text(-14,-5,'(b)',fontsize=14) , ax[2].text(-14,-5,'(c)',fontsize=14)

plt.tight_layout()
#plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/'+str('MC07_all3')+'.jpg',bbox_inches='tight', dpi=300)
#plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/'+str('MC07_all3')+'.jpg',bbox_inches='tight', dpi=1200)

plt.show()







