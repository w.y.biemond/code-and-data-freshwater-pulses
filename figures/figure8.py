#create figure 8
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

#load data
outall05 = []
run0,fold = 0 , 'defi1_05adj'
for i in range(141):
    outall05.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))
                  
outall025 = []
run0,fold = 0 , 'defi1_025adj'
for i in range(141):
    outall025.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))
#calculate quantities
def calc_numbers(Ti, Li,Ui):
    L_min, L_max = np.min(Li) , np.max(Li)
    #print(L_min, L_max)
    #adjustment time
    Ta = Ti[np.where(Li<L_min+(1-0.9)*(L_max-L_min))[0][0]] - Ti[np.where(Ui>Ui[0])[0][0]]
    #change in salt intrusion length
    dL = L_max - L_min
    #recovery time
    Tr = Ti[np.where(Li<L_max-(1-0.9)*(L_max - L_min))[0][-1]] - Ti[np.where(Li == L_min)[0][-1]] 
    #print(Ti[np.where(Li<L_max-(1-0.5)*(L_max - L_min))[0][-1]],Ti[-1],Tr)

    #inital guess scale
    #adjustment time
    Ta_sc = dL*900/np.max(Ui)/3600/24
    #salt intrusion length
    dL_sc = L_max
    #recovery time
    Tr_sc = L_max*900/np.min(Ui)/3600/24
    
    #print(Tr,Tr_sc)
    
    #return  Ta,dL,Tr, 
    return Ta/Ta_sc , dL/dL_sc , Tr/Tr_sc , Ta, dL, Tr #, L_max


FrRp,FrRbc,FrT = np.zeros(len(outall05)),np.zeros(len(outall05)),np.zeros(len(outall05))
for i in range(len(outall05)):
    FrRp[i] = np.max(outall05[i][0][3])/(outall05[i][1][0]*outall05[i][1][1]*np.sqrt(9.81*7.6e-4*outall05[i][1][1]*outall05[i][0][1][0]))
    FrRbc[i] = np.min(outall05[i][0][3])/(outall05[i][1][0]*outall05[i][1][1]*np.sqrt(9.81*7.6e-4*outall05[i][1][1]*outall05[i][0][1][0]))
    FrT[i] = outall05[i][0][0][0]/np.sqrt(9.81*7.6e-4*outall05[i][1][1]*outall05[i][0][1][0])

Ta05_s, dLr05_s, Tr05_s , Ta05_r, dLr05_r, Tr05_r = np.zeros(len(outall05)),np.zeros(len(outall05)),np.zeros(len(outall05)),np.zeros(len(outall05)),np.zeros(len(outall05)),np.zeros(len(outall05))
Ta025_s, dLr025_s, Tr025_s , Ta025_r, dLr025_r, Tr025_r = np.zeros(len(outall025)),np.zeros(len(outall025)),np.zeros(len(outall025)),np.zeros(len(outall025)),np.zeros(len(outall025)),np.zeros(len(outall025))

for i in range(len(outall05)):
    Ta05_s[i],dLr05_s[i],Tr05_s[i], Ta05_r[i],dLr05_r[i],Tr05_r[i] = calc_numbers(outall05[i][3]/24,-outall05[i][4],outall05[i][0][3]/(outall05[i][1][0]*outall05[i][1][1]))
for i in range(len(outall025)):
    Ta025_s[i],dLr025_s[i],Tr025_s[i], Ta025_r[i],dLr025_r[i],Tr025_r[i]  = calc_numbers(outall025[i][3]/24,-outall025[i][4],outall025[i][0][3]/(outall025[i][1][0]*outall025[i][1][1]))

#okay do another interpolation
FrRbc_u = list(dict.fromkeys(FrRbc))
Ta05_si, dLr05_si, Tr05_si = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
Ta05_ri, dLr05_ri, Tr05_ri = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
Ta025_si, dLr025_si, Tr025_si = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
Ta025_ri, dLr025_ri, Tr025_ri = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
for i in range(len(FrRbc_u)):
    ind = np.where(FrRbc == FrRbc_u[i])[0]
    Ta05_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta05_s[ind])
    dLr05_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dLr05_s[ind])
    Tr05_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr05_s[ind])
    Ta05_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta05_r[ind])
    dLr05_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dLr05_r[ind])
    Tr05_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr05_r[ind])
    
    Ta025_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta025_s[ind])
    dLr025_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dLr025_s[ind])
    Tr025_si[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr025_s[ind])
    Ta025_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta025_r[ind])
    dLr025_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dLr025_r[ind])
    Tr025_ri[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr025_r[ind])

#plot
fig,ax = plt.subplots(1,4,figsize=(15,5))

a0=ax[0].contourf(np.linspace(0,0.35,101), FrRbc_u, dLr05_si ,cmap='Spectral_r',levels = np.linspace(0,0.5,11))
a1=ax[1].contourf(np.linspace(0,0.35,101), FrRbc_u, dLr025_si ,cmap='Spectral_r',levels = np.linspace(0,0.5,11))
a2=ax[2].contourf(np.linspace(0,0.35,101), FrRbc_u, Tr05_si ,cmap='Spectral_r',levels = np.linspace(0.3,1,15))
a3=ax[3].contourf(np.linspace(0,0.35,101), FrRbc_u, Tr025_si ,cmap='Spectral_r',levels = np.linspace(0.3,1,15))

c0 = ax[0].contour(np.linspace(0,0.35,101), FrRbc_u, dLr05_ri ,colors='black',levels = [10,20,30,50,75])
c1 =ax[1].contour(np.linspace(0,0.35,101), FrRbc_u, dLr025_ri ,colors='black',levels = [5,10,20,30,40])
c2 =ax[2].contour(np.linspace(0,0.35,101), FrRbc_u, Tr05_ri ,colors='black',levels = [2,5,10,20,100])
c3 =ax[3].contour(np.linspace(0,0.35,101), FrRbc_u, Tr025_ri  ,colors='black',levels = [2,5,10,20,100])

#a01=ax[0].contour(np.reshape(FrR[:110,t0],sh),np.reshape(FrR[:110,t1],sh),np.reshape(dLr_a[:110],sh),colors='black',levels=np.linspace(0.25,0.95,15))#,levels = np.linspace(0.25,0.95,15))

ax[0].set_xlabel(r'$Fr_{R,p}$ ',fontsize=20),ax[0].set_ylabel(r'$Fr_{R,bg}$ ',fontsize=20),ax[2].set_xlabel(r'$Fr_{R,p}$ ',fontsize=20),ax[1].set_xlabel(r'$Fr_{R,p}$ ',fontsize=20)
ax[3].set_xlabel(r'$Fr_{R,p}$ ',fontsize=20)
for i in range(4): ax[i].tick_params(axis='both', which='major', labelsize=12)

#ax[0].clabel(a0, inline=True, fontsize=10)#,ax[1].clabel(a11, inline=True, fontsize=10)
cb0 = fig.colorbar(a0, ax=ax[0],orientation='horizontal', pad=0.16)
cb0.set_label(label='$\Delta X_2/X_{2}(t=0)$',fontsize=20)
cb0.ax.tick_params(labelsize=14)
cb1 = fig.colorbar(a1, ax=ax[1],orientation='horizontal', pad=0.16)
cb1.set_label(label='$\Delta X_2/X_{2}(t=0)$',fontsize=20)
cb1.ax.tick_params(labelsize=14)
cb2 = fig.colorbar(a2, ax=ax[2],orientation='horizontal', pad=0.16)
cb2.set_label(label='$T_{rec}/T_{rec,sc}$',fontsize=20)
cb2.ax.tick_params(labelsize=14)
cb3 = fig.colorbar(a3, ax=ax[3],orientation='horizontal', pad=0.16)
cb3.set_label(label='$T_{rec}/T_{rec,sc}$',fontsize=20)
cb3.ax.tick_params(labelsize=14)
#THIS PART HAS TO BE IMPROVED SOMEWHAT!
# 
ax[0].clabel(c0, inline=True, fontsize=14,fmt='%1.1f',manual = [(0.12,0.02),(0.25,0.02),(0.2,0.005),(0.27,0.001)] )
ax[1].clabel(c1, inline=True, fontsize=14,fmt='%1.1f',manual = [(0.12,0.01),(0.22,0.03),(0.24,0.005)])
ax[2].clabel(c2, inline=True, fontsize=14,fmt='%1.1f', manual = [(0.25,0.06),(0.2,0.03),(0.15,0.02),(0.2,0.01),(0.15,0.001)])
ax[3].clabel(c3, inline=True, fontsize=14,fmt='%1.1f', manual = [(0.25,0.06),(0.2,0.03),(0.15,0.02),(0.2,0.01),(0.15,0.001)])

#manual_locations = [(-1, -1.4), (-0.62, -0.7), (-2, 0.5), (1.7, 1.2), (2.0, 1.4), (2.4, 1.7)]
#plt.clabel(CS, inline=1, fontsize=10, manual=manual_locations)


ax[1].set_yticklabels([]),ax[2].set_yticklabels([]),ax[3].set_yticklabels([])

ax[0].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[0].set_xlim(0.02,0.3) , ax[0].set_ylim(0.001,0.075)
ax[1].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[1].set_xlim(0.02,0.3) , ax[1].set_ylim(0.001,0.075)
ax[2].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[2].set_xlim(0.02,0.3) , ax[2].set_ylim(0.001,0.075)
ax[3].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[3].set_xlim(0.02,0.3) , ax[3].set_ylim(0.001,0.075)

plt.tight_layout()
ax[0].text(0.025,0.068,'(a)',fontsize=20,c='white') 
ax[1].text(0.025,0.068,'(b)',fontsize=20,c='white') 
ax[2].text(0.025,0.068,'(c)',fontsize=20,c='white') 
ax[3].text(0.025,0.068,'(d)',fontsize=20,c='white') 

plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/'+'reg_short7'+'.jpg', dpi=300,bbox_inches='tight')
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/'+'reg_short7'+'.jpg', dpi=1200,bbox_inches='tight')
plt.show()