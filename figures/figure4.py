#create figure 4

runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

#load data
outall = []
run0,fold = 0 , 'defi_tide1'
for i in range(121):
    outall.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))

#calculate numbers - adjustment time, change in salt intrusion length, recovery time
def calc_numbers(Ti, Li,Ui):
    L_min, L_max = np.min(Li) , np.max(Li)
    
    #adjustment time
    Ta = Ti[np.where(Li<L_min+(1-0.9)*(L_max-L_min))[0][0]] - Ti[np.where(Ui>Ui[0])[0][0]]
    #change in salt intrusion length
    dL = L_max - L_min
    #recovery time
    Tr = Ti[np.where(Li<L_max-(1-0.9)*(L_max - L_min))[0][-1]] - Ti[np.where(Li == L_min)[0][-1]] 
    #scales of these variables

    #inital guess scale
    #adjustment time
    Ta_sc = dL*900/np.max(Ui)/3600/24
    #salt intrusion length
    dL_sc = L_max
    #recovery time
    Tr_sc = L_max*900/np.min(Ui)/3600/24
    #Tr_sc = 0.9*(L_max*1000)**4*48*cv*1*10 /(g*Be*1000*35**2)/3600/24
    
    #return  Ta,dL,Tr, 
    return Ta/Ta_sc , dL/dL_sc , Tr/Tr_sc , Ta, dL, Tr, L_max


Ta,dLr,Tr,FrRp,FrRbc,FrT = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Ta2, dL2, Tr2 = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Lm = np.zeros(len(outall))
for i in range(len(outall)):
    Ta[i],dLr[i],Tr[i], Ta2[i],dL2[i],Tr2[i],Lm[i]  = calc_numbers(outall[i][3]/24,-outall[i][4],outall[i][0][3]/(outall[i][1][0]*outall[i][1][1]))
    
    FrRp[i] = np.max(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrRbc[i] = np.min(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrT[i] = outall[i][0][0][0]/np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0])
    
#plot
a0 = plt.contourf([np.linspace(0,1,101),np.linspace(0,1,101)],np.linspace(0,1,101))
plt.close()
fig,ax = plt.subplots(1,4,figsize=(15,5))
a1=ax[1].tricontourf(FrT, FrRbc, dLr ,cmap='Spectral_r',levels = np.linspace(0.4,1,13))
a2=ax[2].tricontourf(FrT, FrRbc, Ta ,cmap='Spectral_r',levels = np.linspace(1,2,6))
a3=ax[3].tricontourf(FrT, FrRbc, Tr ,cmap='Spectral_r',levels = np.linspace(0.3,0.7,9))

c0 =ax[0].tricontour(FrT, FrRbc, Lm  ,colors='black',levels = [20,30,40,50,75,125])
c1 =ax[1].tricontour(FrT, FrRbc, dL2 ,colors='black',levels = [10,20,30,40,50,100])
c2 =ax[2].tricontour(FrT, FrRbc, Ta2 ,colors='black',levels = [0,1,1.5,2,3,5,10])
c3 =ax[3].tricontour(FrT, FrRbc, Tr2 ,colors='black',levels = [3,5,10,20,100])

ax[0].set_xlabel(r'$Fr_{T}$ ',fontsize=20),ax[0].set_ylabel(r'$Fr_{R,bg}$ ',fontsize=20)
ax[1].set_xlabel(r'$Fr_{T}$ ',fontsize=20),ax[2].set_xlabel(r'$Fr_{T}$ ',fontsize=20),ax[3].set_xlabel(r'$Fr_{T}$ ',fontsize=20)
for i in range(4): ax[i].tick_params(axis='both', which='major', labelsize=14)


cb0 = fig.colorbar(a0, ax=ax[0],orientation='horizontal', pad=0.16)
cb0.set_label(label='$T_{adj}/T_{adj,sc}$',fontsize=20)
cb2 = fig.colorbar(a2, ax=ax[2],orientation='horizontal', pad=0.16)
cb2.set_label(label='$T_{adj}/T_{adj,sc}$',fontsize=20)
cb2.ax.tick_params(labelsize=14)
cb1 = fig.colorbar(a1, ax=ax[1],orientation='horizontal', pad=0.16)
cb1.set_label(label='$\Delta X_2/X_{2}(t=0)$',fontsize=20)
cb1.ax.tick_params(labelsize=14)
cb3 = fig.colorbar(a3, ax=ax[3],orientation='horizontal', pad=0.16)
cb3.set_label(label='$T_{rec}/T_{rec,sc}$',fontsize=20)
cb3.ax.tick_params(labelsize=14)
cb3.ax.locator_params(nbins=5)
cb0.remove() 

for i in range(4):ax[i].set_ylim(0.001,0.05)
#THIS PART HAS TO BE IMPROVED SOMEWHAT!
# 
ax[0].clabel(c0, inline=True, fontsize=14,fmt='%1.1f')#,manual = [(0.25,0.06),(0.12,0.03),(0.1,0.02),(0.07,0.01),(0.025,0.005)] )
ax[2].clabel(c2, inline=True, fontsize=14,fmt='%1.1f',manual = [(0.5,0),(0.6,0.015),(0.55,0.01),(0.75,0.02),(0.8,0.04)])
ax[1].clabel(c1, inline=True, fontsize=14,fmt='%1.1f')#, manual = [(0.25,0.06),(0.2,0.03),(0.15,0.02),(0.2,0.01),(0.15,0.001)])
ax[3].clabel(c3, inline=True, fontsize=14,fmt='%1.1f')#,manual = [(0.25,0.06),(0.2,0.03),(0.15,0.02),(0.225,0.01),(0.15,0.001)])

#manual_locations = [(-1, -1.4), (-0.62, -0.7), (-2, 0.5), (1.7, 1.2), (2.0, 1.4), (2.4, 1.7)]
#plt.clabel(CS, inline=1, fontsize=10, manual=manual_locations)


ax[1].set_yticklabels([]),ax[2].set_yticklabels([]),ax[3].set_yticklabels([])


plt.tight_layout()
ax[0].text(0.47,0.046,'(a)',fontsize=20,c='black') 
ax[1].text(0.47,0.046,'(b)',fontsize=20,c='black') 
ax[2].text(0.47,0.046,'(c)',fontsize=20,c='black') 
ax[3].text(0.47,0.046,'(d)',fontsize=20,c='black') 


#plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/'+'reg_Frt7'+'.jpg', dpi=300,bbox_inches='tight')
#plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/'+'reg_Frt7'+'.jpg', dpi=1200,bbox_inches='tight')
plt.show()