#create figure 2
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

def termsvert_contour_zt(par_v, par_c, par_nd,Time,xloc,ts,te):
    Ut, soc, sri, Q, sf, u_w, d_w= par_v
    b0, H, N, Lsc, dt, T = par_c
    Ln, bn, dxn = par_nd
    

    
    DT=(np.sum(Ln)*H*b0*dt)/(3600*Q[0])
    u_w, d_w = 0,0
    
    dln = dxn/Lsc
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]
    
    N= int(N)
    M=N+1 
    #plotting parameters
    nz=50 #vertical step - only for plot
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    pz = np.linspace(-H,0,nz+1)
    
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))
    
    xloc= np.where(px==xloc)[0][0]

    #lists of ks and ns and pis
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    T1,T2,T3,T4,T5,T6,T7 = np.zeros((te-ts,51)),np.zeros((te-ts,51)),np.zeros((te-ts,51)),np.zeros((te-ts,51)),np.zeros((te-ts,51)),np.zeros((te-ts,51)),np.zeros((te-ts,51))
    dsp = np.zeros((te-ts,51))
    for t in range(ts,te):
        #build parameters
        u_bar = Q[t]/(H*b)
        Kh = ch*Ut[t]*b
        Av = cv*Ut[t]*np.array([H]*np.sum(nxn))
        Kv = Av/Sc
        alf = g*Be*H**3/(48*Av)
        
        #partial slip coefficients
        g1 = -1 + (1.5+3*Av/(sf[t]*H)) / (1+ 3 *Av/(sf[t]*H))
        g2 =  -3 / (2+6*Av/(sf[t]*H))
        g3 = (1+4*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H)) * (9+18*Av/(sf[t]*H)) - 8 - 24*Av/(sf[t]*H)
        g4 = -9 * (1+4*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H))
        g5 = - 8
        g6 = 4+4*Av/(sf[t]*H) -12*(0.5+Av/(sf[t]*H))**2/(1+3*Av/(sf[t]*H))
        g7 = 4
        g8 = (3+6*Av/(sf[t]*H)) / (1+3*Av/(sf[t]*H))
        
        sb = np.transpose(np.reshape(outp[t],(np.sum(nxn),M))[:,0])*soc[t]
        sn = np.reshape(outp[t],(np.sum(nxn),M))[:,1:]*soc[t]
    
        term_build = {
            'dsp_dt': lambda sb,sn,x,z: np.sum([(np.reshape(outp[t]- outp[t-1],(np.sum(nxn),M) )[:,1:])[x,n] * np.cos(nnp[n]*z) for n in range(N)],0)*soc[t],
            '1': lambda sb, sn, x, z : u_bar[x] * np.sum([(sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * np.cos(nnp[n]*z) for n in range(N)],0),
            '2': lambda sb, sn, x, z : (u_bar[x]*(g1[x] + g2[x]*z**2) + alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x])*(g3[x]+g4[x]*z**2+g5*z**3)+(r*CD*H*u_w**2*d_w)/(4*Av[x])*(g6[x]+g7*z+g8[x]*z**2) ) * np.sum([(sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * np.cos(nnp[n]*z) for n in range(N)],0),
            '3': lambda sb, sn, x, z : (u_bar[x]*(g1[x] + g2[x]*z**2) + alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x])*(g3[x]+g4[x]*z**2+g5*z**3)+(r*CD*H*u_w**2*d_w)/(4*Av[x])*(g6[x]+g7*z+g8[x]*z**2) ) * (sb[x+1]-sb[x-1])/(2*dx[x]),
            '4': lambda sb, sn, x, z : -(bex[x]**-1*np.sum([sn[x,n]*alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x]) * (2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0) 
                                        + np.sum([ sn[x,n] * alf[x]*(sb[x+1]-2*sb[x]+sb[x-1])/(dx[x]**2)*(2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ (sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * (2*g2[x]*u_bar[x] * np.cos(nnp[n])/nnp[n]**2 + (r*CD*H*u_w**2*d_w)/(4*Av[x]) * (g7+(2*g8[x]-g7)*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ (sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x])*(2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ sn[x,n]*bex[x]**-1*(r*CD*H*u_w**2*d_w)/(4*Av[x])*(g7+(2*g8[x]-g7)*np.cos(nnp[n])/(nnp[n]**2))  for n in range(N)],0) ),  
            '5': lambda sb, sn, x, z : -(alf[x]*H* ( (sb[x+1]-2*sb[x]+sb[x-1])/(dx[x]**2) + bex[x]**-1*(sb[x+1]-sb[x-1])/(2*dx[x]) ) * (-g5/4*z**4-g4[x]/3*z**3 -g3[x]*z)
                                         - bex[x]**-1*(r*CD*H*u_w**2*d_w)/(4*Av[x])*(g6[x]*z+g7/2*z**2+g8[x]/3*z**3)) * np.sum([nnp[n]/H * sn[x,n]*np.sin(nnp[n]*z) for n in range(N)],0) ,
            '6': lambda sb, sn, x, z : Kv[x] * np.sum([nnp[n]**2/H**2 * sn[x,n]*np.cos(nnp[n]*z) for n in range(N)],0) ,
            '7': lambda sb, sn, x, z : -2*bex[x]**(-1)*Kh[x]*np.sum([(sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * np.cos(nnp[n]*z) for n in range(N)],0) - Kh[x] * np.sum([(sn[x+1,n]-2*sn[x,n]+sn[x-1,n])/(dx[x]**2) * np.cos(nnp[n]*z) for n in range(N)],0) ,
            '10': lambda sb, sn, x, z : u_bar[x] * (sb[x+1]-sb[x-1])/(2*dx[x]) ,
            '11': lambda sb, sn, x, z : (bex[x]**-1*np.sum([sn[x,n]*alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x]) * (2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0) 
                                        + np.sum([ sn[x,n] * alf[x]*(sb[x+1]-2*sb[x]+sb[x-1])/(dx[x]**2)*(2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ (sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * (2*g2[x]*u_bar[x] * np.cos(nnp[n])/nnp[n]**2 + (r*CD*H*u_w**2*d_w)/(4*Av[x]) * (g7+(2*g8[x]-g7)*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ (sn[x+1,n]-sn[x-1,n])/(2*dx[x]) * alf[x]*(sb[x+1]-sb[x-1])/(2*dx[x])*(2*g4[x]*np.cos(nnp[n])/(nnp[n]**2)+g5*((6*np.cos(nnp[n])-6)/(nnp[n]**4) -3*np.cos(nnp[n])/(nnp[n]**2))) for n in range(N)],0)
                                        + np.sum([ sn[x,n]*bex[x]**-1*(r*CD*H*u_w**2*d_w)/(4*Av[x])*(g7+(2*g8[x]-g7)*np.cos(nnp[n])/(nnp[n]**2))  for n in range(N)],0) ), 
            '12': lambda sb, sn, x, z : -Kh[x] * (2*bex[x]**(-1) * (sb[x+1]-sb[x-1])/(2*dx[x]) + (sb[x+1]-2*sb[x]+sb[x-1])/(dx[x]**2)) ,
            #'dsb':lambda t: (np.transpose(np.reshape(outp[t+1],(np.sum(nxn),M))[:,0])*soc[t+1]- np.transpose(np.reshape(outp[t-1],(np.sum(nxn),M))[:,0])*soc[t-1]) / (2*dt) , 
            #'dsp':lambda x, t, z: np.sum([(np.reshape(outp[t+1],(np.sum(nxn),M))[:,1:]*soc[t+1] - np.reshape(outp[t-1],(np.sum(nxn),M))[:,1:]*soc[t-1])[x,n] / (2*DT) * np.cos(nnp[n]*z) for n in range(N)],0)
            }
        
        dx=dl*Lsc
        #print((np.reshape(outp[t+1],(np.sum(nxn),M))[:,1:]*soc[t+1] - np.reshape(outp[t-1],(np.sum(nxn),M))[:,1:]*soc[t-1])[xloc] / (2*DT))
        
        T1[t-ts], T2[t-ts], T3[t-ts] = term_build['1'](sb,sn, xloc, np.linspace(-1,0,51)), term_build['2'](sb,sn, xloc, np.linspace(-1,0,51)), term_build['3'](sb,sn, xloc, np.linspace(-1,0,51))
        T4[t-ts] = term_build['4'](sb,sn, xloc, np.linspace(-1,0,51))
        T5[t-ts], T6[t-ts], T7[t-ts] = term_build['5'](sb,sn, xloc, np.linspace(-1,0,51)), term_build['6'](sb,sn, xloc, np.linspace(-1,0,51)), term_build['7'](sb,sn, xloc, np.linspace(-1,0,51))
        dsp[t-ts] = term_build['dsp_dt'](sb,sn, xloc, np.linspace(-1,0,51))

    #second plot: next to each other
    
    fig,ax = plt.subplots(4,2,figsize = (12,10))
    #version 1: dimensional
    lvl = np.linspace(-1.25e-3,1.25e-3,101)    
    #ax[0].plot(Tt[:100], Li[:100], 'g',linewidth=3)
    #ax[0,0].plot(T_eq,s_LV_eq,'black')
    #ax[1].plot(np.linspace(0,25,100),Q[:100], 'g',linewidth=3)
    #ax[2].plot(np.linspace(0,25,100),Uu[0:100])
    tt, zz= Time[ts:te]/24-16, np.linspace(-H,0,51)
    a0=ax[0,0].contourf(tt,zz,(T1+T2+T3+T4+T5+T6+T7).transpose(),cmap='Spectral',levels = lvl )
    a1=ax[0,1].contourf(tt,zz,T1.transpose(),cmap='Spectral',levels = lvl)
    ax[1,0].contourf(tt,zz,T2.transpose(),cmap='Spectral',levels = lvl)
    ax[1,1].contourf(tt,zz,T3.transpose(),cmap='Spectral',levels = lvl)
    ax[2,0].contourf(tt,zz,T4.transpose(),cmap='Spectral',levels = lvl)
    ax[2,1].contourf(tt,zz,T5.transpose(),cmap='Spectral',levels = lvl)
    ax[3,0].contourf(tt,zz,T6.transpose(),cmap='Spectral',levels = lvl)
    ax[3,1].contourf(tt,zz,T7.transpose(),cmap='Spectral',levels = lvl)

    for i in range(4): ax[i,0].set_ylabel('$z$ [m]',fontsize=20) , ax[i,1].yaxis.set_ticklabels([])
    for i in range(3): ax[i,0].xaxis.set_ticklabels([]) , ax[i,1].xaxis.set_ticklabels([])
    ax[0,0].text(28.5,-1.2,'(a)',fontsize=20),ax[1,0].text(28.5,-1.2,'(c)',fontsize=20),ax[2,0].text(28.5,-1.2,'(e)',fontsize=20),ax[3,0].text(28.5,-1.2,'(g)',fontsize=20)
    ax[0,1].text(28.5,-1.2,'(b)',fontsize=20),ax[1,1].text(28.5,-1.2,'(d)',fontsize=20),ax[2,1].text(28.5,-1.2,'(f)',fontsize=20),ax[3,1].text(28.5,-1.2,'(h)',fontsize=20)
    #fig.text(0.12,0.94,'(a)',fontsize=20), fig.text(0.57,0.94,'(b)',fontsize=20), fig.text(0.12,0.71,'(c)',fontsize=20), fig.text(0.57,0.71,'(d)',fontsize=20)
    #fig.text(0.12,0.475,'(e)',fontsize=20), fig.text(0.57,0.475,'(f)',fontsize=20), fig.text(0.12,0.24,'(g)',fontsize=20), fig.text(0.57,0.24,'(h)',fontsize=20)
    for i in range(4): ax[i,0].tick_params(axis='both', which='major', labelsize=15) , ax[i,1].tick_params(axis='both', which='major', labelsize=15)
    ax[3,0].set_xlabel('Time [doy 2009]',fontsize=20) , ax[3,1].set_xlabel('Time [doy 2009]',fontsize=20)
    
    plt.tight_layout()
    fig.subplots_adjust(bottom=0.065)
    cbar_ax = fig.add_axes([0.25, -0.06, 0.6, 0.04])
    cb=fig.colorbar(a1, cax=cbar_ax,orientation='horizontal', format='%.0e',ticks = [-1e-3,-5e-4,0,5e-4,1e-3]) #version 1
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label=' [psu s$^{-1}$]',fontsize=20)
    
    #plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/HDE_sper5.jpg', bbox_inches = "tight",dpi=300)
    #plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/HDE_sper5.jpg', bbox_inches = "tight",dpi=1200)
    plt.show()
    
    
#load here the output from run with 2009 simplified geometry
run,fold= 92,'Guadalquivir'
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/par_nd.txt')
Time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+fold+'/run'+str(run)+'/raw_out.txt')
#'''
termsvert_contour_zt(par_v, par_c, par_nd,Time,-29.178,330,600)