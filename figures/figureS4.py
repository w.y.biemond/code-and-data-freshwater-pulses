#Guadalquivir extra analysis

#load the Guadalquivir data

runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')

#load data
CTD = sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/CTDs_salinity.mat')

S0_t, S0_s = np.array(CTD['S0'][:,0]) ,np.array(CTD['S0'][:,1])
S1_t, S1_s = np.array(CTD['S1'][:,0]) ,np.array(CTD['S1'][:,1])
S2_t, S2_s = np.array(CTD['S2'][:,0]) ,np.array(CTD['S2'][:,1])
S3_t, S3_s = np.array(CTD['S3'][:,0]) ,np.array(CTD['S3'][:,1])
S4_t, S4_s = np.array(CTD['S4'][:,0]) ,np.array(CTD['S4'][:,1])
S5_t, S5_s = np.array(CTD['S5'][:,0]) ,np.array(CTD['S5'][:,1])
S6_t, S6_s = np.array(CTD['S6'][:,0]) ,np.array(CTD['S6'][:,1])
S7_t, S7_s = np.array(CTD['S7'][:,0]) ,np.array(CTD['S7'][:,1])


t2008 = pd.to_datetime('2008-01-01 00:00:00').value/(10**9*3600*24)+719529

#interpolate data on a regular grid
ti = np.arange(0,365*3,1/24) #so timesteps are 0.05 day 
S0_i = np.interp(ti, S0_t-t2008, S0_s)
S1_i = np.interp(ti, S1_t-t2008, S1_s)
S2_i = np.interp(ti, S2_t-t2008, S2_s)
S3_i = np.interp(ti, S3_t-t2008, S3_s)
S4_i = np.interp(ti, S4_t-t2008, S4_s)
S5_i = np.interp(ti, S5_t-t2008, S5_s)
S6_i = np.interp(ti, S6_t-t2008, S6_s)
S7_i = np.interp(ti, S7_t-t2008, S7_s)


'''
#remove values with no data
for i in range(len(ti)):
    if np.abs(ti[i]-(S0_t-t2008)).min() > 0.5: S0_i[i] = None
    if np.abs(ti[i]-(S1_t-t2008)).min() > 0.5: S1_i[i] = None
    if np.abs(ti[i]-(S2_t-t2008)).min() > 0.5: S2_i[i] = None
    if np.abs(ti[i]-(S3_t-t2008)).min() > 0.5: S3_i[i] = None
    if np.abs(ti[i]-(S4_t-t2008)).min() > 0.5: S4_i[i] = None
    if np.abs(ti[i]-(S5_t-t2008)).min() > 0.5: S5_i[i] = None
    if np.abs(ti[i]-(S6_t-t2008)).min() > 0.5: S6_i[i] = None
    if np.abs(ti[i]-(S7_t-t2008)).min() > 0.5: S7_i[i] = None
'''


#low-pass filter salinity data
def filt_dataset(data,t_data,t_filt):
    if t_filt%2==0: 
        print('ERROR: t_filt should be an odd number but it is not!')
        return
    #things which should be the case, otherwise it wont work:
        #you need to give the half-time of the filter in the rsame units as the t of the dataset
        #the dataset need to be on regular time intervals. this can easily be added but I did not do this yet, as I created this function on a friday
    #fix the beginning and the end of the dataset
    data2 = np.zeros(len(data)+t_filt)
    data2[int(t_filt/2):-int(t_filt/2)-1] = data
    data2[:int(t_filt/2)] = data[0]
    data2[-int(t_filt/2)-1:] = data[-1]
    
    #calculate the gaussian distribution
    weigh = np.exp(-(np.linspace(-3,3,t_filt))**2)/np.sum(np.exp(-(np.linspace(-3,3,t_filt))**2))
    weigh_tot = np.zeros(t_filt)
    for i in range(t_filt): weigh_tot[i] = np.sum(weigh[:i])
    #print('The halftime of this filter based on surface is ',(t_filt/2+0.5-np.where(weigh_tot>0.25)[0][0])*2, ' hours')
    #print('The half-amplitude of this filter based on value is ',(t_filt/2+0.5-np.where(weigh>0.5*np.max(weigh))[0][0])*2, ' hours')

    #do the dataset times the gaussian distribution
    w1 = np.tile(np.arange(t_filt),len(data2)-t_filt).reshape(len(data2)-t_filt,t_filt)+np.arange(len(data2)-t_filt)[:,np.newaxis]
    #sum over the values to get the filtered dataset
    dat_sm = np.nansum(data2[w1]*weigh,1)
    
    #dat_sm[np.where(np.isnan(data)==True)] = None #where there are nans in the dataset, should be a nan in the interpolated value. 
    
    return dat_sm

S0_f = filt_dataset(S0_i,ti,71)
S1_f = filt_dataset(S1_i,ti,71)
S2_f = filt_dataset(S2_i,ti,71)
S3_f = filt_dataset(S3_i,ti,71)
S4_f = filt_dataset(S4_i,ti,71)
S5_f = filt_dataset(S5_i,ti,71)
S6_f = filt_dataset(S6_i,ti,71)
S7_f = filt_dataset(S7_i,ti,71)

#discharge
disc = sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/freshwater_discharges.mat')
#print(list(disc))
Q_gu = np.array(disc['Q']).flatten()
Qt = np.array(disc['t']).flatten()

# =============================================================================
# #load model output
# =============================================================================
g=9.81
Be = 7.6e-4

#load files
run=92
#run number 
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/raw_out.txt')

#extract paramaters
Ut, soc, sri, Q, sf, u_w, d_w= par_v
b0, H, N, Lsc,  dt, T = par_c
#b0, H, N, Lsc = par_c
Ln, bn, dxn = par_nd
T=len(time)

#rebuild x-coordinates
nxn = np.array(Ln/dxn+1,dtype=int)
M,T,nx = int(N+1) , int(T) , np.sum(nxn)-1
px = np.zeros(np.sum(nxn))
px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
px=px/1000

#erg ruw bepaald eh
S0_ind = list(px).index(min(px, key=lambda x:abs(x-(-0-25)))) #index of Le Verdon (MOET NOG FF WAT BETER BEPAALD)
S1_ind = list(px).index(min(px, key=lambda x:abs(x-(-17.5-25)))) #51  #index of Pauillac (MOET NOG FF WAT BETER BEPAALD)
S2_ind = list(px).index(min(px, key=lambda x:abs(x-(-23.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S3_ind = list(px).index(min(px, key=lambda x:abs(x-(-26-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S4_ind = list(px).index(min(px, key=lambda x:abs(x-(-35.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S5_ind = list(px).index(min(px, key=lambda x:abs(x-(-47-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S6_ind = list(px).index(min(px, key=lambda x:abs(x-(-57.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S7_ind = list(px).index(min(px, key=lambda x:abs(x-(-84.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)

s_min =np.zeros(T)
L_int, sm_S0, sm_S1, sm_S2, sm_S3, sm_S4, sm_S5, sm_S6, sm_S7 = np.zeros(T), np.zeros(T) , np.zeros(T) , np.zeros(T), np.zeros(T), np.zeros(T) , np.zeros(T) , np.zeros(T), np.zeros(T)

for j in range(T):
    sss=outp[j]
    
    nz=20 #vertical step - only for plot
    s_b = np.transpose([np.reshape(sss,(nx+1,M))[:,0]]*(nz+1))
    
    sn = np.reshape(sss,(nx+1,M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for k in range(nx+1)])
    s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    s_min[j] = np.min(s)
    #salt intrusion length
    L_int[j] =  px[np.where(s[:,-1]>2)[0][0]]+Ln[-1]/1000
    
    #salinity at the selected stations
    sm_S0[j] = (soc[0]*(s_b[S0_ind,0]+np.sum([sn[S0_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S1[j] = (soc[0]*(s_b[S1_ind,0]+np.sum([sn[S1_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S2[j] = (soc[0]*(s_b[S2_ind,0]+np.sum([sn[S2_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S3[j] = (soc[0]*(s_b[S3_ind,0]+np.sum([sn[S3_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S4[j] = (soc[0]*(s_b[S4_ind,0]+np.sum([sn[S4_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S5[j] = (soc[0]*(s_b[S5_ind,0]+np.sum([sn[S5_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S6[j] = (soc[0]*(s_b[S6_ind,0]+np.sum([sn[S6_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S7[j] = (soc[0]*(s_b[S7_ind,0]+np.sum([sn[S7_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)


s_stat_92 = np.array([sm_S0,sm_S1,sm_S2,sm_S3,sm_S4,sm_S5,sm_S6,sm_S7])

#time series of salt intrusion length, river discharge and time in days
Li_r1=L_int
Qq_r1=Q
Tt_r1=time/24
Uu_r1=Ut
    
print('run ', run, ' is loaded')
print(np.min(s_min))

#load files
run=95 #wordt hopelijk 90
#run number 
par_v = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_var.txt')
par_c = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_const.txt')
par_nd = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/par_nd.txt')
time = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/time.txt')
outp = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/Guadalquivir/run'+str(run)+'/raw_out.txt')

#extract paramaters
Ut, soc, sri, Q, sf, u_w, d_w= par_v
b0, H, N, Lsc,  dt, T = par_c
#b0, H, N, Lsc = par_c
Ln, bn, dxn = par_nd
T=len(time)

#rebuild x-coordinates
nxn = np.array(Ln/dxn+1,dtype=int)
M,T,nx = int(N+1) , int(T) , np.sum(nxn)-1
px = np.zeros(np.sum(nxn))
px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
px=px/1000

#erg ruw bepaald eh
S0_ind = list(px).index(min(px, key=lambda x:abs(x-(-0-25)))) #index of Le Verdon (MOET NOG FF WAT BETER BEPAALD)
S1_ind = list(px).index(min(px, key=lambda x:abs(x-(-17.5-25)))) #51  #index of Pauillac (MOET NOG FF WAT BETER BEPAALD)
S2_ind = list(px).index(min(px, key=lambda x:abs(x-(-23.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S3_ind = list(px).index(min(px, key=lambda x:abs(x-(-26-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S4_ind = list(px).index(min(px, key=lambda x:abs(x-(-35.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S5_ind = list(px).index(min(px, key=lambda x:abs(x-(-47-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S6_ind = list(px).index(min(px, key=lambda x:abs(x-(-57.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)
S7_ind = list(px).index(min(px, key=lambda x:abs(x-(-84.5-25)))) #index of Bordeaux (MOET NOG FF WAT BETER BEPAALD)

s_min =np.zeros(T)
L_int, sm_S0, sm_S1, sm_S2, sm_S3, sm_S4, sm_S5, sm_S6, sm_S7 = np.zeros(T), np.zeros(T) , np.zeros(T) , np.zeros(T), np.zeros(T), np.zeros(T) , np.zeros(T) , np.zeros(T), np.zeros(T)

for j in range(T):
    sss=outp[j]
    
    nz=20 #vertical step - only for plot
    s_b = np.transpose([np.reshape(sss,(nx+1,M))[:,0]]*(nz+1))
    
    sn = np.reshape(sss,(nx+1,M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[k,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for k in range(nx+1)])
    s = (s_b+s_p)*soc[0] #heb er mn twijfels bij of dit goed is eigenlijk..
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    s_min[j] = np.min(s)
    #salt intrusion length 
    L_int[j] =  px[np.where(s[:,-1]>2)[0][0]]+Ln[-1]/1000
    
    #salinity at the selected stations
    sm_S0[j] = (soc[0]*(s_b[S0_ind,0]+np.sum([sn[S0_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S1[j] = (soc[0]*(s_b[S1_ind,0]+np.sum([sn[S1_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S2[j] = (soc[0]*(s_b[S2_ind,0]+np.sum([sn[S2_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S3[j] = (soc[0]*(s_b[S3_ind,0]+np.sum([sn[S3_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S4[j] = (soc[0]*(s_b[S4_ind,0]+np.sum([sn[S4_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S5[j] = (soc[0]*(s_b[S5_ind,0]+np.sum([sn[S5_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S6[j] = (soc[0]*(s_b[S6_ind,0]+np.sum([sn[S6_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)
    sm_S7[j] = (soc[0]*(s_b[S7_ind,0]+np.sum([sn[S7_ind,n-1]*np.cos(np.pi*n/H*-1) for n in range(1,M)]))).round(2)

s_stat_95 = np.array([sm_S0,sm_S1,sm_S2,sm_S3,sm_S4,sm_S5,sm_S6,sm_S7])


#time series of salt intrusion length, river discharge and time in days
Li_r2=L_int
Qq_r2=Q
Tt_r2=time/24
Uu_r2=Ut
    
print('run ', run, ' is loaded')

# =============================================================================
# load tributaries - I believe this is correct
# =============================================================================
#prebewerkign: files saved as xlsx files
Gergal = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Gergal.xlsx'))
Aznalc = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Aznalczar.xlsx'))
Guadaira = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Guadaira.xlsx'))
Aguila = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/TorredelAguila.xlsx'))
#distances from mouth - smallest width taken as start estuary
d_Azn = 17.8 #km
d_Agu = 48.1 #km
d_Gua = 58.9 #km
d_Ger = 91.7 #km 

dimn = [0,31,30,31,31,28,31,30,31,30,31,31,30]#days in month - first month is october here!!
diml = [0,31,30,31,31,29,31,30,31,30,31,31,30]#days in month - first month is october here!!

#Gergal
noy = 5 #number of years
year = 2008
Q_Ger = np.zeros(365*noy+2)
t_Ger = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Ger[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Gergal[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.plot(t_Ger,Q_Ger)

#Aznalc: 4 jaar: 2008, 2009  2011, 2012
noy=5
year = 2008
Q_Azn = np.zeros(365*noy+2)
t_Azn = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
tdp = tdp+365
for i in range(2):
    dim = diml if (year+3+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+(i+2)*31:1+(i+2)*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Azn[365*2+1:365*3+1] = -999
plt.plot(t_Azn,Q_Azn)

#Guadaira: 2 year: 2008-2009
noy=5
year = 2008
Q_Gua = np.zeros(365*5+2)
t_Gua = np.arange(365*5+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529 
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Gua[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Guadaira[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Gua[365*2+1:] = -999
    
plt.plot(t_Gua, Q_Gua)

#torre del Aguila
noy = 5 #number of years
year = 2008
Q_Agu = np.zeros(365*noy+2)
t_Agu = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Agu[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aguila[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.show()

# =============================================================================
# #%%interpolate salinity data
# =============================================================================

S_all = np.transpose([S0_f,S1_f,S2_f,S3_f,S4_f, S5_f, S6_f, S7_f])
x_all = np.array([0,-17.3,-23.6,-26.6,-35.5,-47.1,-57.6,-84.3])

Smeas_int = np.zeros((len(S0_f),1001))
for i in range(len(S0_f)):
    treshold=0
    if S_all[i,1]<treshold: Smeas_int[i] = None
    else: Smeas_int[i] = np.interp(np.linspace(-100,0,1001),np.flip(x_all),np.flip(S_all[i]))

X2_meas = np.zeros(len(S0_f))
for i in range(len(S0_f)): 
    temp = np.where(Smeas_int[i]>2)[0]
    if len(temp)>0: X2_meas[i] =-np.linspace(-100,0,1001)[temp][0]
    else: X2_meas[i]=None
    
# =============================================================================
# interpolate model output to observations
# =============================================================================
s2_stat_92, s2_stat_95 = [] , []

ds,de=650,975
ts,te = np.where(ti==ds)[0][0],np.where(ti==de+1)[0][0]
for i in range(8):  s2_stat_95.append(np.interp(ti[ts:te] , Tt_r2+ds , s_stat_95[i] ))

ds,de=350,450
ts,te = np.where(ti==ds)[0][0],np.where(ti==de+1)[0][0]
for i in range(8):  s2_stat_92.append(np.interp(ti[ts:te] , Tt_r1+ds , s_stat_92[i] ))

#calculate root mean squared error
def rmse(measured: np.ndarray, model: np.ndarray):
    return np.sqrt(np.mean(np.square(measured-model)))

# =============================================================================
# plot for pulse in 2010
# =============================================================================
#ds,de =350,450
ds,de = 650,975

ts,te = np.where(Qt==t2008+ds)[0][0],np.where(Qt==t2008+de+1)[0][0]
ts2,te2 = np.where(t_Ger==t2008+ds)[0][0],np.where(t_Ger==t2008+de+1)[0][0]
Q_Ger[np.where(Q_Ger==-999.)],Q_Gua[np.where(Q_Gua==-999.)],Q_Azn[np.where(Q_Azn==-999.)],Q_Agu[np.where(Q_Agu==-999.)] = 0 , 0 , 0 , 0
Q_here = Q_Ger[ts2:te2]+Q_Gua[ts2:te2]+Q_Azn[ts2:te2]+Q_Agu[ts2:te2]+Q_gu[ts:te]

fig,ax = plt.subplots(9,1,figsize = (10.8,18))

ax[0].plot(Qt[ts:te]-t2008, Q_here ,'r')
ax[0].set_ylim(0,3500)
ax[0].set_ylabel('River discharge [m$^3$/s]')

ax[1].plot(S0_t-t2008, S0_s,alpha=0.3,c='b')
ax[1].plot(ti, S0_f,'b',label='obs')
ax[1].plot(Tt_r2+ds,s_stat_95[0],'g',label='mod')

ax[2].plot(S1_t-t2008, S1_s,alpha=0.3,c='b')
ax[2].plot(ti, S1_f,'b',label='obs')
ax[2].plot(Tt_r2+ds,s_stat_95[1],'g',label='mod')

ax[3].plot(S2_t-t2008, S2_s,alpha=0.3,c='b')
ax[3].plot(ti, S2_f,'b',label='obs')
ax[3].plot(Tt_r2+ds,s_stat_95[2],'g',label='mod')

ax[4].plot(S3_t-t2008, S3_s,alpha=0.3,c='b')
ax[4].plot(ti, S3_f,'b',label='obs')
ax[4].plot(Tt_r2+ds,s_stat_95[3],'g',label='mod')

ax[5].plot(S4_t-t2008, S4_s,alpha=0.3,c='b')
ax[5].plot(ti, S4_f,'b',label='obs')
ax[5].plot(Tt_r2+ds,s_stat_95[4],'g',label='mod')

#'''
ax[6].plot(S5_t-t2008, S5_s,alpha=0.3,c='b')
ax[6].plot(ti, S5_f,'b',label='obs')
ax[6].plot(Tt_r2+ds,s_stat_95[5],'g',label='mod')

ax[7].plot(S6_t-t2008, S6_s,alpha=0.3,c='b')
ax[7].plot(ti, S6_f,'b',label='obs')
ax[7].plot(Tt_r2+ds,s_stat_95[6],'g',label='mod')

ax[8].plot(S7_t-t2008, S7_s,alpha=0.3,c='b')
ax[8].plot(ti, S7_f,'b',label='obs')
ax[8].plot(Tt_r2+ds,s_stat_95[7],'g',label='mod')

ax[8].set_xlabel('Time [doy 2008]')
'''
#locations
ax[1].set_title('location station: x = 0 km')
ax[2].set_title('location station: x = -17.30 km')
ax[3].set_title('location station: x = -23.60 km')
ax[4].set_title('location station: x = -26.60 km')
ax[5].set_title('location station: x = -35.50 km')
ax[6].set_title('location station: x = -47.10 km')
ax[7].set_title('location station: x = -57.60 km')
ax[8].set_title('location station: x = -84.30 km')
'''
for i in range(9):
    ax[i].grid()
    ax[i].set_xlim(ds,de)
    
    if i!=8: 
        ax[i].xaxis.set_ticklabels([])
    if i!=0: 
        ax[i].set_ylabel('Salinity [psu]')
        #ax[i].legend(loc=1)

ax[0].text(652,2850,'(a)',fontsize=15)
ax[1].text(652,30,'(b)',fontsize=15)
ax[2].text(652,29.5,'(c)',fontsize=15)
ax[3].text(652,20.5,'(d)',fontsize=15)
ax[4].text(652,20.5,'(e)',fontsize=15)
ax[5].text(652,13.75,'(f)',fontsize=15)
ax[6].text(652,10,'(g)',fontsize=15)
ax[7].text(652,5.5,'(h)',fontsize=15)
ax[8].text(652,1.6,'(i)',fontsize=15)

ts,te = np.where(ti==ds)[0][0],np.where(ti==de+1)[0][0]
ax[1].text(652,2,'RMSE = '+f'{rmse(S0_f[ts:te],s2_stat_95[0]):.2f}'+' psu')
ax[2].text(652,2,'RMSE = '+f'{rmse(S1_f[ts:te],s2_stat_95[1]):.2f}'+' psu')
ax[3].text(652,1,'RMSE = '+f'{rmse(S2_f[ts:te],s2_stat_95[2]):.2f}'+' psu')
ax[4].text(652,1,'RMSE = '+f'{rmse(S3_f[ts:te],s2_stat_95[3]):.2f}'+' psu')
ax[5].text(652,1,'RMSE = '+f'{rmse(S4_f[ts:te],s2_stat_95[4]):.2f}'+' psu')
ax[6].text(652,0.5,'RMSE = '+f'{rmse(S5_f[ts:te],s2_stat_95[5]):.2f}'+' psu')
ax[7].text(652,0.3,'RMSE = '+f'{rmse(S6_f[ts:te],s2_stat_95[6]):.2f}'+' psu')
ax[8].text(652,0.1,'RMSE = '+f'{rmse(S7_f[ts:te],s2_stat_95[7]):.2f}'+' psu')

ax[1].legend(loc=4)
ax[1].set_ylim(0,37), ax[2].set_ylim(0,37), ax[3].set_ylim(0,25), ax[4].set_ylim(0,25), ax[5].set_ylim(0,17) , ax[6].set_ylim(0,12) , ax[7].set_ylim(0,7), ax[8].set_ylim(0,2)
   
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/stat95.jpg', dpi=300,bbox_inches='tight')
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/stat95.jpg', dpi=1200,bbox_inches='tight')
