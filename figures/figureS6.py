#all figures paper 
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water
# ==============================================================
# theory of Chen
Tadj_Chen = lambda ub, Lmax, Lmin : (Lmax-Lmin) / (2*ub*(1 - (0.5*Lmin*(Lmax+Lmin))/(Lmax**2))) /(3600*24) * - np.log(0.1)
Trec_Chen = lambda ub, Lmax, Lmin : (Lmax-Lmin) / (2*ub*((0.5*Lmax*(Lmax+Lmin))/(Lmin**2) - 1)) /(3600*24) * - np.log(0.1)


# =============================================================================
# fig
# =============================================================================

outall = []
run0,fold = 0 , 'defi2'
for i in range(141):
    outall.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))

#calculate numbers - adjustment time, change in salt intrusion length, recovery time
    
def calc_numbers(Ti, Li,Ui):
    L_min, L_max = np.min(Li) , np.max(Li)
    
    #adjustment time
    Ta = Ti[np.where(Li<L_min+(1-0.9)*(L_max-L_min))[0][0]] - Ti[np.where(Ui>Ui[0])[0][0]]#(Ti[np.where(Ui>Ui[0])[0][0]-1]+.25)
    #change in salt intrusion length
    dL = L_max - L_min
    #recovery time
    Tr = Ti[np.where(Li<L_max-(1-0.9)*(L_max - L_min))[0][-1]] - Ti[np.where(Li == L_min)[0][-1]] #(Ti[np.where(Li == L_min)[0][-1]-1] +0.25)

    #scales of these variables
    #adjustment time
    Ta_sc = dL*900/np.max(Ui)/3600/24
    #salt intrusion length
    dL_sc = L_max
    #recovery time
    Tr_sc = L_max*900/np.min(Ui)/3600/24
    #Tr_sc = (L_max*dL*900*1000*48*cv*Ut)/(g*Be*soc*H**3)
    #return  Ta,dL,Tr, 
    return Ta/Ta_sc , dL/dL_sc , Tr/Tr_sc , Ta, dL, Tr, L_max , L_min

Ta,dLr,Tr,FrRp,FrRbc,FrT = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Ta2, dL2, Tr2 = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Lmax_mod, Lmin_mod =  np.zeros(len(outall)) , np.zeros(len(outall))
ub_max, ub_min = np.zeros(len(outall)) , np.zeros(len(outall))
Lm = np.zeros(len(outall))
for i in range(len(outall)):
    Ta[i],dLr[i],Tr[i], Ta2[i],dL2[i],Tr2[i],Lmax_mod[i],Lmin_mod[i]  = calc_numbers(outall[i][3]/24,-outall[i][4],outall[i][0][3]/(outall[i][1][0]*outall[i][1][1]))
    
    FrRp[i] = np.max(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrRbc[i] = np.min(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrT[i] = outall[i][0][0][0]/np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0])

    ub_max[i] = np.max(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1])
    ub_min[i] = np.min(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1])

#okay do another interpolation
FrRbc_u = list(dict.fromkeys(FrRbc))
Ta_i, dLr_i, Tr_i = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
Ta2_i, dL2_i, Tr2_i = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
Lm_i = np.zeros((len(FrRbc_u),101))
Lmax_mod_i , Lmin_mod_i = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
ub_max_i , ub_min_i = np.zeros((len(FrRbc_u),101)) , np.zeros((len(FrRbc_u),101))
for i in range(len(FrRbc_u)):
    ind = np.where(FrRbc == FrRbc_u[i])[0]
    Ta_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta[ind])
    dLr_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dLr[ind])
    Tr_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr[ind])
    Ta2_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Ta2[ind])
    dL2_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],dL2[ind])
    Tr2_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Tr2[ind])
    Lm_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Lmax_mod[ind])

    Lmax_mod_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Lmax_mod[ind])
    Lmin_mod_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],Lmin_mod[ind])
    
    ub_max_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],ub_max[ind])
    ub_min_i[i] = np.interp(np.linspace(0,0.35,101),FrRp[ind],ub_min[ind])


TaC2 = Tadj_Chen(ub_max_i, Lmax_mod_i*1000, Lmin_mod_i*1000)
TrC2 = Trec_Chen(ub_min_i, Lmax_mod_i*1000, Lmin_mod_i*1000)

TaC = TaC2 / (dL2_i*900/ub_max_i/(3600*24))
TrC = TrC2 / (Lmax_mod_i*900/ub_min_i/3600/24)

#plot
fig,ax = plt.subplots(1,4,figsize=(15,5))

a0 = ax[0].contourf(np.linspace(0,0.35,101), FrRbc_u,TaC,cmap='Spectral_r',levels = np.linspace(1.25,3,11))#,levels = [0,1,1.5,2,3,5,10])#,levels = np.linspace(1,2,6))
a2 = ax[2].contourf(np.linspace(0,0.35,101), FrRbc_u,TrC,cmap='Spectral_r',levels = np.linspace(0.0,0.5,11))#,levels = [0,1,1.5,2,3,5,10])#,levels = np.linspace(1,2,6))
a1 = ax[1].contour(np.linspace(0,0.35,101), FrRbc_u, (TaC2-Ta2_i),colors='black',levels = [0.5,1,2])# ,levels = [0,1,1.5,2,3,5,10])
a3 = ax[3].contour(np.linspace(0,0.35,101), FrRbc_u, (TrC2-Tr2_i),colors='black',levels = [-100,-20,-5],linestyles='solid')# ,levels = [0,3,5,10,20,100,500])

c0 = ax[0].contour(np.linspace(0,0.35,101), FrRbc_u, TaC2,colors='black',levels = [1,1.5,2,3,5,10])#,levels = np.linspace(1,2,6))
c2 = ax[2].contour(np.linspace(0,0.35,101), FrRbc_u, TrC2,colors='black',levels = [1,2,3,5,10,20,100])#,levels = np.linspace(1,2,6))
c1 = ax[1].contourf(np.linspace(0,0.35,101), FrRbc_u, (TaC2-Ta2_i)/Ta2_i,cmap='Spectral_r',levels = np.linspace(-0.1,0.5,13))# ,levels = [0,1,1.5,2,3,5,10])
c3 = ax[3].contourf(np.linspace(0,0.35,101), FrRbc_u, (TrC2-Tr2_i)/Tr2_i,cmap='Spectral_r',levels = np.linspace(-1,-0.1,10))# ,levels = [0,3,5,10,20,100,500])

ax[0].clabel(c0, inline=True, fontsize=14,fmt='%1.1f',manual = [(0.25,0.06),(0.2,0.03),(0.15,0.02),(0.225,0.003),(0.15,0.001)])
ax[1].clabel(a1, inline=True, fontsize=14,fmt='%1.1f')
ax[2].clabel(c2,inline=True, fontsize=14,fmt='%1.1f', manual = [(0.25,0.06),(0.2,0.03),(0.2,0.01),(0.15,0.001)])
ax[3].clabel(a3, inline=True, fontsize=14,fmt='%1.1f')
             
cb0 = fig.colorbar(a0, ax=ax[0],orientation='horizontal', pad=0.16)
cb0.set_label(label='$T_{adj}^c/T_{adj,sc}$',fontsize=20)
cb2 = fig.colorbar(a2, ax=ax[2],orientation='horizontal', pad=0.16)
cb2.set_label(label='$T_{rec}^c/T_{rec,sc}$',fontsize=20)
cb2.ax.tick_params(labelsize=14)

cb1 = fig.colorbar(c1, ax=ax[1],orientation='horizontal', pad=0.16)
cb1.set_label(label='$\Delta T_{adj} / T_{adj}$',fontsize=20)
cb1.ax.tick_params(labelsize=14)
cb3 = fig.colorbar(c3, ax=ax[3],orientation='horizontal', pad=0.16)
cb3.set_label(label='$\Delta T_{rec} / T_{rec}$',fontsize=20)
cb3.ax.tick_params(labelsize=14)
cb3.ax.locator_params(nbins=5)

ax[1].set_yticklabels([]),ax[2].set_yticklabels([]),ax[3].set_yticklabels([])

ax[0].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[0].set_xlim(0.02,0.3) , ax[0].set_ylim(0.001,0.075)
ax[1].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[1].set_xlim(0.02,0.3) , ax[1].set_ylim(0.001,0.075)
ax[2].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[2].set_xlim(0.02,0.3) , ax[2].set_ylim(0.001,0.075)
ax[3].fill_between(np.linspace(0.002,0.075*3,100), 0.075, 1/3*np.linspace(0.002,0.075*3,100),facecolor='black')
ax[3].set_xlim(0.02,0.3) , ax[3].set_ylim(0.001,0.075)

plt.tight_layout()
ax[0].text(0.025,0.068,'(a)',fontsize=20,c='white') 
ax[1].text(0.025,0.068,'(b)',fontsize=20,c='white') 
ax[2].text(0.025,0.068,'(c)',fontsize=20,c='white') 
ax[3].text(0.025,0.068,'(d)',fontsize=20,c='white') 

for i in range(4):ax[i].set_xlabel(r'$Fr_{R,p}$ ',fontsize=20)
ax[0].set_ylabel(r'$Fr_{R,bg}$ ',fontsize=20)#,ax[0].set_ylabel(r'$Fr_{R,bc}$ ',fontsize=17)
for i in range(4): ax[i].tick_params(axis='both', which='major', labelsize=12)

plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_def/Chenpeak1.jpg', dpi=300,bbox_inches='tight')
plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper FWP/figs_HQ/Chenpeak1.jpg', dpi=1200,bbox_inches='tight')
plt.show()
