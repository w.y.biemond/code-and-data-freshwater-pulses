#statistics river discharge

#we have multiple rivers so far: 
#Rhine at Lobith
#Thbames - but a short dataset
#Garonne: 30 years of data 1989-now
#Dordogne: 12 year of data 2008-now
#Guadalquivir: long dataset: 1931-2011

#lets do the same analysis with all of these datasets
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
dimn = [0,31,28,31,30,31,30,31,31,30,31,30,31]#days in month
diml = [0,31,29,31,30,31,30,31,31,30,31,30,31]#days in month


#%%import data
#discharge
# =============================================================================
# #Guadalquivir
# =============================================================================
Q_gu = np.ma.masked_array(sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/freshwater_discharges.mat')['Q']).flatten()
t_gu = np.array(sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/freshwater_discharges.mat')['t']).flatten()
t_gu2 = np.zeros(len(t_gu))
for i in range(len(t_gu)):
    dat = str(pd.to_datetime(t_gu[i]-719529, unit='D').date())
    y= int(dat[:4])
    m= int(dat[5:7])
    d=int(dat[8:10])
    dim=diml if y%4 == 0 else dimn
    t_gu2[i] = y+(np.sum(dim[:m])+d/dim[m])/np.sum(dim)
print(t_gu2)

# =============================================================================
# #Dordogne
# =============================================================================
Q_DO = np.array(pd.read_excel('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Gironde/Q_dordogne_2001_2021.xlsx', header=None,  index_col=False))#prebewerking files: opgeslagen onder andere naam en opgeslagen als xlsx ipv csv

#at location 42 the daily discharge starts
#the entire story is 73 cells long
ys,ye = 2001,2020
noy = ye-ys+1 #number of years


#make time series of this
Q_do, t_do, t_do2= np.zeros(noy*365+5), np.zeros(noy*365+5), np.zeros(noy*365+5) # 5 leap years
dp=0
for i in range(noy):
    dim = diml if (ys+i)%4==0 else dimn
    for j in range(12):
        Q_do[dp+np.sum(dim[:j+1]):dp+np.sum(dim[:j+2])] = Q_DO[42+i*73:42+i*73+dim[j+1] , 1+2*j]
    t_do[dp:dp+np.sum(dim)] = pd.to_datetime(str(ys+i)+'-01-01 00:00:00').value/(10**9*3600*24)+719529 + np.arange(np.sum(dim))
    t_do2[dp:dp+np.sum(dim)] = np.linspace(ys+i, ys+i+1,np.sum(dim))
    dp = dp + np.sum(dim)#days passed 
        
Q_do, t_do , t_do2= np.ma.masked_array(Q_do[np.where(np.isnan(Q_do)==0)[0][0]:]), t_do[np.where(np.isnan(Q_do)==0)[0][0]:] , t_do2[np.where(np.isnan(Q_do)==0)[0][0]:]

#Garonne
Q_GA = np.array(pd.read_excel('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Gironde/Q_garonne_1989_2021.xlsx', header=None,  index_col=False))#prebewerking files: opgeslagen onder andere naam en opgeslagen als xlsx ipv csv

#at location 42 the daily discharge starts
#the entire story is 73 cells long
ys,ye = 1989,2020
noy = ye-ys+1 #number of years
dimn = [0,31,28,31,30,31,30,31,31,30,31,30,31]#days in month
diml = [0,31,29,31,30,31,30,31,31,30,31,30,31]#days in month

#make time series of this
Q_ga, t_ga,t_ga2= np.zeros(noy*365+8), np.zeros(noy*365+8), np.zeros(noy*365+8) #8 leap years
dp=0
for i in range(noy):
    dim = diml if (ys+i)%4==0 else dimn
    for j in range(12):
        Q_ga[dp+np.sum(dim[:j+1]):dp+np.sum(dim[:j+2])] = Q_GA[42+i*73:42+i*73+dim[j+1] , 1+2*j]
    t_ga[dp:dp+np.sum(dim)] = pd.to_datetime(str(ys+i)+'-01-01 00:00:00').value/(10**9*3600*24)+719529 + np.arange(np.sum(dim))
    t_ga2[dp:dp+np.sum(dim)] = np.linspace(ys+i, ys+i+1,np.sum(dim))
    dp = dp + np.sum(dim)#days passed 

#remove nans is not nessecary here
'''
mas = np.zeros(len(Q_ga))
mas[np.where(np.isnan(Q_ga)==1)[0]] = 1
Q_ga, t_ga = np.ma.masked_array(Q_ga[np.where(np.isnan(Q_ga)==0)[0][0]:], mask = mas ), t_ga[np.where(np.isnan(Q_ga)==0)[0][0]:]
'''
#combine Garonne and Dordogne to Gironde
Q_gi,t_gi = [] , []
for i in range(len(t_ga)):
    if t_ga[i] in t_do:
        t_gi.append(t_ga[i])
        Q_gi.append(Q_ga[i] + Q_do[ np.where(t_ga[i] == t_do) ] )
Q_gi,t_gi = np.array(Q_gi) , np.array(t_gi)

# =============================================================================
# # San Francisco Bay
# =============================================================================

loc_sf = '/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/SanFranciscoBay/'
Q_sf = np.concatenate([np.array(pd.read_csv(loc_sf+'dayflow-results-1929-1939.csv')['TOT1']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1940-1949.csv')['TOT2']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1950-1955.csv')['TOT2']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1956-1969.csv')['TOT']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1970-1983.csv')['TOT']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1984-1996.csv')['TOT']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1997-2020.csv')['TOT']),
                       ]) * 0.0283168466 #converted to m3/s #TOT = total discharge. there is also a variable OUT, where precipitation etc are taken into account
date_sf = np.concatenate([np.array(pd.read_csv(loc_sf+'dayflow-results-1929-1939.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1940-1949.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1950-1955.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1956-1969.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1970-1983.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1984-1996.csv')['Date']),
                       np.array(pd.read_csv(loc_sf+'dayflow-results-1997-2020.csv')['Date']),
                       ])

#build time vector
t_sf = np.zeros(len(date_sf))
for i in range(len(t_sf)): t_sf[i] = pd.to_datetime(date_sf[i]).value/(10**9*3600*24)+719529 

#there is also salt intrusion length data!
X2_sf = np.array(pd.read_csv(loc_sf+'dayflow-results-1997-2020.csv')['X2'])

#plt.plot(t_sf,Q_sf)
#plt.plot(X2_sf)
''' #this is the short dataset of the Guadiana, which I do not use, since I have a longer dataset from this estuary
#%%Guadiana
#River discharge
RD_Pedro = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Other data Manuel/Guadiana/Garel-Ferreira_2015/RD_Pedrogao bewerkt.txt' , skiprows=24)
#RD_Pulo = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Other data Manuel/Guadiana/Garel-Ferreira_2015/RD_Pulo_de_Lobo bewerkt.txt' , skiprows=25)
t_gd = np.zeros(len(RD_Pedro))
for i in range(len(t_gd)):
    t_gd[i] = pd.to_datetime(str(int(RD_Pedro[i,0]))+'-'+str(int(RD_Pedro[i,1]))+'-'+ str(int(RD_Pedro[i,2]))+' '+str(int(RD_Pedro[i,3]))+':00:00').value/(10**9*3600*24)+719529 
Q_gd = RD_Pedro[:,-1]
Q_gd[np.where(Q_gd==-999.)] = None
'''

# =============================================================================
# #load data Taag
# =============================================================================
Tagus = pd.read_csv('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Taag/Taag_adjusted_v1.csv', encoding='latin-1')
Q_ta = np.array(Tagus['discharge'])[:-6]
date_ta = np.array(Tagus['date'])[:-6]
#build time vector
t_ta = np.zeros(len(date_ta))
for i in range(len(t_ta)): t_ta[i] = pd.to_datetime(date_ta[i],dayfirst = True).value/(10**9*3600*24)+719529 

# =============================================================================
# #load data Douro
# =============================================================================
Douro = pd.read_csv('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Douro/Douro_adjusted_v1.csv', encoding='latin-1')
Q_du = np.array(Douro['discharge'])[:-6]
date_du = np.array(Douro['date'])[:-6]

#build time vector
t_du = np.zeros(len(date_du))
for i in range(len(t_du)): t_du[i] = pd.to_datetime(date_du[i],dayfirst = True).value/(10**9*3600*24)+719529 

# =============================================================================
# #load data Guadiana
# =============================================================================
Guadiana = pd.read_csv('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Guadiana/Guadiana_adjusted_v1.csv', encoding='latin-1')
Q_gd = np.array(Guadiana['discharge'])[:-6]
date_gd = np.array(Guadiana['date'])[:-6]

#build time vector
t_gd = np.zeros(len(date_gd))
for i in range(len(t_gd)): t_gd[i] = pd.to_datetime(date_gd[i],dayfirst = True).value/(10**9*3600*24)+719529 
Q_gd[np.argmax(Q_gd)] = None #tehre is one strange value which I remove here

# =============================================================================
# load data Mitchell River
# =============================================================================
import netCDF4 as nc
fold = '/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Datasets rivierafvoer GRDC/'
ds = nc.Dataset(fold+'GRDC-Daily.nc 8')
Q_mi = np.array(ds['runoff_mean'])[:,0]
Q_mi[np.where(Q_mi==-999.)] = None
t_mi = np.array(ds['time']) + 719529 + pd.to_datetime('1700-01-01 00:00:00').value/(10**9*3600*24)

#%%
# =============================================================================
# Detect freshwater pulses and quantify
# =============================================================================
# to improve: way how to calculate yearly average
# discharge data now has to be daily values, but this can be mad emore general

#input: timeseries discharge, corresponding time vector, yearvector , b , H


def detect_freshwater_pulse(times,disc,b,H,river): 
    years = np.array(pd.to_datetime(times-719529, unit='D').year)
    #calculate yearly average
    ya_disc = np.zeros(int(years[-1]-years[0])) #empty array for yearly average discharge 
    ya_t = np.arange(int(years[0]),int(years[-1])) #time vector for years
    events = []  # np.array(np.zeros(int(RD_Pedro_2[-1,0]-RD_Pedro_2[0,0])),dtype=object)#events grouped in this array
    #eve_Pedro_2,eve_Pedro_3 = [] , []
    
    for year in ya_t:
        y_ind = np.where(years==year)
        ya_disc[int(year-years[0])] = np.nanmean(disc[np.where((years>year-5) & (year<year+5))])
        
        #calculate when above long-yearly average
        #plt.plot(RD_Pedro_2[:,-1][y_ind])
        event = np.where(disc[y_ind] > np.nanmean(disc[np.where((years>year-5) & (year<year+5))])*3)[0]
        if len(event) == 0: continue #if there are no events detected, go to next year
        
        #split in seperate events
        event2 = []
        temp= []
        for i in range(len(event)-1):
            temp.append(event[i])
            if event[i] != event[i+1]-1:
                event2.append(temp)
                temp=[]
                
        temp.append(event[-1])
        event2.append(temp)    
        
        #concatenate events when there is 2 days or shorter between them
        event3 = []
        temp = [event2[0]]
        for i in range(len(event2)-1):
            if event2[i+1][0]<event2[i][-1]+3: temp.append(event2[i+1])
            else:
                event3.append([item for sublist in temp for item in sublist])
                temp = [event2[i+1]]
        event3.append([item for sublist in temp for item in sublist])
        
        #throw away pulses longer than 31 days
        event4 = []
        for i in range(len(event3)):
            if  len(event3[i])<31:# and len(event3[i])>2 :
                event4.append(event3[i])
        if len(event4) == 0: continue #if there are no events detected, go to next year
        
        #save the river discharge data
        for i in range(len(event4)):
            #calculate the time when this happened
            Qt = times[y_ind[0][0]] + event4[i][0] 
            #calculate the river discharge during this peak
            Qp = np.nanmean(disc[y_ind][event4[i]])
            Qp_std = np.nanstd(disc[y_ind][event4[i]])
            #calculate the river discharge before the peak
            Qbc = np.nanmean(disc[int(y_ind[0][0] + event4[i][0] - 17):int(y_ind[0][0] + event4[i][0]-3)]) #here we assume that the discharge data are daily values
            Qbc_std = np.nanstd(disc[int(y_ind[0][0] + event4[i][0] - 17):int(y_ind[0][0] + event4[i][0]-3)]) #here we assume that the discharge data are daily values
            
            dur = len(event4[i])
            
            events.append([Qt,Qp,Qbc,Qp_std,Qbc_std,dur])
            
    events = np.array(events)

    #calculate dimensionless parameters
    g,Be,soc = 9.81 , 7.6e-4 , 35
    events = np.concatenate((events,np.zeros((len(events),4))),1)
    events[:,6:] = events[:,1:5]/(b*H*(g*Be*H*soc)**.5)

    #flag events where the river discharge does not triple
    events = np.concatenate((events,np.zeros((len(events),1))),1)
    events[np.where(events[:,1]/events[:,2]<3),-1] = 1
    '''
    #plot in regime diagram
    plt.title(river+ ', N = '+str(len(events)))
    plt.errorbar(events[:,6], events[:,7],fmt='o',c='black',xerr=events[:,7],yerr=events[:,8])
    plt.grid()
    plt.xlabel(r'$Fr_{R,p}$ '), plt.ylabel(r'$Fr_{R,bc}$ ')
    #plt.text(0,0, 'N = '+str(len(events)))
    plt.show()
    #'''

    return events #shape: time, peak Q, background Q, std peak Q, std background Q, flag if discharge triples (1 if it does not)

ev_gd = detect_freshwater_pulse(t_gd,Q_gd,500,6.5,'Guadiana')
ev_gu = detect_freshwater_pulse(t_gu,Q_gu,500,7.1,'Guadalquivir')
#ev_ga = detect_freshwater_pulse(t_ga,Q_ga,5000,9,'Garonne')
#ev_do = detect_freshwater_pulse(t_do,Q_do,5000,9,'Dordogne')
ev_gi = detect_freshwater_pulse(t_gi,Q_gi,5000,9,'Gironde')
ev_sf = detect_freshwater_pulse(t_sf,Q_sf,3000,12,'San Francisco Bay') #width was 6000 m
ev_ta = detect_freshwater_pulse(t_ta,Q_ta,1800,5.1,'Tagus')
ev_du = detect_freshwater_pulse(t_du,Q_du,271,10,'Douro')
#ev_br = detect_freshwater_pulse(t_br,Q_br,200,6,'Brazos')
ev_mi = detect_freshwater_pulse(t_mi,Q_mi,1000,7,'Mitchel')


#%%remove events where the river discharge does not triple
#Guadiana
markersize = 20
plt.figure(figsize=(10,7))

temp = ev_gd.copy()
temp[np.where(ev_gd==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='Guadiana',c='b',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_gd.copy()
temp[np.where(ev_gd==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='b',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'
temp = ev_gd.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='b',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'

#Guadalquivir
temp = ev_gu.copy()
temp[np.where(ev_gu==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='Guadalquivir',c='g',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_gu.copy()
temp[np.where(ev_gu==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='g',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])label='Guadalquivir, <3x',
temp = ev_gu.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='g',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'

#Gironde
temp = ev_gi.copy()
temp[np.where(ev_gi==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='Gironde',c='r',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_gi.copy()
temp[np.where(ev_gi==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='r',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Gironde, <3x'
temp = ev_gi.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='r',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'

#San Francisco
temp = ev_sf.copy()
temp[np.where(ev_sf==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='San Francisco Bay',c='m',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_sf.copy()
temp[np.where(ev_sf==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='m',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='San Francisco, <3x'
temp = ev_sf.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='m',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'

#Tagus
temp = ev_ta.copy()
temp[np.where(ev_ta==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='Tagus',c='c',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_ta.copy()
temp[np.where(ev_ta==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='c',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='San Francisco, <3x'
temp = ev_ta.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='c',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'

#Mitchell
temp = ev_mi.copy()
temp[np.where(ev_mi==1.)[0]] = None
temp[np.where(temp[:,6]>0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],label='Mitchell River',c='orange',marker='D',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8])
temp = ev_mi.copy()
temp[np.where(ev_mi==0.)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='orange',marker='x',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='San Francisco, <3x'
temp = ev_mi.copy()
temp[np.where(temp[:,6]<0.3)[0]] = None
plt.scatter(temp[:,6], temp[:,7],c='orange',marker='o',s=markersize)#,c='black',xerr=events[:,7],yerr=events[:,8]),label='Guadiana, <3x'


#%%calculate which part of the freshwater pulses reach equilibrium and which part not. 

#this prints the length of the pulse, FrRp, FrRbc
#print(ev_gd[:,[5,6,7]])


#theoretical value
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Freshwater pulses/new_connect_regdiag.py')
runcell('calculate numbers - adjustment time, change in salt intrusion length, recovery time, #1', '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Freshwater pulses/new_connect_regdiag.py')

#print(Ta2, FrRp,FrRbc)

#calculate closest point. 
ev_gd2 = np.delete(ev_gd, np.where(ev_gd[:,-1]==1)[0],0) #remove elements which do not start from a steady state ev_gd.copy() #
eq_gd = np.zeros(len(ev_gd2))
for i in range(len(ev_gd2)):
    ind = np.argmin((ev_gd2[i,6]-FrRp)**2+ (ev_gd2[i,7]-FrRbc)**2)
    eq_gd[i] = Ta2[ind]< ev_gd2[i,5]
    #print('Equilibrium?' , Ta2[ind]< ev_gd[i,5])
print('for the Guadiana ', len(np.where(eq_gd==True)[0])/len(eq_gd) , 'of the FWPs reaches equilibrium \n')

ev_gu2 = np.delete(ev_gu, np.where(ev_gu[:,-1]==1)[0],0) #ev_gu.copy() #
eq_gu = np.zeros(len(ev_gu2))
for i in range(len(ev_gu2)):
    ind = np.argmin((ev_gu2[i,6]-FrRp)**2+ (ev_gu2[i,7]-FrRbc)**2)
    eq_gu[i] = Ta2[ind]< ev_gu2[i,5]
    #print('Equilibrium?' , Ta2[ind]< ev_gu[i,5])
print('for the Guadalquivir ', len(np.where(eq_gu==True)[0])/len(eq_gu) , 'of the FWPs reaches equilibrium \n')

ev_gi2 = np.delete(ev_gi, np.where(ev_gi[:,-1]==1)[0],0) #ev_gi.copy() #
eq_gi = np.zeros(len(ev_gi2))
for i in range(len(ev_gi2)):
    ind = np.argmin((ev_gi2[i,6]-FrRp)**2+ (ev_gi2[i,7]-FrRbc)**2)
    eq_gi[i] = Ta2[ind]< ev_gi2[i,5]
    #print('Equilibrium?' , Ta2[ind]< ev_gi[i,5])
print('for the Gironde ', len(np.where(eq_gi==True)[0])/len(eq_gi) , 'of the FWPs reaches equilibrium \n')

ev_sf2 = np.delete(ev_sf, np.where(ev_sf[:,-1]==1)[0],0) #ev_sf.copy() #
eq_sf = np.zeros(len(ev_sf2))
for i in range(len(ev_sf2)):
    ind = np.argmin((ev_sf2[i,6]-FrRp)**2+ (ev_sf2[i,7]-FrRbc)**2)
    eq_sf[i] = Ta2[ind]< ev_sf2[i,5]
    #print('Equilibrium?' , Ta2[ind]< ev_sf[i,5])
print('for San Francisco Bay ', len(np.where(eq_sf==True)[0])/len(eq_sf) , 'of the FWPs reaches equilibrium \n')

ev_ta2 = np.delete(ev_ta, np.where(ev_ta[:,-1]==1)[0],0) #ev_ta.copy() #
eq_ta = np.zeros(len(ev_ta2))
for i in range(len(ev_ta2)):
    ind = np.argmin((ev_ta2[i,6]-FrRp)**2+ (ev_ta2[i,7]-FrRbc)**2)
    eq_ta[i] = Ta2[ind]< ev_ta2[i,5]
    #print('Equilibrium?' , Ta2[ind]< ev_ta[i,5], Ta2[ind],ev_ta[i,5])
print('for the Tagus ', len(np.where(eq_ta==True)[0])/len(eq_ta) , 'of the FWPs reaches equilibrium \n')

ev_mi2 = np.delete(ev_mi, np.where(ev_mi[:,-1]==1)[0],0) #ev_mi.copy() #
eq_mi = np.zeros(len(ev_mi2))
for i in range(len(ev_mi2)):
    ind = np.argmin((ev_mi2[i,6]-FrRp)**2+ (ev_mi2[i,7]-FrRbc)**2)
    eq_mi[i] = Ta2[ind]< ev_mi2[i,5]
    #print('Equilibrium?' , mi2[ind]< ev_mi[i,5], mi2[ind],ev_mi[i,5])
print('for the Mitchell river ', len(np.where(eq_mi==True)[0])/len(eq_mi) , 'of the FWPs reaches equilibrium \n')







