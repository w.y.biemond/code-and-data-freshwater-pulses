#powerlaw fits
"""
Created on Wed Mar 23 08:23:23 2022

@author: biemo004
"""
#all figures paper 
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

#to make one plot, weneed to save all the plotting sdata
plotdata = []

#load data from experiment Background
outall = []
run0,fold = 0 , 'defi_tide1'
for i in range(121):
    outall.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))

#calculate numbers - adjustment time, change in salt intrusion length, recovery time
    
def calc_numbers(Ti, Li,Ui):
    L_min, L_max = np.min(Li) , np.max(Li)
    
    #adjustment time
    Ta = Ti[np.where(Li<L_min+(1-0.9)*(L_max-L_min))[0][0]] - Ti[np.where(Ui>Ui[0])[0][0]]
    #change in salt intrusion length
    dL = L_max - L_min
    #recovery time
    Tr = Ti[np.where(Li<L_max-(1-0.9)*(L_max - L_min))[0][-1]] - Ti[np.where(Li == L_min)[0][-1]] 
    #scales of these variables

    #inital guess scale
    #adjustment time
    Ta_sc = dL*900/np.max(Ui)/3600/24
    #salt intrusion length
    dL_sc = L_max
    #recovery time
    Tr_sc = L_max*900/np.min(Ui)/3600/24
    #Tr_sc = 0.9*(L_max*1000)**4*48*cv*1*10 /(g*Be*1000*35**2)/3600/24
    
    #return  Ta,dL,Tr, 
    return Ta/Ta_sc , dL/dL_sc , Tr/Tr_sc , Ta, dL, Tr, L_max


Ta,dLr,Tr,FrRp,FrRbc,FrT = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Ta2, dL2, Tr2 = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Lm = np.zeros(len(outall))
for i in range(len(outall)):
    Ta[i],dLr[i],Tr[i], Ta2[i],dL2[i],Tr2[i],Lm[i]  = calc_numbers(outall[i][3]/24,-outall[i][4],outall[i][0][3]/(outall[i][1][0]*outall[i][1][1]))
    
    FrRp[i] = np.max(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrRbc[i] = np.min(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrT[i] = outall[i][0][0][0]/np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0])

# fit the backgroudn value
#excliude the points where FrRbg<0.005
Lm_1 = Lm[22:]
FrT_1 = FrT[22:]
FrRbc_1 = FrRbc[22:]
FrRp_1 = FrRp[22:]

def calc_rsq(para, x , y , func):
    inp=[x]
    for i in range(len(para)):inp.append(para[i])
    res = y - func(*inp)
    ss_res = np.sum(res**2)
    ss_tot = np.sum((y-np.mean(y))**2)
    return 1- ss_res/ss_tot



#fit the powerlaw
def testfunc(t, a, b, c):
    t1,t2 = t
    return a * t1**b * t2**c

params, params_covariance = sp.optimize.curve_fit(testfunc, (FrT_1, FrRbc_1), Lm_1)#  , p0=[0.001, 0, 0])
print(params,np.sqrt(np.diag(params_covariance)))
print(calc_rsq(params , (FrT_1, FrRbc_1) , Lm_1 , testfunc)  )


#fit the change in salt intrusion length
dL2_1 = dL2[22:]

#fit the powerlaw
def testfunc(X, a, b, c, d):
    x,y,z = X
    return  a*(x**b - y**c) * z**d
#good initial guess is important
params, params_covariance = sp.optimize.curve_fit(testfunc, (FrRp_1,FrRbc_1,FrT_1), dL2_1  , p0=[-10, -1/3, -1/3,-1 ])
print(params,np.sqrt(np.diag(params_covariance)))
print(calc_rsq(params , (FrRp_1,FrRbc_1,FrT_1), dL2_1 , testfunc)  )



#load the data from experiment Peak
outall = []
run0,fold = 0 , 'defi2'
for i in range(141):
    outall.append((np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_var.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_const.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/par_nd.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/time.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/raw_out.txt'),
                      np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/run'+str(run0 + i)+'/salt_mm.txt') ))

#calculate numbers - adjustment time, change in salt intrusion length, recovery time
    
def calc_numbers(Ti, Li,Ui):
    L_min, L_max = np.min(Li) , np.max(Li)
    
    #adjustment time
    Ta = Ti[np.where(Li<L_min+(1-0.9)*(L_max-L_min))[0][0]] - Ti[np.where(Ui>Ui[0])[0][0]]#(Ti[np.where(Ui>Ui[0])[0][0]-1]+.25)
    #change in salt intrusion length
    dL = L_max - L_min
    #recovery time
    Tr = Ti[np.where(Li<L_max-(1-0.9)*(L_max - L_min))[0][-1]] - Ti[np.where(Li == L_min)[0][-1]] #(Ti[np.where(Li == L_min)[0][-1]-1] +0.25)

    #scales of these variables
    #adjustment time
    Ta_sc = dL*900/np.max(Ui)/3600/24
    #salt intrusion length
    dL_sc = L_max
    #recovery time
    Tr_sc = L_max*900/np.min(Ui)/3600/24
    #Tr_sc = (L_max*dL*900*1000*48*cv*Ut)/(g*Be*soc*H**3)
    #return  Ta,dL,Tr, 
    return Ta/Ta_sc , dL/dL_sc , Tr/Tr_sc , Ta, dL, Tr, L_max


Ta,dLr,Tr,FrRp,FrRbc,FrT = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Ta2, dL2, Tr2 = np.zeros(len(outall)),np.zeros(len(outall)),np.zeros(len(outall))
Lm = np.zeros(len(outall))
for i in range(len(outall)):
    Ta[i],dLr[i],Tr[i], Ta2[i],dL2[i],Tr2[i],Lm[i]  = calc_numbers(outall[i][3]/24,-outall[i][4],outall[i][0][3]/(outall[i][1][0]*outall[i][1][1]))
    
    FrRp[i] = np.max(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrRbc[i] = np.min(outall[i][0][3])/(outall[i][1][0]*outall[i][1][1]*np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0]))
    FrT[i] = outall[i][0][0][0]/np.sqrt(9.81*7.6e-4*outall[i][1][1]*outall[i][0][1][0])


#fit the powerlaw
def testfunc(t, a, b):
    return (1-a*t**b )

#good initial guess is important
params, params_covariance = sp.optimize.curve_fit(testfunc, (FrRbc/FrRp), dLr , p0=[1,-1/3])
print(params,np.sqrt(np.diag(params_covariance)))
print(calc_rsq(params , (FrRbc/FrRp), dLr  , testfunc)  )
