#MacCready2007 
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
import scipy.integrate as integrate 

#%%load river discharge
#prebewerkign: files saved as xlsx files
Gergal = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Gergal.xlsx'))
Aznalc = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Aznalczar.xlsx'))
Guadaira = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Guadaira.xlsx'))
Aguila = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/TorredelAguila.xlsx'))
#distances from mouth - smallest width taken as start estuary
d_Azn = 17.8 #km
d_Agu = 48.1 #km
d_Gua = 58.9 #km
d_Ger = 91.7 #km 

dimn = [0,31,30,31,31,28,31,30,31,30,31,31,30]#days in month - first month is october here!!
diml = [0,31,30,31,31,29,31,30,31,30,31,31,30]#days in month - first month is october here!!

#Gergal
noy = 5 #number of years
year = 2008
Q_Ger = np.zeros(365*noy+2)
t_Ger = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Ger[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Gergal[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.plot(t_Ger,Q_Ger)

#Aznalc: 4 jaar: 2008, 2009  2011, 2012
noy=5
year = 2008
Q_Azn = np.zeros(365*noy+2)
t_Azn = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
tdp = tdp+365
for i in range(2):
    dim = diml if (year+3+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+(i+2)*31:1+(i+2)*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Azn[365*2+1:365*3+1] = -999
plt.plot(t_Azn,Q_Azn)

#Guadaira: 2 year: 2008-2009
noy=5
year = 2008
Q_Gua = np.zeros(365*5+2)
t_Gua = np.arange(365*5+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529 
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Gua[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Guadaira[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Gua[365*2+1:] = -999
    
plt.plot(t_Gua, Q_Gua)

#torre del Aguila
noy = 5 #number of years
year = 2008
Q_Agu = np.zeros(365*noy+2)
t_Agu = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Agu[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aguila[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.show()

#discharge main river
ds = 350#day at which the simulation starts
disc = sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/freshwater_discharges.mat')
Q_gu = np.array(disc['Q']).flatten()
Qt = np.array(disc['t']).flatten()
t2008 = pd.to_datetime('2008-01-01 00:00:00').value/(10**9*3600*24)+719529

#load tributaries
Q_Azn[np.where(Q_Azn==-999)], Q_Gua[np.where(Q_Gua==-999)], Q_Ger[np.where(Q_Ger==-999)] , Q_Agu[np.where(Q_Agu==-999)] = 0,0,0,0 #remove nans



#%%calculate coefficients

p2p4 = integrate.quad(lambda z: (1-9*z**2-8*z**3)*(-1/12 + 1/2*z**2 - 3/4 *z**4 -2/5*z**5)   , -1, 0) [0]
p1p3 = integrate.quad(lambda z: (1/2 - 3/2*z**2)*(-7/120 + 1/4*z**2 - 1/8*z**4)   , -1, 0) [0]
p2p3 = integrate.quad(lambda z: (1-9*z**2-8*z**3)*(-7/120 + 1/4*z**2 - 1/8*z**4)   , -1, 0) [0]
p1p4 = integrate.quad(lambda z: (1/2 - 3/2*z**2)*(-1/12 + 1/2*z**2 - 3/4 *z**4 -2/5*z**5)   , -1, 0) [0]

#print(p2p4)
#p2p4 = integrate.quad(lambda z: (8/5-54/5*z**2-8*z**3)*(-23/150 + 4/5*z**2 - 9/10 *z**4 -2/5*z**5)   , -1, 0) [0]
#print(p2p4)
#define solver qubic equations
def q_solv(a,b,c,d): #finds only the real solution, I think that is sufficient for now
    #define variables
    e=1/a * (c - b**2/(3*a))
    f=1/a * (d + 2*b**3/(27*a**2) - b*c/(3*a))
    w1 = -0.5*f + 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    w2 = -0.5*f - 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    z1,z2 = w1**(1/3),w2**(1/3) #we do not get all the solutions here 
    y1,y2 = z1-e/(3*z1) , z2-e/(3*z2)
    x1,x2 = y1-b/(3*a) , y2-b/(3*a)
    
    #check
    if z1 ==0. or z2==0. : print('error: qsolve does not work', x1,x2)
    #select the real solution
    return np.real(x1) if np.imag(x1)==0 else np.real(x2)

#%%values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante

#lets make a time integration of this
#for formula's: see first orange paper of my notebook

def MacCre04(Ut, soc, Q, L, bs, H):
    nx = int(L/dx)+1
    #make lists 
    b=bs[0] * np.exp(bs[1]**(-1)*(np.linspace(0,-L,nx)+L))
    print('The maximum width is ' , int(np.round(np.max(b))), 'm')
    #check numerical parameters
    if L%dx !=0: print( 'WARNING: L/dx is not an integer')
    
    #calculate parameters for the integration
    
    #diffusivity and stuff
    Av=cv*Ut*H  #vertical eddy viscosity coefficient
    Kv=Av/Sc #vertical eddy diffusivity coefficient
    Kh=ch*Ut*b #horizontal eddy diffusivity coefficient - if this changes the second order derivative changes as well!0
    c=np.sqrt(g*Be*soc*H) #typical fase velocity internal wave
    u_bar=Q/(b*H) #river flow velocity
    #print('The constant velocity is ', u_bar[0] , 'm/s (at x=?)') #works only for constant width and depth

    #length scales 
    #not-rounded ones
    LD = Kh/u_bar
    Le1= -p1p3*H**2/Kv*u_bar
    Le2= np.sqrt((-p1p4-p2p3)/(48*Kv*Av))*H**2*c
    Le3= (-p2p4*c/(48**2*u_bar*Av**2*Kv))**(1/3) * H**2*c

    #Do the initialisation step  - this qubic solver should work!
    init0 = q_solv(Le3[0]**3 , Le2**2+c**2*H**4/(720*Kv*Av) , Le1[0]+LD[0]+u_bar[0]*H**2/(15*Kv) , -1)
    #calculate the dimensionless salinity
    init1 = (Le3[0]*init0)**3 + (Le2*init0)**2 + (Le1[0]+LD[0])*init0
    init2 = (-bs[1]**(-1) * Le3[0]**3*init0**3 + (-2*bs[1]**(-1)*LD[0]+bs[1]**(-1)*Le1[0]+1)*init0 ) / (3*Le3[0]**3*init0**2 + 2*Le2**2*init0 +Le1[0] +LD[0])
    
    #initialise lists
    s_nd  = np.zeros(nx) #dimensionless salt concentration at the bottom
    ds_nd  = np.zeros(nx) #derivative of salt concentration
    ds2_nd = np.zeros(nx) #second derivative of salt concentration
    s_nd [0] = init1
    ds_nd [0] = init0
    ds2_nd[0] = init2 

    #Integrate the rest of the domain 
    #do the integration
    for i in range(nx-1): 
        sol_s = s_nd [i] - dx*ds_nd[i] + 1/2*dx**2*ds2_nd[i]
        sol_ds = q_solv(Le3[i+1]**3 , Le2**2 , Le1[i+1]+LD[i+1], -sol_s)
        sol_ds2 = (-bs[1]**(-1) * Le3[i+1]**3*sol_ds**3 + (-2*bs[1]**(-1)*LD[i+1]+bs[1]**(-1)*Le1[i+1]+1)*sol_ds ) / (3*Le3[i+1]**3*sol_ds**2 + 2*Le2**2*sol_ds +Le1[i+1] +LD[i+1])
        s_nd[i+1], ds_nd[i+1] , ds2_nd[i+1] = sol_s , sol_ds , sol_ds2
        
    return s_nd


def MacCre07(t_sim,dt,t_plot, params , init):
    
    Ut, soc,Q,L, bs, H = params
    #make lists 
    b=np.flip(bs[0] * np.exp(bs[1]**(-1)*(np.linspace(0,-L,int(L/dx+1))+L)))
    print('The maximum width is ' , int(np.round(np.max(b))), 'm')
    #check numerical parameters
    if L%dx !=0: print( 'WARNING: L/dx is not an integer')
    nx=int(L/dx)+1
    
    #diffusivity and stuff
    Av=cv*Ut*H  #vertical eddy viscosity coefficient
    Kv=Av/Sc #vertical eddy diffusivity coefficient
    Kh=ch*np.repeat(Ut,nx).reshape((tt,nx))*b #horizontal eddy diffusivity coefficient - if this changes the second order derivative changes as well!0
    c=np.sqrt(g*Be*soc*H) #typical fase velocity internal wave
    ubar=np.repeat(Q,nx).reshape((tt,nx))/(b*H)#river flow velocity
    #print('The constant velocity is ', u_bar[0] , 'm/s (at x=?)') #works only for constant width and depth


    #length scales 
    LD = Kh/ubar
    Le1= -p1p3*H**2/np.repeat(Kv,nx).reshape((tt,nx))*ubar
    Le2= np.sqrt((-p1p4-p2p3)/(48*Kv*Av))*H**2*c
    Le3= (-p2p4*np.repeat(c,nx).reshape((tt,nx))/(48**2*ubar*np.repeat(Av,nx).reshape((tt,nx))**2*np.repeat(Kv,nx).reshape((tt,nx))))**(1/3) * H**2*np.repeat(c,nx).reshape((tt,nx))

    def dSdx(S):
        outp= np.zeros(len(S))
        outp[1:-1] = (S[2:]-S[:-2])/(2*dx)
        outp[0] = (-S[2]+4*S[1]-3*S[0])/(2*dx)
        outp[-1] = (3*S[-1]-4*S[-2]+S[-3])/(2*dx)
        return outp
    
    def dS2dx(S):
        outp= np.zeros(len(S))
        outp[1:-1] = (S[2:]-2*S[1:-1]+S[:-2])/(dx**2)
        outp[0] = (S[0]-2*S[1]+S[2])/(2*dx**2) 
        outp[-1] = (S[-1]-2*S[-2]+S[-3])/(2*dx**2)
        return outp
  
    
    def u_func(z,S,t):
        return (3/2-3/2*z**2/H**2)*ubar[t] + g*Be*H**3/(48*Av[t]) * soc[t]*dSdx(S)*(1-9*z**2/H**2 - 8*z**3/H**3)
    
    def w_func(z,S,t):
        return (2*z**4/H**4 + 3*z**3/H**3 -z/H)* g*Be*H**4/(48*Av[t]) * soc[t] *(dS2dx(S) + b[1]**(-1)*dSdx(S))
        
    def s_func(z,S,t):
        return soc[t]*S + soc[t]*H**2/Kv[t] *dSdx(S)*(ubar[t]*(-7/120+1/4*z**2/H**2-1/8*z**4/H**4) +
                                                 g*Be*H**3/(48*Av[t])*soc[t]*dSdx(S)*(-1/12 + 1/2*z**2/H**2 - 3/4*z**4/H**4 - 2/5*z**5/H**5))
    
    def salt_flux(S,t): #assume S is 1D-array, S[0] = S[-L] actually.. be carefull this might give problems with notation
        #calculate spatial derivatives with centred finite differences
        dS = dSdx(S)
        dS2 = dS2dx(S)
        sf= ( -ubar[t] * bs[1]**(-1) * (Le3[t]**3*dS**3 + Le2[t]**2*dS**2 + (Le1[t]+LD[t])*dS - S)
            - ubar[t] * (3*Le3[t]**3*dS**2*dS2 + 2*Le2[t]**2*dS*dS2 + (Le1[t]+LD[t])*dS2 - dS)  )
        return -sf
    
    def RK4(S,t): #no time dependence in function, salt_flux() is the function
        k1 = dt*salt_flux(S,t)
        k2 = dt*salt_flux(S+k1/2,t)
        k3 = dt*salt_flux(S+k2/2,t)
        k4 = dt*salt_flux(S+k3,t)
        return S + 1/6 * (k1+2*k2+2*k3+k4)
    
    def S_river(S1):
        return 0
    
    def S_sea(S1,t):
        A = (c[t]**2*H**4)/(720*dx**2*Av[t]*Kv[t])
        B = -(S1*c[t]**2*H**4)/(360*dx**2*Av[t]*Kv[t]) + (H**2*ubar[t,-1])/(15*Kv[t]*dx) +1
        C = (S1**2*c[t]**2*H**4)/(720*dx**2*Av[t]*Kv[t]) - (H**2*ubar[t,-1]*S1)/(15*Kv[t]*dx) - 1
        return (-B+np.sqrt(B**2-4*A*C)) / (2*A) #returning this solution (and abandoning the other one) seems reasonable, since this value is higher than S1, and we expect Sigma to increase towards the sea. However, this is mathematically not directly evident...
    
    t_sim2 = t_sim*3600*24
    #dt=dt*3600
    
    #initialise model
    init[-1], init[0] = S_sea(init[-2],0) , S_river(0)
    #return RK4(S_ini)
    #new output
    S_outp, dS_outp,dS2_outp = np.zeros((int(t_sim2/dt),nx)),np.zeros((int(t_sim2/dt),nx)),np.zeros((int(t_sim2/dt),nx))
    S_outp[0], dS_outp[0], dS2_outp[0] = init, dSdx(init) , dS2dx(init)
    
    #intialise output
    u,w,s = np.zeros((int(t_sim2/dt/t_plot),nx,nz+1)), np.zeros((int(t_sim2/dt/t_plot),nx,nz+1)), np.zeros((int(t_sim2/dt/t_plot),nx,nz+1)) 
    for i in range(nz+1): #actual variables
        u[0,:,i] = u_func(-H/nz*i,init ,0)
        w[0,:,i] = w_func(-H/nz*i,init ,0)
        s[0,:,i] = s_func(-H/nz*i,init ,0)
    
    tijd = time.time()
    #actual integration
    for t in range(1,int(t_sim2/dt)):
        S_outp[t] = RK4(S_outp[t-1],t)
        #print(S_outp[t,-1],S_outp[t,0], S_sea(S_outp[t,-2]) , S_river(0) )
        S_outp[t,-1],S_outp[t,0] = S_sea(S_outp[t,-2],t) , S_river(0) #apply boundary conditions
        dS_outp[t] = dSdx(S_outp[t])        
        dS2_outp[t] = dS2dx(S_outp[t])   
        
        if t%t_plot ==0: #only calculate variables one in t_plot time steps
            for i in range(nz+1): #actual variables 
                u[int(t/t_plot),:,i] = u_func(-H/nz*i,S_outp[t] ,t)
                w[int(t/t_plot),:,i] = w_func(-H/nz*i,S_outp[t] ,t)
                s[int(t/t_plot),:,i] = s_func(-H/nz*i,S_outp[t] ,t)
            
        #if np.min(s[t+1])<-0.0001:   print('There are negative salt concentrations. The most negative value is ', np.round(np.min(s),3), 'psu at t = ',t )
        #if np.max(s[t+1])>35.0001:   print('There are too high salt concentrations. The most positive value is ', np.round(np.max(s),3), 'psu at t = ',t)
    print('The integration took '+ str(time.time()-tijd)+ ' seconds')
    
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    print(np.min(s[1:]))

    px,pz = np.linspace(-L/1000,0,nx) ,np.linspace(0,-H,nz+1)
    
    fig,ax = plt.subplots(figsize=(10,10))
    l1=ax.contourf(px,pz,  s[-1].transpose(), cmap='jet',levels=(np.linspace(0,35,11)))
    #ax.quiver(qx,qz,qu.transpose(),qw.transpose(),color='white', scale=2)
    cb0 = fig.colorbar(l1, ax=ax,orientation='horizontal', pad=0.16)
    cb0.set_label(label='Salinity [psu]',fontsize=16)
    ax.set_xlabel('Along-channel distance [km]',fontsize=16) 
    ax.set_ylabel('Vertical [m]',fontsize=16)    
    plt.show()
    
    #here is the long desired animation
    #do not plot all the time steps, but lets say every x time steps
    #'''
    x=np.linspace(-L/1000,0,nx)
    y=np.linspace(0,-H,nz+1)
    #xq=np.mean( np.reshape(np.linspace(-L/1000,0,nx),(int(L/dx/25),25)),1)
    
    def init():
        l0=ax.contourf(x,y,s[0].transpose(),cmap='Spectral_r',levels=(np.linspace(0,35,15)))
        cb=fig.colorbar(l0, ax=ax,orientation='horizontal', pad=0.16)
        cb.set_label(label='Salinity [psu]',fontsize=16)
        return #ax.cla()
    def animate(i):
        ax.clear()  
        l0=ax.contourf(x,y,s[i].transpose(),cmap='Spectral_r',levels=(np.linspace(0,35,15)))
        #ax.quiver(xq,y,np.mean( np.reshape(u[i].transpose(),(nz+1,int(L/dx/25),25)),2),np.mean( np.reshape(w[i].transpose(),(nz+1,int(L/dx/25),25)),2))
        ax.set_title('Situation after '+ str( np.round(dt*t_plot*i/3600/24,decimals=1)) +' days of simulation.',fontsize=16) 
        ax.set_xlim(-50,0)
        ax.set_xlabel('x [km]',fontsize=16)
        ax.set_ylabel('z [m]', fontsize=16)
        return ax
     
    tijd = time.time()
    fig = plt.figure(figsize=(10,10))
    fig.tight_layout()
    ax = fig.add_subplot(111)
    anim = ani.FuncAnimation(fig,animate,int(len(s)),init_func=init,blit=False)
    anim.save("/Users/biemo004/Documents/UU phd Saltisolutions/Output/Animations MC07/"+str('test_tanp01')+".mp4", fps=int(len(s)/10), extra_args=['-vcodec', 'libx264'])
    print('Making of the animation took '+ str(time.time()-tijd)+ ' seconds')
    
    #return S_outp
    #'''
    return s,u,w
    
#insert simulation time [days], time step[seconds], how often to plot(1 in this amount of time steps)
#params =[L,H,bR,mu_b,soc,Q]

#numerical constants
dx=250  #horizontal spatial step
nz=20 #vertical step

T = 100
dt = 15 #timestep in seconds
tt = int(T*3600*24/dt)

L = 110000
H = 7.1
bR = 150
be = 75000
soc = np.linspace(35,35,tt) #35 weet niet precies wat er gebeurt als je dit verandert met de schaling... 

Ut = np.linspace(1.15,1.15,tt) #1

def Q_build(Q1,Q2,dom): #river discharge 1, river 2 dicscharge, domain where the new river flos in

    #river discharge- let the dordogne come in later
    #Q = np.repeat((qdo+qga),int(24/DT))[:T] #2 years, starts 01-01-2017
    
    #build indices just like in normal code
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)

    Q = np.zeros((T,di[-1]))
    
    for x in range(di[-1]): #for every x
        Q[:,x] = Q1
        for d in range(len(dom)): #for every tributary
            if x>di[dom[d]]: #after 2 domains the Dordogne confluences with the Garonne
                Q[:,x] = Q[:,x] + Q2[d]
    return Q

#select right point of time
Q_gu_i = Q_gu[np.where(Qt==t2008)[0][0]+ds:np.where(Qt==t2008)[0][0]+T+ds]
Q_Azn_i = Q_Azn[np.where(t_Azn==t2008)[0][0]+ds:np.where(t_Azn==t2008)[0][0]+T+ds]
Q_Gua_i = Q_Gua[np.where(t_Gua==t2008)[0][0]+ds:np.where(t_Gua==t2008)[0][0]+T+ds]
Q_Ger_i = Q_Ger[np.where(t_Ger==t2008)[0][0]+ds:np.where(t_Ger==t2008)[0][0]+T+ds]
Q_Agu_i = Q_Agu[np.where(t_Agu==t2008)[0][0]+ds:np.where(t_Agu==t2008)[0][0]+T+ds]
Qh = Q_gu_i + Q_Azn_i + Q_Gua_i + Q_Ger_i + Q_Agu_i
Q = np.repeat(Qh,int(3600*24/dt))

inn = np.flip(MacCre04(Ut[0], soc[0], Q[0], L,[bR,be],H))
test = MacCre07(T,dt,int(6*3600/dt), (Ut,soc,Q,L,[bR,be],H),inn)

print(test[0].shape)
s_out = test[0].reshape((T*4,(4*110+1)*21))
#np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/'+str('MC07 comp/')+ str('guadal1.txt'),s_out, fmt='%s')







