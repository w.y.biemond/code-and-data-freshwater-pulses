#!/usr/bin/env python3

#here the code which is used to do one timestep 
#to be coupled with general model
#not so well explained code; for that, see the original file

runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
def adapt_dt3(inp_p, inp_chn, inp_t, term, init, dt_sc):
    #meaning input:
        #inp_p: physical parameters: (Ut, soc, Q, L, bs, H)
        #inp_n: numerical paramters: (dx, N, Lsc)
        #init: initial guess for solutions, in the right format - TO INCLUDE LATER
        #term: which terms of the equation are taken into account
        
    Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc = inp_p #1. , 35. , 150. , 100000. , [2000.,1e9] , 10.
    Ln, b0, bs, dxn = inp_chn
    T, dt,theta = inp_t
    if np.any(Ln%dxn!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln) != len(bs) or len(Ln)!=len(dxn) : print('ERROR: number of domains is not correct')
    
    dln = dxn/Lsc
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]

    M=N+1 
    #plotting parameters
    nz=25 #vertical step - only for plot
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    pz = np.linspace(-H,0,nz+1)
    
    #build parameters: convergence length
    bn = np.zeros(len(Ln))
    bn[0] = 9e99 if bs[0] == b0 else Ln[0]/np.log(bs[0]/b0)
    for i in range(1,len(Ln)):
        bn[i] = 9e99 if bs[i] == bs[i-1] else Ln[i]/np.log(bs[i]/bs[i-1])
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))

    u_bar = Q/(H*b)
    Kh = ch*Ut[:,np.newaxis]*b
    Av = cv*Ut[:,np.newaxis]*np.array([H]*np.sum(nxn))
    Kv = Av/Sc
    alf = g*Be*H**3/(48*Av)
    
    #lists of ks and ns and pis
    kkp = np.linspace(1,N,N)*np.pi #k*pi
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
    pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
    np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)
    
    #new partial slip coefficients
    #'''
    g1 = -1 + (1.5+3*Av/(sf[:,np.newaxis]*H)) / (1+ 3 *Av/(sf[:,np.newaxis]*H))
    g2 =  -3 / (2+6*Av/(sf[:,np.newaxis]*H))
    g3 = (1+4*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H)) * (9+18*Av/(sf[:,np.newaxis]*H)) - 8 - 24*Av/(sf[:,np.newaxis]*H)
    g4 = -9 * (1+4*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H))
    g5 = - 8
    g6 = 4+4*Av/(sf[:,np.newaxis]*H) -12*(0.5+Av/(sf[:,np.newaxis]*H))**2/(1+3*Av/(sf[:,np.newaxis]*H))
    g7 = 4
    g8 = (3+6*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H))
    #'''
    #g1,g2,g3,g4,g5 = 1/2 , -3/2 , 1, -9 ,-8
    
    #===========================================
    #BUILD COEFFICIENTS
    #for the definitions of the coefficients: see my pdf file
    if 1 in term: 
        C1a = np.zeros((T,di[-1]))
        for t3 in range(T):
            C1a[t3] = u_bar[t3]/2
    if 2 in term: 
        C2a, C2b, C2c, C2d = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T, di[-1], N, N))
        for t3 in range(T):
            C2a[t3] = u_bar[t3,:,np.newaxis] * (g1[t3,:,np.newaxis]/2 + g2[t3,:,np.newaxis]/6 + g2[t3,:,np.newaxis]/(4*kkp**2)) + (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) *(g6[t3,:,np.newaxis]/2-g7/4+g8[t3,:,np.newaxis]*(1/6+1/(4*kkp**2)))
            C2b[t3] = alf[t3,:, np.newaxis]*soc[t3]/Lsc * (g3[t3,:,np.newaxis]/2 + g4[t3,:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g5*(1/8 + 3/(8*kkp**2)) )
            C2c[t3] = (u_bar[t3,:,np.newaxis, np.newaxis] * g2[t3,:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 )  
                       + (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis,np.newaxis]) * (g7/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g8[t3,:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
            C2d[t3] = alf[t3,:, np.newaxis, np.newaxis] * soc[t3]/Lsc * (g4[t3,:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g5* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )     
        C2c[np.where(np.isnan(C2c))] = 0 
        C2d[np.where(np.isnan(C2d))] = 0
    if 3 in term:
        C3a, C3b = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N))
        for t3 in range(T):
            C3a[t3] = u_bar[t3,:,np.newaxis] *2*g2[t3,:,np.newaxis]/(kkp**2)*np.cos(kkp)+ (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (g7+(2*g8[t3,:,np.newaxis]-g7)*np.cos(kkp))/(kkp**2)
            C3b[t3] = alf[t3,:, np.newaxis] *soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/kkp**2 * np.cos(kkp) - g5/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )
    if 5 in term: 
        C5a, C5b, C5c, C5d, C5e, C5f = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N,N))
        for t3 in range(T):
            C5a[t3] = alf[t3,:,np.newaxis] *soc[t3]/Lsc * nnp* ( -9*g5+6*g4[t3,:,np.newaxis]+nnp**2*(-12*g3[t3,:,np.newaxis]-4*g4[t3,:,np.newaxis]+3*g5) ) / (48*nnp**3)
            C5b[t3] = alf[t3,:,np.newaxis] *soc[t3]*bex[:,np.newaxis]**(-1) * nnp* ( -9*g5+6*g4[t3,:,np.newaxis]+nnp**2*(-12*g3[t3,:,np.newaxis]-4*g4[t3,:,np.newaxis]+3*g5) ) / (48*nnp**3)
            C5c[t3] = alf[t3,:,np.newaxis,np.newaxis] *soc[t3]/Lsc*nnp* ( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) 
                            + np.cos(pkn)/pkn * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2) 
                            +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2))
            C5d[t3] = alf[t3,:,np.newaxis,np.newaxis] *soc[t3]*bex[:,np.newaxis,np.newaxis]**(-1)*nnp*( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) 
                            + np.cos(pkn)/pkn * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2) 
                            +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2))
            C5e[t3] = Lsc*bex[:,np.newaxis]**(-1) * (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (-g6[t3,:,np.newaxis]/4 + g7/8 + g8[t3,:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
            C5f[t3] = (Lsc*bex[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis,np.newaxis]) * nnp
              * ( ((g8[t3,:,np.newaxis,np.newaxis]-g7/2)*np.cos(pkn)-g7/2)/(pkn**3) + ((g8[t3,:,np.newaxis,np.newaxis]-g7/2)*np.cos(pnk)-g7/2)/(pnk**3) 
                 +(-g6[t3,:,np.newaxis,np.newaxis]/2+g7/4-g8[t3,:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g6[t3,:,np.newaxis,np.newaxis]/2+g7/4-g8[t3,:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )    
        C5c[np.where(np.isnan(C5c))] = 0 
        C5d[np.where(np.isnan(C5d))] = 0
        C5f[np.where(np.isnan(C5f))] = 0
    if 6 in term:
        C6a = np.zeros((T, di[-1], N))
        for t3 in range(T):
            C6a[t3] = Kv[t3, :, np.newaxis]*Lsc*kkp**2/(2*H**2)
    if 7 in term: 
        C7a, C7b = np.zeros((T,di[-1])) , np.zeros((T,di[-1]))
        for t3 in range(T):
            C7a[t3] = -bex**-1 * Kh[t3]
            C7b[t3] = -Kh[t3]/(2*Lsc)

    #always average salt
    C10a, C10b, C10c, C10d, C10e, C10f, C10g = np.zeros((T,di[-1])) , np.zeros((T,di[-1])) , np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)) , np.zeros((T,di[-1],N))
    for t3 in range(T):
        C10a[t3] = (u_bar[t3]-2*bex**(-1)*Kh[t3])
        C10b[t3] = -Kh[t3]/Lsc
        C10c[t3] = bex[:,np.newaxis]**(-1)*alf[t3,:, np.newaxis] *soc[t3] * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10d[t3] = alf[t3, :, np.newaxis] *soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10e[t3] = u_bar[t3,:, np.newaxis] *2*g2[t3,:,np.newaxis] / nnp**2 * np.cos(nnp) + r*CD*H*u_w[t3]**2*d_w[t3]/(2*Av[t3,:,np.newaxis]) * (g7+ (2*g8[t3,:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)
        C10f[t3] = alf[t3,:, np.newaxis]*soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10g[t3] = Lsc*bex[:,np.newaxis]**(-1)*(r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (g7+(2*g8[t3,:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)

    #===========================================

    #build indices for jacobian and solution vector
    #remove the boundary points
    di2 = np.zeros((len(di)-2)*2)
    for i in range(1,len(di)-1):
        di2[i*2-2] = di[i]-1
        di2[i*2-1] = di[i]
    di2 = np.array(di2, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    x = np.delete(np.arange(di[-1]),di2)[1:-1] # x coordinates for the points which are not on a aboundary
    xr = x.repeat(N) #x for N values, mostly i in old code
    xr_m = xr*M #M*i in old code, diagonal 
    xrm_m = (xr-1)*M #M*(i-1) in old code, left of the boundary
    xrp_m = (xr+1)*M #M*(i+1) in old code, right of the boundary
    xr_mj = xr_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*i+j in old code, diagonal fourier coefficients
    xrp_mj = xrp_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*(i-1)+j in old code, left fourier coefficients
    xrm_mj = xrm_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*(i+1)+j in old code, right fourier coefficients
    j1 = np.tile(np.arange(N),di[-1]-2-(len(di[1:-1])*2)) #j in old code
    
    #for the sums in the jacobian we need to repeat some arrays
    xr_mj2=np.repeat(xr_m,N)+np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))).repeat(N)
    xr2 = xr.repeat(N)
    j12 = j1.repeat(N)
    xr_m2 = xr_m.repeat(N)
    xrp_m2 = xrp_m.repeat(N)
    xrm_m2 = xrm_m.repeat(N)
    
    #and repeat of the repetition
    k1 = np.tile(j1,N)
    xr_mk=np.repeat(xr_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrp_mk=np.repeat(xrp_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrm_mk=np.repeat(xrm_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    #the same staory, but for the averag salt equation, which requires less repetition
    x_m = x*M
    xp_m = (x+1)*M
    xm_m = (x-1)*M
    
    #for the boundaries 
    bnd = (di[np.arange(1,len(nxn))]*M+np.arange(M)[:,np.newaxis]).flatten()
    bnd_dl = np.repeat(np.arange(1,len(nxn)),M) #not used yet

    #===========================================
    
    #build jacobian problem
    def jac_time(ans,terms,t):
        jac = np.zeros((di[-1]*M,di[-1]*M))
        
        if 1 in terms : #contribution to jacobian due to term T1
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C1a[t,xr]/(2*dl[xr]) #left of diagonal
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C1a[t,xr]/(2*dl[xr]) #right of diagonal
                   
        if 2 in terms: #contribution to jacobian due to term T2
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C2a[t,xr,j1]/(2*dl[xr]) - C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C2a[t,xr,j1]/(2*dl[xr]) + C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrm_m] = (jac[xr_mj,xrm_m] - C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                           - np.sum([C2d[t,xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj,xrp_m] = (jac[xr_mj,xrp_m]  + C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                                               + np.sum([C2d[t,xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj2,xrm_mk] = jac[xr_mj2,xrm_mk] - C2c[t,xr2,j12,k1]/(2*dl[xr2]) - C2d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 
            jac[xr_mj2,xrp_mk] = jac[xr_mj2,xrp_mk] + C2c[t,xr2,j12,k1]/(2*dl[xr2]) + C2d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 

        if 3 in terms: #contribution to jacobian due to term T3
            jac[xr_mj,xrm_m] = jac[xr_mj,xrm_m] - C3a[t,xr,j1]/(2*dl[xr]) - C3b[t,xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_m] = jac[xr_mj,xrp_m] + C3a[t,xr,j1]/(2*dl[xr]) + C3b[t,xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])


        if 5 in terms: #contribution to jacobian due to term T5
            jac[xr_mj, xrm_m] = (jac[xr_mj, xrm_m] + C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) - C5b[t,xr,j1]/(2*dl[xr])*ans[xr_mj]
                                   + np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0) 
                                   - np.sum([ans[xr_m+n]*C5d[t,xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj, xr_m] = jac[xr_mj, xr_m] - 2*C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) -2* np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
            jac[xr_mj, xrp_m] = (jac[xr_mj, xrp_m] + C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) + C5b[t,xr,j1]/(2*dl[xr])*ans[xr_mj]
                                           + np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
                                           + np.sum([ans[xr_m+n]*C5d[t,xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj,xr_mj] = (jac[xr_mj, xr_mj] + C5a[t,xr,j1]*(ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) 
                                             + C5b[t,xr,j1]*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) + C5e[t,xr,j1] )
            jac[xr_mj2, xr_mk] = (jac[xr_mj2, xr_mk] + C5c[t,xr2,j12,k1]*(ans[xrp_m2]-2*ans[xr_m2]+ans[xrm_m2])/(dl[xr2]**2)
                                                 + C5d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(2*dl[xr2]) +C5f[t,xr2,j12,k1] )

        if 6 in terms: #contribution to jacobian due to term T6
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] + C6a[t,xr,j1]

        if 7 in terms: 
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C7a[t,xr]/(2*dl[xr]) + C7b[t,xr] / (dl[xr]**2)
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] - 2*C7b[t,xr]/(dl[xr]**2)
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C7a[t,xr]/(2*dl[xr]) + C7b[t,xr] / (dl[xr]**2)
        
        #contribution to jacobian due to term the average salt equation - always present
        #left
        jac[x_m, xm_m] = (jac[x_m, xm_m] - C10a[t,x]/(2*dl[x]) + C10b[t,x]/(dl[x]**2) - 1/(2*dl[x])*np.sum([C10c[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       - 1/(2*dl[x])*np.sum([C10f[t,x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrm_mj] = jac[xr_m, xrm_mj] - C10e[t,xr,j1]/(2*dl[xr]) - C10f[t,xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        #center
        jac[x_m,x_m] = jac[x_m,x_m] - 2/(dl[x]**2)*C10b[t,x]  -2/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0)
        jac[xr_m, xr_mj] = jac[xr_m,xr_mj] + (ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) * C10c[t,xr,j1] + (ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) * C10d[t,xr,j1] + C10g[t,xr,j1]
        #right
        jac[x_m, xp_m] = (jac[x_m,xp_m] + C10a[t,x]/(2*dl[x]) + C10b[t,x]/(dl[x]**2) + 1/(2*dl[x])*np.sum([C10c[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(2*dl[x])*np.sum([C10f[t,x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrp_mj] = jac[xr_m,xrp_mj] + C10e[t,xr,j1]/(2*dl[xr]) + C10f[t,xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        
        jac = jac*theta #weighting of this part - should not apply to the boundaries, works if I place it here
        
        #river boundary - salt is prescribed
        jac[np.arange(M),np.arange(M)] = jac[np.arange(M),np.arange(M)] + 1
       
        #inner boundary 
        for d in range(1,len(nxn)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di[d]-1)*M+np.arange(M), (di[d]-3)*M+np.arange(M)] =  1/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-2)*M+np.arange(M)] = - 4/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-1)*M+np.arange(M)] =  3/(2*dl[di[d]-1]) 
            
            jac[(di[d]-1)*M+np.arange(M), di[d]*M+np.arange(M)] =  3/(2*dl[di[d]]) 
            jac[(di[d]-1)*M+np.arange(M), (di[d]+1)*M+np.arange(M)] =  - 4/(2*dl[di[d]])
            jac[(di[d]-1)*M+np.arange(M), (di[d]+2)*M+np.arange(M)] =  1/(2*dl[di[d]])

        #points equal
        jac[bnd, bnd-M] = 1                   
        jac[bnd, bnd] = -1

        #sea boundary
        jac[M*(di[-1]-1),M*(di[-1]-1)] = jac[M*(di[-1]-1),M*(di[-1]-1)] - 1
        jac[np.arange(M*(di[-1]-1)+1,M*di[-1]),np.arange(M*(di[-1]-1)+1,M*di[-1])] =  1

        #Time step        
        jac[x_m,x_m] = jac[x_m,x_m] + 1/dt * dt_sc
        jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] + 1/(2*dt) * dt_sc
             
        return jac
    #===========================================
    
    #solution formulas - not used anymore so often
    #inner points
    sol1 = {'se' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, x, t: (C10a[t,x]*(S1-S_1)/(2*dl[x]) + C10b[t,x]*(S1-2*S0+S_1)/(dl[x]**2) + (S1-S_1)/(2*dl[x]) * np.sum([C10c[t,x, n]*Sn0[n] for n in range(0,N)]) 
                                                                             + (S1-2*S0+S_1)/(dl[x]**2) * np.sum([C10d[t,x,n]*Sn0[n] for n in range(0,N)]) + np.sum([C10e[t,x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)]) 
                                                                             + (S1-S_1)/(2*dl[x]) * np.sum([C10f[t,x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)]) 
                                                                             + np.sum([C10g[t,x,n]*Sn0[n] for n in range(0,N)]) ) ,
            '1' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C1a[t,x] * ((Sk1-Sk_1)/(2*dl[x])) ,
            '2' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: (C2a[t,x,k]*(Sk1-Sk_1)/(2*dl[x]) + C2b[t,x,k]*(Sk1-Sk_1)/(2*dl[x])*(S1-S_1)/(2*dl[x])
                                                             + np.sum([C2c[t,x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])
                                                             + np.sum([C2d[t,x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])*(S1-S_1)/(2*dl[x])) ,
             '3' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C3a[t,x,k] * (S1-S_1)/(2*dl[x]) + C3b[t,x,k] * ((S1-S_1)/(2*dl[x]))**2 ,
             '5' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t:  (C5a[t,x,k]*(S1-2*S0+S_1)/(dl[x]**2)*Sk0 + C5b[t,x,k]*(S1-S_1)/(2*dl[x])*Sk0  
                                       +np.sum([C5c[t,x,k,n]*Sn0[n] for n in range(0,N)])*(S1-2*S0+S_1)/(dl[x]**2) 
                                       +np.sum([C5d[t,x,k,n]*Sn0[n] for n in range(0,N)])*(S1-S_1)/(2*dl[x]) 
                                       + C5e[t,x,k]*Sk0 + np.sum([C5f[t,x,k,n]*Sn0[n] for n in range(0,N)])) ,
             '6' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C6a[t,x,k]*Sk0 , 
             '7' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C7a[t,x]*(Sk1-Sk_1)/(2*dl[x]) + C7b[t,x]*(Sk1 - 2*Sk0 + Sk_1)/(dl[x]**2) 
             }
    #inner boundary - points and derivatives are equal
    sol_in2 =  {'se': lambda S_1, S0: S_1 - S0,
                'dse': lambda S_3,S_2,S_1,S0,S1,S2,d: (3*S_1-4*S_2+S_3)/(2*dl[di[d]-1]) - (-3*S0+4*S1-S2)/(2*dl[di[d]])
                }
    #===========================================
    
    #build solution vector 
    def sol_time(ans_n, ans_o, terms, t):
        so = np.zeros(di[-1]*M)
        
        #timedependence parts
        so[x_m ] = so[x_m ] + 1/dt*ans_n[x_m] * dt_sc - 1/dt*ans_o[x_m] * dt_sc
        so[xr_mj] = so[xr_mj] + 1/(2*dt)*ans_n[xr_mj] * dt_sc - 1/(2*dt)*ans_o[xr_mj] * dt_sc
        
        #river boundary - salt prescribed
        so[0] = ans_n[0]-sri[t]/soc[0]
        so[1:M] = ans_n[1:M]
                
        #average salt
        so[x_m] = so[x_m] + (1-theta)*(C10a[t,x]*(ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) + C10b[t,x]*(ans_o[xp_m] - 2*ans_o[x_m] + ans_o[xm_m])/(dl[x]**2) 
                    + (ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) * np.sum([C10c[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) 
                    + (ans_o[xp_m] - 2*ans_o[x_m] + ans_o[xm_m])/(dl[x]**2) * np.sum([C10d[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) 
                    + np.sum([C10e[t,x,n-1]*(ans_o[xp_m+n]-ans_o[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) 
                    + (ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) * np.sum([C10f[t,x,n-1]*(ans_o[xp_m+n]-ans_o[xm_m+n])/(2*dl[x]) for n in range(1,M)],0)
                    + np.sum([C10g[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) )    
        so[x_m] = so[x_m] + theta*(C10a[t,x]*(ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) + C10b[t,x]*(ans_n[xp_m] - 2*ans_n[x_m] + ans_n[xm_m])/(dl[x]**2) 
                    + (ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) * np.sum([C10c[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) 
                    + (ans_n[xp_m] - 2*ans_n[x_m] + ans_n[xm_m])/(dl[x]**2) * np.sum([C10d[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) 
                    + np.sum([C10e[t,x,n-1]*(ans_n[xp_m+n]-ans_n[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) 
                    + (ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) * np.sum([C10f[t,x,n-1]*(ans_n[xp_m+n]-ans_n[xm_m+n])/(2*dl[x]) for n in range(1,M)],0)
                    + np.sum([C10g[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) )   
        
        #perturbed salt
        if 1 in terms : #contribution to solution vector due to term T1
            so[xr_mj] = so[xr_mj] + (1-theta)* (C1a[t,xr] * ((ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr])))
            so[xr_mj] = so[xr_mj] + theta*    (C1a[t,xr] * ((ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr])))

        if 2 in terms: #contribution to solution vector due to term T2
            so[xr_mj] = so[xr_mj] + (1-theta)*(C2a[t,xr,j1]*(ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr])
                                    + C2b[t,xr,j1]*((ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr]))*((ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]))
                                    +np.sum([C2c[t,xr,j1,n-1] * (ans_o[xrp_m+n]-ans_o[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    +np.sum([C2d[t,xr,j1,n-1] * (ans_o[xrp_m+n]-ans_o[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    *(ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) )
            so[xr_mj] = so[xr_mj] + theta*   (C2a[t,xr,j1]*(ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr])
                                    + C2b[t,xr,j1]*((ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr]))*((ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]))
                                    +np.sum([C2c[t,xr,j1,n-1] * (ans_n[xrp_m+n]-ans_n[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    +np.sum([C2d[t,xr,j1,n-1] * (ans_n[xrp_m+n]-ans_n[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    *(ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) )

        if 3 in terms: #contribution to solution vector due to term T3
            so[xr_mj] = so[xr_mj] + (1-theta)*(C3a[t,xr,j1] * (ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) + C3b[t,xr,j1] * ((ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]))**2)
            so[xr_mj] = so[xr_mj] + theta*    (C3a[t,xr,j1] * (ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) + C3b[t,xr,j1] * ((ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]))**2)

        if 5 in terms: #contribution to jacobian due to term T5
            so[xr_mj] = so[xr_mj] + (1-theta)*(C5a[t,xr,j1]*(ans_o[xrp_m] - 2*ans_o[xr_m] + ans_o[xrm_m])/(dl[xr]**2)*ans_o[xr_mj] 
                                     + C5b[t,xr,j1]*(ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) * ans_o[xr_mj]  
                                       +np.sum([C5c[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0) * (ans_o[xrp_m] - 2*ans_o[xr_m] + ans_o[xrm_m])/(dl[xr]**2) 
                                       +np.sum([C5d[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0) * (ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr])
                                       + C5e[t,xr,j1]*ans_o[xr_mj] + np.sum([C5f[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0))
            so[xr_mj] = so[xr_mj] + theta*    (C5a[t,xr,j1]*(ans_n[xrp_m] - 2*ans_n[xr_m] + ans_n[xrm_m])/(dl[xr]**2)*ans_n[xr_mj] 
                                     + C5b[t,xr,j1]*(ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) * ans_n[xr_mj]  
                                       +np.sum([C5c[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0) * (ans_n[xrp_m] - 2*ans_n[xr_m] + ans_n[xrm_m])/(dl[xr]**2) 
                                       +np.sum([C5d[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0) * (ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr])
                                       + C5e[t,xr,j1]*ans_n[xr_mj] + np.sum([C5f[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0))

        if 6 in terms: #contribution to solution vector due to term T6
            so[xr_mj] = so[xr_mj] + (1-theta)*(C6a[t,xr,j1]*ans_o[xr_mj])
            so[xr_mj] = so[xr_mj] + theta*    (C6a[t,xr,j1]*ans_n[xr_mj])

        if 7 in terms: 
            so[xr_mj] = so[xr_mj] + (1-theta)*(C7a[t,xr]*(ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr]) + C7b[t,xr]*(ans_o[xrp_mj] - 2*ans_o[xr_mj] + ans_o[xrm_mj])/(dl[xr]**2) )
            so[xr_mj] = so[xr_mj] + theta*    (C7a[t,xr]*(ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr]) + C7b[t,xr]*(ans_n[xrp_mj] - 2*ans_n[xr_mj] + ans_n[xrm_mj])/(dl[xr]**2) )
                        
        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn)):
            for j in range(M):
                s_3, s_2, s_1 = ans_n[(di[d]-3)*M:(di[d]-2)*M], ans_n[(di[d]-2)*M:(di[d]-1)*M], ans_n[(di[d]-1)*M:di[d]*M]
                s0, s1, s2 = ans_n[di[d]*M:(di[d]+1)*M], ans_n[(di[d]+1)*M:(di[d]+2)*M], ans_n[(di[d]+2)*M:(di[d]+3)*M]
                so[(di[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],d)
                so[di[d]*M+j] =  sol_in2['se'](s_1[j],s0[j])
                
        #sea boundary
        so[M*(di[-1]-1)] = 1-ans_n[M*(di[-1]-1)]
        so[M*(di[-1]-1)+1:M*di[-1]] = ans_n[M*(di[-1]-1)+1:M*di[-1]]
    
        return so
    
    sss_o, sss_n = init, init
     
    tijd = time.time()
    for t2 in range(T):
        jaco, solu = jac_time(sss_n, term, t2) , sol_time(sss_n, sss_o ,term, t2)
        sss_i =sss_n - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)
        it=1
    
        # do the rest of the iterations
        while np.max(np.abs(sss_i-sss_n))>1e-6: #check whether the algoritm has converged
            sss_n = sss_i #update
            
            jaco, solu = jac_time(sss_n, term, t2) , sol_time(sss_n, sss_o ,term, t2)
            sss_i =sss_n - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)
            it=1+it

            #print('That was iteration step ', it)    
            if it>=10: break
        
        if it<10:
            print('Timestep', t2+1 , 'of total of', T, 'timesteps is finished. ') 
        else:
            print('ERROR: no convergence')            
            return np.array([None]*len(sss_n))


        sss_o, sss_n =sss_i, sss_i

    return sss_i
