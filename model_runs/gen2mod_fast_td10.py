#Galerkin method for all term equation salt balance

#included in this file: timestep is not constant anymore, but is an array and has to be put in as an array. LETS TRY...

#this is the best code I have so far
#it includes
#free choice of domains
#partial slip
#wind - not tested
#adaptive time step
#another river inflow (tributaries) can be taken into account (as long as there is no salt flux into that river)

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal 1#
cv=7.28e-5 #empirische constante 1e-4#
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

def gen_mod_timesim(inp_p, inp_chn, inp_t, term, init, save):
    #meaning input:
        #inp_p: physical parameters: (Ut, soc, sri, Q, u_w, d_w, sf, H, N Lsc)
        #inp_n: domain paramters: (Ln, b0, bs, dxn)
        #inp_t: time parameters(t,dt)
        #term: which terms of the equation are taken into account
        #init: initial guess for solutions, in the right format - TO INCLUDE LATER
        
    #load parameters function
    Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc = inp_p #1. , 35. , 150. , 100000. , [2000.,1e9] , 10.
    Ln, b0, bs, dxn = inp_chn
    T, dt, theta = inp_t #choose theta = 0.5 voor Crank-Nicolson, thetat = 1 voor Backward Euler
    M=N+1 #for later use 
    
    #do a few checks
    if np.any(Ln%dxn!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln) != len(bs) or len(Ln)!=len(dxn) : print('ERROR: number of domains is not correct')
    
    #===========================================
    #build grid parameters
    dln = dxn/Lsc #normalised spatial step
    nxn = np.array(Ln/dxn+1,dtype=int) #number of gridpoints
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    #make a list with normalised spatial step
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]
    
    #plotting parameters
    nz=20 #vertical number of points - only for plot
    #horizontal gridpoints
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    pz = np.linspace(-H,0,nz+1)
    
    #build parameters: convergence length
    bn = np.zeros(len(Ln))
    bn[0] = 9e99 if bs[0] == b0 else Ln[0]/np.log(bs[0]/b0)
    for i in range(1,len(Ln)):
        bn[i] = 9e99 if bs[i] == bs[i-1] else Ln[i]/np.log(bs[i]/bs[i-1])
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))
    print('The maximum width  is ' , int(np.round(np.max(b))), 'm')
    
    #===========================================
    #build parameters which will be used later
    u_bar = Q/(H*b) #depth-averaged velocity
    Kh = ch*Ut[:,np.newaxis]*b #horizontal diffusion coefficient
    Av = cv*Ut[:,np.newaxis]*np.array([H]*np.sum(nxn)) #vertical viscosity coefficient
    Kv = Av/Sc #vertical diffusion coefficient
    alf = g*Be*H**3/(48*Av) #parameter determining the strength of the density driven current
    
    #lists of ks and ns and pis - will be used later a lot
    kkp = np.linspace(1,N,N)*np.pi #k*pi
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
    pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
    np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)
    
    #Partial slip coefficients - from momentum equation solution
    g1 = -1 + (1.5+3*Av/(sf[:,np.newaxis]*H)) / (1+ 3 *Av/(sf[:,np.newaxis]*H))
    g2 =  -3 / (2+6*Av/(sf[:,np.newaxis]*H))
    g3 = (1+4*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H)) * (9+18*Av/(sf[:,np.newaxis]*H)) - 8 - 24*Av/(sf[:,np.newaxis]*H)
    g4 = -9 * (1+4*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H))
    g5 = - 8
    g6 = 4+4*Av/(sf[:,np.newaxis]*H) -12*(0.5+Av/(sf[:,np.newaxis]*H))**2/(1+3*Av/(sf[:,np.newaxis]*H))
    g7 = 4
    g8 = (3+6*Av/(sf[:,np.newaxis]*H)) / (1+3*Av/(sf[:,np.newaxis]*H))
    #g1,g2,g3,g4,g5 = 1/2 , -3/2 , 1, -9 ,-8 #for no slip
    
    #===========================================
    #BUILD COEFFICIENTS
    #for the definitions of the coefficients: see my pdf file
    if 1 in term: 
        C1a = np.zeros((T,di[-1]))
        for t3 in range(T):
            C1a[t3] = u_bar[t3]/2
    if 2 in term: 
        C2a, C2b, C2c, C2d = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T, di[-1], N, N))
        for t3 in range(T):
            C2a[t3] = u_bar[t3,:,np.newaxis] * (g1[t3,:,np.newaxis]/2 + g2[t3,:,np.newaxis]/6 + g2[t3,:,np.newaxis]/(4*kkp**2)) + (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) *(g6[t3,:,np.newaxis]/2-g7/4+g8[t3,:,np.newaxis]*(1/6+1/(4*kkp**2)))
            C2b[t3] = alf[t3,:, np.newaxis]*soc[t3]/Lsc * (g3[t3,:,np.newaxis]/2 + g4[t3,:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g5*(1/8 + 3/(8*kkp**2)) )
            C2c[t3] = (u_bar[t3,:,np.newaxis, np.newaxis] * g2[t3,:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 )  
                       + (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis,np.newaxis]) * (g7/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g8[t3,:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
            C2d[t3] = alf[t3,:, np.newaxis, np.newaxis] * soc[t3]/Lsc * (g4[t3,:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g5* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )     
        C2c[np.where(np.isnan(C2c))] = 0 
        C2d[np.where(np.isnan(C2d))] = 0
    if 3 in term:
        C3a, C3b = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N))
        for t3 in range(T):
            C3a[t3] = u_bar[t3,:,np.newaxis] *2*g2[t3,:,np.newaxis]/(kkp**2)*np.cos(kkp)+ (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (g7+(2*g8[t3,:,np.newaxis]-g7)*np.cos(kkp))/(kkp**2)
            C3b[t3] = alf[t3,:, np.newaxis] *soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/kkp**2 * np.cos(kkp) - g5/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )
    if 5 in term: 
        C5a, C5b, C5c, C5d, C5e, C5f = np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T, di[-1], N, N)) , np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N,N))
        for t3 in range(T):
            C5a[t3] = alf[t3,:,np.newaxis] *soc[t3]/Lsc * nnp* ( -9*g5+6*g4[t3,:,np.newaxis]+nnp**2*(-12*g3[t3,:,np.newaxis]-4*g4[t3,:,np.newaxis]+3*g5) ) / (48*nnp**3)
            C5b[t3] = alf[t3,:,np.newaxis] *soc[t3]*bex[:,np.newaxis]**(-1) * nnp* ( -9*g5+6*g4[t3,:,np.newaxis]+nnp**2*(-12*g3[t3,:,np.newaxis]-4*g4[t3,:,np.newaxis]+3*g5) ) / (48*nnp**3)
            C5c[t3] = alf[t3,:,np.newaxis,np.newaxis] *soc[t3]/Lsc*nnp* ( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) 
                            + np.cos(pkn)/pkn * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2) 
                            +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2))
            C5d[t3] = alf[t3,:,np.newaxis,np.newaxis] *soc[t3]*bex[:,np.newaxis,np.newaxis]**(-1)*nnp*( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) 
                            + np.cos(pkn)/pkn * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2) 
                            +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[t3,:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[t3,:,np.newaxis,np.newaxis]/6 - g3[t3,:,np.newaxis,np.newaxis]/2))
            C5e[t3] = Lsc*bex[:,np.newaxis]**(-1) * (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (-g6[t3,:,np.newaxis]/4 + g7/8 + g8[t3,:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
            C5f[t3] = (Lsc*bex[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis,np.newaxis]) * nnp
              * ( ((g8[t3,:,np.newaxis,np.newaxis]-g7/2)*np.cos(pkn)-g7/2)/(pkn**3) + ((g8[t3,:,np.newaxis,np.newaxis]-g7/2)*np.cos(pnk)-g7/2)/(pnk**3) 
                 +(-g6[t3,:,np.newaxis,np.newaxis]/2+g7/4-g8[t3,:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g6[t3,:,np.newaxis,np.newaxis]/2+g7/4-g8[t3,:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )    
        C5c[np.where(np.isnan(C5c))] = 0 
        C5d[np.where(np.isnan(C5d))] = 0
        C5f[np.where(np.isnan(C5f))] = 0
    if 6 in term:
        C6a = np.zeros((T, di[-1], N))
        for t3 in range(T):
            C6a[t3] = Kv[t3, :, np.newaxis]*Lsc*kkp**2/(2*H**2)
    if 7 in term: 
        C7a, C7b = np.zeros((T,di[-1])) , np.zeros((T,di[-1]))
        for t3 in range(T):
            C7a[t3] = -bex**-1 * Kh[t3]
            C7b[t3] = -Kh[t3]/(2*Lsc)

    #always average salt
    C10a, C10b, C10c, C10d, C10e, C10f, C10g = np.zeros((T,di[-1])) , np.zeros((T,di[-1])) , np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)), np.zeros((T,di[-1],N)) , np.zeros((T,di[-1],N))
    for t3 in range(T):
        C10a[t3] = (u_bar[t3]-2*bex**(-1)*Kh[t3])
        C10b[t3] = -Kh[t3]/Lsc
        C10c[t3] = bex[:,np.newaxis]**(-1)*alf[t3,:, np.newaxis] *soc[t3] * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10d[t3] = alf[t3, :, np.newaxis] *soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10e[t3] = u_bar[t3,:, np.newaxis] *2*g2[t3,:,np.newaxis] / nnp**2 * np.cos(nnp) + r*CD*H*u_w[t3]**2*d_w[t3]/(2*Av[t3,:,np.newaxis]) * (g7+ (2*g8[t3,:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)
        C10f[t3] = alf[t3,:, np.newaxis]*soc[t3]/Lsc * ( 2*g4[t3,:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
        C10g[t3] = Lsc*bex[:,np.newaxis]**(-1)*(r*CD*H*u_w[t3]**2*d_w[t3])/(4*Av[t3,:,np.newaxis]) * (g7+(2*g8[t3,:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)

    #===========================================

    #build indices for jacobian and solution vector
    #remove the boundary points
    di2 = np.zeros((len(di)-2)*2)
    for i in range(1,len(di)-1):
        di2[i*2-2] = di[i]-1
        di2[i*2-1] = di[i]
    di2 = np.array(di2, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    x = np.delete(np.arange(di[-1]),di2)[1:-1] # x coordinates for the points which are not on a aboundary
    xr = x.repeat(N) #x for N values, mostly i in old code
    xr_m = xr*M #M*i in old code, diagonal 
    xrm_m = (xr-1)*M #M*(i-1) in old code, left of the boundary
    xrp_m = (xr+1)*M #M*(i+1) in old code, right of the boundary
    xr_mj = xr_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*i+j in old code, diagonal fourier coefficients
    xrp_mj = xrp_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*(i-1)+j in old code, left fourier coefficients
    xrm_mj = xrm_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2)) #M*(i+1)+j in old code, right fourier coefficients
    j1 = np.tile(np.arange(N),di[-1]-2-(len(di[1:-1])*2)) #j in old code
    
    #for the sums in the jacobian we need to repeat some arrays
    xr_mj2=np.repeat(xr_m,N)+np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))).repeat(N)
    xr2 = xr.repeat(N)
    j12 = j1.repeat(N)
    xr_m2 = xr_m.repeat(N)
    xrp_m2 = xrp_m.repeat(N)
    xrm_m2 = xrm_m.repeat(N)
    
    #and repeat of the repetition
    k1 = np.tile(j1,N)
    xr_mk=np.repeat(xr_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrp_mk=np.repeat(xrp_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrm_mk=np.repeat(xrm_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    #the same staory, but for the averag salt equation, which requires less repetition
    x_m = x*M
    xp_m = (x+1)*M
    xm_m = (x-1)*M
    
    #for the boundaries 
    bnd = (di[np.arange(1,len(nxn))]*M+np.arange(M)[:,np.newaxis]).flatten()
    bnd_dl = np.repeat(np.arange(1,len(nxn)),M) #not used yet

    #===========================================
    
    #build jacobian problem
    def jac_time(ans,terms,t):
        jac = np.zeros((di[-1]*M,di[-1]*M))
        
        if 1 in terms : #contribution to jacobian due to term T1
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C1a[t,xr]/(2*dl[xr]) #left of diagonal
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C1a[t,xr]/(2*dl[xr]) #right of diagonal
                   
        if 2 in terms: #contribution to jacobian due to term T2
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C2a[t,xr,j1]/(2*dl[xr]) - C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C2a[t,xr,j1]/(2*dl[xr]) + C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrm_m] = (jac[xr_mj,xrm_m] - C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                           - np.sum([C2d[t,xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj,xrp_m] = (jac[xr_mj,xrp_m]  + C2b[t,xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                                               + np.sum([C2d[t,xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj2,xrm_mk] = jac[xr_mj2,xrm_mk] - C2c[t,xr2,j12,k1]/(2*dl[xr2]) - C2d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 
            jac[xr_mj2,xrp_mk] = jac[xr_mj2,xrp_mk] + C2c[t,xr2,j12,k1]/(2*dl[xr2]) + C2d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 

        if 3 in terms: #contribution to jacobian due to term T3
            jac[xr_mj,xrm_m] = jac[xr_mj,xrm_m] - C3a[t,xr,j1]/(2*dl[xr]) - C3b[t,xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_m] = jac[xr_mj,xrp_m] + C3a[t,xr,j1]/(2*dl[xr]) + C3b[t,xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])


        if 5 in terms: #contribution to jacobian due to term T5
            jac[xr_mj, xrm_m] = (jac[xr_mj, xrm_m] + C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) - C5b[t,xr,j1]/(2*dl[xr])*ans[xr_mj]
                                   + np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0) 
                                   - np.sum([ans[xr_m+n]*C5d[t,xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj, xr_m] = jac[xr_mj, xr_m] - 2*C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) -2* np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
            jac[xr_mj, xrp_m] = (jac[xr_mj, xrp_m] + C5a[t,xr,j1]*ans[xr_mj]/(dl[xr]**2) + C5b[t,xr,j1]/(2*dl[xr])*ans[xr_mj]
                                           + np.sum([ans[xr_m+n]*C5c[t,xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
                                           + np.sum([ans[xr_m+n]*C5d[t,xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj,xr_mj] = (jac[xr_mj, xr_mj] + C5a[t,xr,j1]*(ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) 
                                             + C5b[t,xr,j1]*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) + C5e[t,xr,j1] )
            jac[xr_mj2, xr_mk] = (jac[xr_mj2, xr_mk] + C5c[t,xr2,j12,k1]*(ans[xrp_m2]-2*ans[xr_m2]+ans[xrm_m2])/(dl[xr2]**2)
                                                 + C5d[t,xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(2*dl[xr2]) +C5f[t,xr2,j12,k1] )

        if 6 in terms: #contribution to jacobian due to term T6
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] + C6a[t,xr,j1]

        if 7 in terms: 
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C7a[t,xr]/(2*dl[xr]) + C7b[t,xr] / (dl[xr]**2)
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] - 2*C7b[t,xr]/(dl[xr]**2)
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C7a[t,xr]/(2*dl[xr]) + C7b[t,xr] / (dl[xr]**2)
        
        #contribution to jacobian due to term the average salt equation - always present
        #left
        jac[x_m, xm_m] = (jac[x_m, xm_m] - C10a[t,x]/(2*dl[x]) + C10b[t,x]/(dl[x]**2) - 1/(2*dl[x])*np.sum([C10c[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       - 1/(2*dl[x])*np.sum([C10f[t,x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrm_mj] = jac[xr_m, xrm_mj] - C10e[t,xr,j1]/(2*dl[xr]) - C10f[t,xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        #center
        jac[x_m,x_m] = jac[x_m,x_m] - 2/(dl[x]**2)*C10b[t,x]  -2/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0)
        jac[xr_m, xr_mj] = jac[xr_m,xr_mj] + (ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) * C10c[t,xr,j1] + (ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) * C10d[t,xr,j1] + C10g[t,xr,j1]
        #right
        jac[x_m, xp_m] = (jac[x_m,xp_m] + C10a[t,x]/(2*dl[x]) + C10b[t,x]/(dl[x]**2) + 1/(2*dl[x])*np.sum([C10c[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[t,x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(2*dl[x])*np.sum([C10f[t,x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrp_mj] = jac[xr_m,xrp_mj] + C10e[t,xr,j1]/(2*dl[xr]) + C10f[t,xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        
        jac = jac*theta #weighting of this part - should not apply to the boundaries, works if I place it here
        
        #river boundary - salt is prescribed
        jac[np.arange(M),np.arange(M)] = jac[np.arange(M),np.arange(M)] + 1
       
        #inner boundary 
        for d in range(1,len(nxn)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di[d]-1)*M+np.arange(M), (di[d]-3)*M+np.arange(M)] =  1/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-2)*M+np.arange(M)] = - 4/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-1)*M+np.arange(M)] =  3/(2*dl[di[d]-1]) 
            
            jac[(di[d]-1)*M+np.arange(M), di[d]*M+np.arange(M)] =  3/(2*dl[di[d]]) 
            jac[(di[d]-1)*M+np.arange(M), (di[d]+1)*M+np.arange(M)] =  - 4/(2*dl[di[d]])
            jac[(di[d]-1)*M+np.arange(M), (di[d]+2)*M+np.arange(M)] =  1/(2*dl[di[d]])

        #points equal
        jac[bnd, bnd-M] = 1                   
        jac[bnd, bnd] = -1

        #sea boundary
        jac[M*(di[-1]-1),M*(di[-1]-1)] = jac[M*(di[-1]-1),M*(di[-1]-1)] - 1
        jac[np.arange(M*(di[-1]-1)+1,M*di[-1]),np.arange(M*(di[-1]-1)+1,M*di[-1])] =  1

        #Time step        
        jac[x_m,x_m] = jac[x_m,x_m] + 1/dt[t] * Lsc*u_bar[0,0]/np.sum(Ln)
        jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] + 1/(2*dt[t]) * Lsc*u_bar[0,0]/np.sum(Ln)
             
        return jac
    #===========================================
    
    #solution formulas - not used anymore so often
    #inner points
    sol1 = {'se' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, x, t: (C10a[t,x]*(S1-S_1)/(2*dl[x]) + C10b[t,x]*(S1-2*S0+S_1)/(dl[x]**2) + (S1-S_1)/(2*dl[x]) * np.sum([C10c[t,x, n]*Sn0[n] for n in range(0,N)]) 
                                                                             + (S1-2*S0+S_1)/(dl[x]**2) * np.sum([C10d[t,x,n]*Sn0[n] for n in range(0,N)]) + np.sum([C10e[t,x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)]) 
                                                                             + (S1-S_1)/(2*dl[x]) * np.sum([C10f[t,x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)]) 
                                                                             + np.sum([C10g[t,x,n]*Sn0[n] for n in range(0,N)]) ) ,
            '1' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C1a[t,x] * ((Sk1-Sk_1)/(2*dl[x])) ,
            '2' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: (C2a[t,x,k]*(Sk1-Sk_1)/(2*dl[x]) + C2b[t,x,k]*(Sk1-Sk_1)/(2*dl[x])*(S1-S_1)/(2*dl[x])
                                                             + np.sum([C2c[t,x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])
                                                             + np.sum([C2d[t,x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])*(S1-S_1)/(2*dl[x])) ,
             '3' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C3a[t,x,k] * (S1-S_1)/(2*dl[x]) + C3b[t,x,k] * ((S1-S_1)/(2*dl[x]))**2 ,
             '5' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t:  (C5a[t,x,k]*(S1-2*S0+S_1)/(dl[x]**2)*Sk0 + C5b[t,x,k]*(S1-S_1)/(2*dl[x])*Sk0  
                                       +np.sum([C5c[t,x,k,n]*Sn0[n] for n in range(0,N)])*(S1-2*S0+S_1)/(dl[x]**2) 
                                       +np.sum([C5d[t,x,k,n]*Sn0[n] for n in range(0,N)])*(S1-S_1)/(2*dl[x]) 
                                       + C5e[t,x,k]*Sk0 + np.sum([C5f[t,x,k,n]*Sn0[n] for n in range(0,N)])) ,
             '6' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C6a[t,x,k]*Sk0 , 
             '7' : lambda S_1, S0, S1, Sk_1, Sk0, Sk1, Sn_1, Sn0, Sn1, k, x, t: C7a[t,x]*(Sk1-Sk_1)/(2*dl[x]) + C7b[t,x]*(Sk1 - 2*Sk0 + Sk_1)/(dl[x]**2) 
             }
    #inner boundary - points and derivatives are equal
    sol_in2 =  {'se': lambda S_1, S0: S_1 - S0,
                'dse': lambda S_3,S_2,S_1,S0,S1,S2,d: (3*S_1-4*S_2+S_3)/(2*dl[di[d]-1]) - (-3*S0+4*S1-S2)/(2*dl[di[d]])
                }
    #===========================================
    
    #build solution vector 
    def sol_time(ans_n, ans_o, terms, t):
        so = np.zeros(di[-1]*M)
        
        #timedependence parts
        so[x_m ] = so[x_m ] + 1/dt[t]*ans_n[x_m] * Lsc*u_bar[0,0]/np.sum(Ln) - 1/dt[t]*ans_o[x_m] * Lsc*u_bar[0,0]/np.sum(Ln)  
        so[xr_mj] = so[xr_mj] + 1/(2*dt[t])*ans_n[xr_mj] * Lsc*u_bar[0,0]/np.sum(Ln) - 1/(2*dt[t])*ans_o[xr_mj] * Lsc*u_bar[0,0]/np.sum(Ln)
        
        #river boundary - salt prescribed
        so[0] = ans_n[0]-sri[t]/soc[0]
        so[1:M] = ans_n[1:M]
                
        #average salt
        so[x_m] = so[x_m] + (1-theta)*(C10a[t,x]*(ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) + C10b[t,x]*(ans_o[xp_m] - 2*ans_o[x_m] + ans_o[xm_m])/(dl[x]**2) 
                    + (ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) * np.sum([C10c[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) 
                    + (ans_o[xp_m] - 2*ans_o[x_m] + ans_o[xm_m])/(dl[x]**2) * np.sum([C10d[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) 
                    + np.sum([C10e[t,x,n-1]*(ans_o[xp_m+n]-ans_o[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) 
                    + (ans_o[xp_m]-ans_o[xm_m])/(2*dl[x]) * np.sum([C10f[t,x,n-1]*(ans_o[xp_m+n]-ans_o[xm_m+n])/(2*dl[x]) for n in range(1,M)],0)
                    + np.sum([C10g[t,x,n-1]*ans_o[x_m+n] for n in range(1,M)],0) )    
        so[x_m] = so[x_m] + theta*(C10a[t,x]*(ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) + C10b[t,x]*(ans_n[xp_m] - 2*ans_n[x_m] + ans_n[xm_m])/(dl[x]**2) 
                    + (ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) * np.sum([C10c[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) 
                    + (ans_n[xp_m] - 2*ans_n[x_m] + ans_n[xm_m])/(dl[x]**2) * np.sum([C10d[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) 
                    + np.sum([C10e[t,x,n-1]*(ans_n[xp_m+n]-ans_n[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) 
                    + (ans_n[xp_m]-ans_n[xm_m])/(2*dl[x]) * np.sum([C10f[t,x,n-1]*(ans_n[xp_m+n]-ans_n[xm_m+n])/(2*dl[x]) for n in range(1,M)],0)
                    + np.sum([C10g[t,x,n-1]*ans_n[x_m+n] for n in range(1,M)],0) )   
        
        #perturbed salt
        if 1 in terms : #contribution to solution vector due to term T1
            so[xr_mj] = so[xr_mj] + (1-theta)* (C1a[t,xr] * ((ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr])))
            so[xr_mj] = so[xr_mj] + theta*    (C1a[t,xr] * ((ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr])))

        if 2 in terms: #contribution to solution vector due to term T2
            so[xr_mj] = so[xr_mj] + (1-theta)*(C2a[t,xr,j1]*(ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr])
                                    + C2b[t,xr,j1]*((ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr]))*((ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]))
                                    +np.sum([C2c[t,xr,j1,n-1] * (ans_o[xrp_m+n]-ans_o[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    +np.sum([C2d[t,xr,j1,n-1] * (ans_o[xrp_m+n]-ans_o[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    *(ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) )
            so[xr_mj] = so[xr_mj] + theta*   (C2a[t,xr,j1]*(ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr])
                                    + C2b[t,xr,j1]*((ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr]))*((ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]))
                                    +np.sum([C2c[t,xr,j1,n-1] * (ans_n[xrp_m+n]-ans_n[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    +np.sum([C2d[t,xr,j1,n-1] * (ans_n[xrp_m+n]-ans_n[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    *(ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) )

        if 3 in terms: #contribution to solution vector due to term T3
            so[xr_mj] = so[xr_mj] + (1-theta)*(C3a[t,xr,j1] * (ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) + C3b[t,xr,j1] * ((ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]))**2)
            so[xr_mj] = so[xr_mj] + theta*    (C3a[t,xr,j1] * (ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) + C3b[t,xr,j1] * ((ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]))**2)

        if 5 in terms: #contribution to jacobian due to term T5
            so[xr_mj] = so[xr_mj] + (1-theta)*(C5a[t,xr,j1]*(ans_o[xrp_m] - 2*ans_o[xr_m] + ans_o[xrm_m])/(dl[xr]**2)*ans_o[xr_mj] 
                                     + C5b[t,xr,j1]*(ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr]) * ans_o[xr_mj]  
                                       +np.sum([C5c[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0) * (ans_o[xrp_m] - 2*ans_o[xr_m] + ans_o[xrm_m])/(dl[xr]**2) 
                                       +np.sum([C5d[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0) * (ans_o[xrp_m]-ans_o[xrm_m])/(2*dl[xr])
                                       + C5e[t,xr,j1]*ans_o[xr_mj] + np.sum([C5f[t,xr,j1,n-1]*ans_o[xr_m+n] for n in range(1,M)],0))
            so[xr_mj] = so[xr_mj] + theta*    (C5a[t,xr,j1]*(ans_n[xrp_m] - 2*ans_n[xr_m] + ans_n[xrm_m])/(dl[xr]**2)*ans_n[xr_mj] 
                                     + C5b[t,xr,j1]*(ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr]) * ans_n[xr_mj]  
                                       +np.sum([C5c[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0) * (ans_n[xrp_m] - 2*ans_n[xr_m] + ans_n[xrm_m])/(dl[xr]**2) 
                                       +np.sum([C5d[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0) * (ans_n[xrp_m]-ans_n[xrm_m])/(2*dl[xr])
                                       + C5e[t,xr,j1]*ans_n[xr_mj] + np.sum([C5f[t,xr,j1,n-1]*ans_n[xr_m+n] for n in range(1,M)],0))

        if 6 in terms: #contribution to solution vector due to term T6
            so[xr_mj] = so[xr_mj] + (1-theta)*(C6a[t,xr,j1]*ans_o[xr_mj])
            so[xr_mj] = so[xr_mj] + theta*    (C6a[t,xr,j1]*ans_n[xr_mj])

        if 7 in terms: 
            so[xr_mj] = so[xr_mj] + (1-theta)*(C7a[t,xr]*(ans_o[xrp_mj]-ans_o[xrm_mj])/(2*dl[xr]) + C7b[t,xr]*(ans_o[xrp_mj] - 2*ans_o[xr_mj] + ans_o[xrm_mj])/(dl[xr]**2) )
            so[xr_mj] = so[xr_mj] + theta*    (C7a[t,xr]*(ans_n[xrp_mj]-ans_n[xrm_mj])/(2*dl[xr]) + C7b[t,xr]*(ans_n[xrp_mj] - 2*ans_n[xr_mj] + ans_n[xrm_mj])/(dl[xr]**2) )
                        
        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn)):
            for j in range(M):
                s_3, s_2, s_1 = ans_n[(di[d]-3)*M:(di[d]-2)*M], ans_n[(di[d]-2)*M:(di[d]-1)*M], ans_n[(di[d]-1)*M:di[d]*M]
                s0, s1, s2 = ans_n[di[d]*M:(di[d]+1)*M], ans_n[(di[d]+1)*M:(di[d]+2)*M], ans_n[(di[d]+2)*M:(di[d]+3)*M]
                so[(di[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],d)
                so[di[d]*M+j] =  sol_in2['se'](s_1[j],s0[j])
                
        #sea boundary
        so[M*(di[-1]-1)] = 1-ans_n[M*(di[-1]-1)]
        so[M*(di[-1]-1)+1:M*di[-1]] = ans_n[M*(di[-1]-1)+1:M*di[-1]]
    
        return so

    #===========================================
    
    #build function to calculate salinity
    def calc_salt(ss): #calculate salinity function
        s_b = np.transpose([np.reshape(ss,(di[-1],M))[:,0]]*(nz+1))
        sn = np.reshape(ss,(di[-1],M))[:,1:]
        s_p = np.array([np.sum([sn[i,n-1]*np.cos(np.pi*n/H*np.linspace(-H,0,nz+1)) for n in range(1,M)],0) for i in range(di[-1])])
        s = (s_b+s_p)*soc[0]
        s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
        return s
     
    #=========================================== 
    #the simulation starts
    tijd = time.time() #keep the time
    
    sss_o, sss_n = init, init #initialize
    sss_save, salt_mm = [] , []
    
    #the time loop
    for t2 in range(T): 
        #do the first iteration step
        jaco, solu = jac_time(sss_n, term, t2) , sol_time(sss_n, sss_o ,term, t2) #calculation of jacobian and solution vector
        sss_i =sss_n - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  #solve the matrix equation and use this for Newton Rapshon
        it=1 #count the number of iterations

        # do the rest of the iterations
        while np.max(np.abs(sss_i-sss_n))>1e-6: #check whether the algoritm has converged
            sss_n = sss_i #update
            
            jaco, solu = jac_time(sss_n, term, t2) , sol_time(sss_n, sss_o ,term, t2) #calculation of jacobian and solution vector
            sss_i =sss_n - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  #solve the matrix equation and use this for Newton Rapshon
            it=1+it #one more iteration has been done

            if it>=10: break #if it does not converge within 10 iteration steps, something is wrong
        
        s=calc_salt(sss_i) #calculate salinity
        
        if it<10 and np.min(s)>sri[t2]-1e-6: #if salinity is above its minimum value and we have had less than 10 iteration steps, the time step was succesfull and we can continue
            print('Timestep', t2+1 , 'of total of', T, 'timesteps is finished. ') 
                
        else: #if no convergence or too low salinity, are we going to use a smaller time step
            print('NO CONVERGENCE OR TOO LOW SALT; we are going to use adaptive time stepping')
            ite=2
            print('timestep is now '+str(1/ite)+'of the original')
            
            #Export the parameters to the adaptive time step function (which is almost identical to this one)
            
            #in earlier days this was interpolated as below. I believe this is not the right way. 
            '''
            Q_p2 = np.zeros((ite+1,di[-1]))#discharge special treatment because it depends on x
            for i in range(ite+1): Q_p2[i] = Q[t2-1]+(Q[t2]-Q[t2-1])*(i/ite)
            inp_p2 =  (np.linspace(Ut[t2-1], Ut[t2],ite+1)[1:] , np.linspace(soc[t2-1], soc[t2],ite+1)[1:] , np.linspace(sri[t2-1], sri[t2],ite+1)[1:] , Q_p2[1:] ,
                       np.linspace(u_w[t2-1], u_w[t2],ite+1)[1:] , np.linspace(d_w[t2-1], d_w[t2],ite+1)[1:] , np.linspace(sf[t2-1], sf[t2],ite+1)[1:] , H, N , Lsc)   
            inp_t2 = (ite,dt[t2]/ite, theta)
            '''
            inp_p2 =  (np.repeat(Ut[t2],ite) , np.repeat(soc[t2],ite) , np.repeat(sri[t2],ite) , np.tile(Q[t2],(ite,1)) ,
                       np.repeat(u_w[t2],ite) , np.repeat(d_w[t2],ite) , np.repeat(sf[t2],ite) , H, N , Lsc)   
            inp_t2 = (ite,dt[t2]/ite, theta)
            
            sss_i = adapt_dt3(inp_p2, inp_chn, inp_t2, term, sss_o, Lsc*u_bar[0,0]/np.sum(Ln)) #do the timestep with the other function
            sss_n = sss_i#update
            if sss_i[0]!= None :  s=calc_salt(sss_i)#calculate salinity   
             
            while sss_i[0]==None or np.min(s)<sri[t2]-1e-6 and ite<9: #chekc again and repeat this procedure if required
                ite = 2*ite
                print('timestep is now '+str(1/ite)+'of the original')
                
                #Export the parameters to the adaptive time step function (which is almost identical to this one)
                inp_p2 =  (np.repeat(Ut[t2],ite) , np.repeat(soc[t2],ite) , np.repeat(sri[t2],ite) , np.tile(Q[t2],(ite,1)) ,
                           np.repeat(u_w[t2],ite) , np.repeat(d_w[t2],ite) , np.repeat(sf[t2],ite) , H, N , Lsc)   
                inp_t2 = (ite,dt[t2]/ite, theta)
                    
                sss_i = adapt_dt3(inp_p2, inp_chn, inp_t2, term, sss_o, Lsc*u_bar[0,0]/np.sum(Ln)) #do the timestep with the other function
                sss_n = sss_i #update
                if sss_i[0]!= None :  s=calc_salt(sss_i)#calculate salinity   
                if ite>32: #if still no convergence after smaller timesteps. If the salinity is too low, the model ocnitunes, this is a non-fatal error (at least, in my opinion...)
                    print('FATAL ERROR: Also adaptive timestepping did not work')
                    return (Ut, soc, sri, Q, sf), (b0, H, N, Lsc, dt, T), np.array([Ln, bn, dxn]) , np.linspace(0, dt[t2]*(np.sum(Ln))/u_bar[0,0]*T/3600, T,endpoint=True) , np.array(sss_save)

            if ite>32: #if still no convergence after smaller timesteps. If the salinity is too low, the model ocnitunes, this is a non-fatal error (at least, in my opinion...)
                print('FATAL ERROR: Also adaptive timestepping did not work')
                return (Ut, soc, sri, Q, sf), (b0, H, N, Lsc, dt, T), np.array([Ln, bn, dxn]) , np.linspace(0, dt[t2]*(np.sum(Ln))/u_bar[0,0]*T/3600, T,endpoint=True) , np.array(sss_save)
            else:  print('Timestep', t2+1 , 'of total of', T, 'timesteps is finished. ') 
           
        sss_o, sss_n =sss_i, sss_i #update for next timestep
        sss_save.append(sss_n) , salt_mm.append([np.min(s),np.max(s)])

    print('Doing the time simulation takes  '+ str(time.time()-tijd)+ ' seconds. \n We have then simulated '+str(T)+' timesteps, in total '+ str(np.round(np.sum(dt)*np.sum(Ln)/u_bar[0,0]/3600/24)) + 'days')
    #save output. Note that discharge does not contain the information about the tributaries
    
    #time vector
    Tim = np.zeros(T+1)
    for i in range(T): Tim[i+1] = np.sum(dt[:i+1])*np.sum(Ln)*H*b0/(3600*Q[0,0])
    
    #output results
    if save == 'all': 
        return  (Ut, soc, sri, Q[:,-1], sf, u_w, d_w), (b0, H, N, Lsc, dt[0], T), np.array([Ln, bn, dxn]) , Tim[1:] , np.array(sss_save) #L_int #input, input, time series, output
    elif save == 'prep': #save preparation file - if we want to initalise a run with a time run, sometimes required when we can not spin up from nothing
        return sss_n
    else: 
        #optional: save only salt intrusion length 
        sbot = soc[:,np.newaxis]*(np.reshape(sss_save,(T,di[-1],M))[:,:,0] + np.sum(np.reshape(sss_save,(T,di[-1],M))[:,:,1:]*np.array([(-1)**n for n in range(1,M)]),2))
        Lint = np.zeros(len(sbot))
        for i in range(len(sbot)): Lint[i]=px[np.where(sbot[i]>2)[0][0]] + Ln[-1]/1000 
        return (Ut, soc, sri, Q[:,-1], sf, u_w, d_w), (b0, H, N, Lsc, dt[0], T), np.array([Ln, bn, dxn]) , Tim[1:] , Lint, salt_mm #L_int #input, input, time series, salt intrusion length
      


'''
#numerical parameters
N=5
Lsc=1000

#time parameters
DT = np.zeros(120)+12 #hours
T = len(DT)

#varying parameter
Ut = np.array([1]*T) #1.15+0.15*np.sin(4*np.pi/29.53 * (DT/24*np.arange(0,T,1)+9))
soc = np.array([35]*T)
sri = np.array([0]*T)
u_w = np.linspace(0,0,T)
d_w = np.linspace(1,1,T)
sf =2*cv*Ut

#depth, constant in all dimensions
H=7.1

#geometry
Ln = np.array([100000,100000,25000])
b0 = 1000
bs = np.array([b0,b0,b0*np.exp(10)])
dxn = np.array([100,500,500])

#River discharge 
#If you have just one discahrge, use just Q2= np.zeros(T)
#Q = np.repeat(Q_gu[np.where(Qt-t2008 == 0)[0][0]+710: np.where(Qt-t2008 == 0)[0][0]+int(T*DT/24)+710],int(24/DT))

def Q_build(Q1,Q2,dom): #river discharge 1, river 2 dicscharge, domain where the new river flos in
    #river discharge- let the dordogne come in later
    #Q = np.repeat((qdo+qga),int(24/DT))[:T] #2 years, starts 01-01-2017

    #build indices just like in normal code
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)

    Q = np.zeros((T,di[-1]))
    for x in range(di[-1]):
        Q[:,x] = np.repeat(Q1,int(24/DT))[:T]
        if x>di[dom]: #after 2 domains the Dordogne confluences with the Garonne
            Q[:,x] = Q[:,x] + np.repeat(Q2,int(24/DT))[:T]
    return Q
#Q = Q_build(Q_gu[np.where(Qt-t2008 == 0)[0][0]+710: np.where(Qt-t2008 == 0)[0][0]+int(T*DT/24)+710],np.zeros(T),2)
Qh = np.linspace(200,300,T)
nxn = np.array(Ln/dxn+1,dtype=int)
Q = np.repeat(Qh,np.sum(nxn)).reshape((T,np.sum(nxn)))

#calculate normalised time step
dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here
LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,5,3,6,7], MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0], u_w[0], d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,5,3,6,7]),None)

#%%
run = 6
os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run))
LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,5,3,6,7], MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0], u_w[0], d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,5,3,6,7]))
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run)+'/'
           +str('par_var.txt'),LL[0], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run)+'/'
           +str('par_const.txt'),LL[1], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run)+'/'
           +str('par_nd.txt'),LL[2], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run)+'/'
           +str('time.txt'),LL[3], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Testruns')+'/'+str('run')+str(run)+'/'
           +str('raw_out.txt'),LL[4], fmt='%s') 

print('run', run, ' is finished ')
#'''