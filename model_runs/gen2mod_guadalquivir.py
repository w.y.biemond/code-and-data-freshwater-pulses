#Guadalquivir simulation
#three runs: with simplified geometry, february 2009 pulse, 2010 pulses
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py') #load packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ti10.py') #load equilibrium model
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ats10.py') #load code to do adaptive time stepping
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_td10.py') #load model - second verison
t2008 = pd.to_datetime('2008-01-01 00:00:00').value/(10**9*3600*24)+719529

#load tributaries 
#prebewerkign: files saved as xlsx files
Gergal = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Gergal.xlsx'))
Aznalc = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Aznalczar.xlsx'))
Guadaira = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/Guadaira.xlsx'))
Aguila = np.array(pd.read_excel ('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/tributaries/tributaries - mean daily freshwater discharges/TorredelAguila.xlsx'))
#distances from mouth - smallest width taken as start estuary
d_Azn = 17.8 #km
d_Agu = 48.1 #km
d_Gua = 58.9 #km
d_Ger = 91.7 #km 

dimn = [0,31,30,31,31,28,31,30,31,30,31,31,30]#days in month - first month is october here!!
diml = [0,31,30,31,31,29,31,30,31,30,31,31,30]#days in month - first month is october here!!

#Gergal
noy = 5 #number of years
year = 2008
Q_Ger = np.zeros(365*noy+2)
t_Ger = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Ger[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Gergal[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.plot(t_Ger,Q_Ger)

#Aznalc: 4 jaar: 2008, 2009  2011, 2012
noy=5
year = 2008
Q_Azn = np.zeros(365*noy+2)
t_Azn = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
tdp = tdp+365
for i in range(2):
    dim = diml if (year+3+i)%4==0 else dimn
    for j in range(12):
        Q_Azn[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aznalc[1+(i+2)*31:1+(i+2)*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Azn[365*2+1:365*3+1] = -999
plt.plot(t_Azn,Q_Azn)

#Guadaira: 2 year: 2008-2009
noy=5
year = 2008
Q_Gua = np.zeros(365*5+2)
t_Gua = np.arange(365*5+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529 
tdp = 0 #total days passed
for i in range(2):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Gua[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Guadaira[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)
Q_Gua[365*2+1:] = -999
    
plt.plot(t_Gua, Q_Gua)

#torre del Aguila
noy = 5 #number of years
year = 2008
Q_Agu = np.zeros(365*noy+2)
t_Agu = np.arange(365*noy+2) + pd.to_datetime('2007-10-01 00:00:00').value/(10**9*3600*24)+719529
tdp = 0 #total days passed
for i in range(noy):
    dim = diml if (year+i)%4==0 else dimn
    for j in range(12):
        Q_Agu[tdp+np.sum(dim[:j+1]):tdp+np.sum(dim[:j+2])] = Aguila[1+i*31:1+i*31+dim[j+1] , 3+j]
    tdp = tdp+np.sum(dim)

plt.show()

Q_Azn[np.where(Q_Azn==-999)], Q_Gua[np.where(Q_Gua==-999)], Q_Ger[np.where(Q_Ger==-999)] , Q_Agu[np.where(Q_Agu==-999)] = 0,0,0,0 #remove nans

#discharge main river
disc = sp.io.loadmat('/Users/biemo004/Documents/UU phd Saltisolutions/Databestanden/Quadalquivir/BBiemond/freshwater_discharges.mat')
Q_gu = np.array(disc['Q']).flatten()
Qt = np.array(disc['t']).flatten()

#a few functions for the discharge
def Q_build(Q1,Q2,dom): #river discharge 1, river 2 dicscharge, domain where the new river flos in

    #river discharge- let the dordogne come in later
    #Q = np.repeat((qdo+qga),int(24/DT))[:T] #2 years, starts 01-01-2017
    
    #build indices just like in normal code
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)

    Q = np.zeros((T,di[-1]))
    
    for x in range(di[-1]): #for every x
        Q[:,x] = Q1
        for d in range(len(dom)): #for every tributary
            if x>di[dom[d]]: #after 2 domains the Dordogne confluences with the Garonne
                Q[:,x] = Q[:,x] + Q2[d]
    return Q

#new method: just copy the stuff
def outerpolate(t,Q): #replacement of the interpolate function. Does something like np.repeat
    outp=[] 
    T=1
    for i in range(len(t)):
        if t[i]<T:
            outp.append(Q[T-1])   
        else: 
            T= T+1
            outp.append(Q[T-1])
    return outp



# =============================================================================
# set up simulations
# =============================================================================


#%%run with simplified geometry
#numerical parameters
N=10
Lsc=1000

#time parameters
DT = np.concatenate([np.zeros(20*2)+12,np.zeros(60*12)+2,np.zeros(20*2)+12]) #hours
T=len(DT)

#varying parameter
Ut= 1.15+np.zeros(T)#+0.15*np.sin(4*np.pi/29.53 * (np.arange(0,100,0.25)+13))
soc = np.array([35]*T)
sri = np.array([0]*T) #np.array([0.5]*T)
sf =2*cv*Ut #np.concatenate([2*cv*Ut[:200], 1.5*cv*Ut[200:]])#2*cv*Ut np.array([9e99]*T)#

#depth, constant in all dimensions
H=7.1

#wind
u_w = np.zeros(T)
d_w =np.zeros(len(u_w))
d_w[np.where(u_w>0)[0]]=1
d_w[np.where(u_w<0)[0]]=-1

#geometry
Ln = np.array([75000,35000,25000])
b0 = 150
bs = np.array([408,650,650*np.exp(10)])
dxn = np.array([500,100,200]) 

#interpolate discharge to time-varying grid
Tim = np.zeros(T) #the time grid of the varying grid
for i in range(T): Tim[i] = np.sum(DT[:i])/24 

ds = 350#day at which the simulation starts

Q_gu_i = outerpolate(Tim,Q_gu[np.where(Qt==t2008)[0][0]+ds:np.where(Qt==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Azn_i = outerpolate(Tim,Q_Azn[np.where(t_Azn==t2008)[0][0]+ds:np.where(t_Azn==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Gua_i = outerpolate(Tim,Q_Gua[np.where(t_Gua==t2008)[0][0]+ds:np.where(t_Gua==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Ger_i = outerpolate(Tim,Q_Ger[np.where(t_Ger==t2008)[0][0]+ds:np.where(t_Ger==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Agu_i = outerpolate(Tim,Q_Agu[np.where(t_Agu==t2008)[0][0]+ds:np.where(t_Agu==t2008)[0][0]+int(np.sum(DT)/24)+ds])

#Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[2,3,4,5])
Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[0,0,0,0])
Q[:,0] = Q[:,1] #opvallend dat dit nodig is...

#Q = Q_build(Q_gu_i , [np.zeros(T)],[1])
#calculate normalised time step
dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here

#do the actual run
run = 1
os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run))
LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,3,5,6,7],
                     MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0], 0, d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,3,5,6,7]),'all')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_var.txt'),LL[0], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_const.txt'),LL[1], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_nd.txt'),LL[2], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('time.txt'),LL[3], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('raw_out.txt'),LL[4], fmt='%s') 

print('run', run, ' is finished ')


#%%run for 2009 freshwater pulse
#numerical parameters
N=10
Lsc=1000

#time parameters
DT = np.concatenate([np.zeros(20*2)+12,np.zeros(60*12)+2,np.zeros(20*2)+12]) #hours
T=len(DT)

#varying parameter
Ut= 1.15+np.zeros(T)#+0.15*np.sin(4*np.pi/29.53 * (np.arange(0,100,0.25)+13))
soc = np.array([35]*T)
sri = np.array([0]*T) #np.array([0.5]*T)
sf =2*cv*Ut #np.concatenate([2*cv*Ut[:200], 1.5*cv*Ut[200:]])#2*cv*Ut np.array([9e99]*T)#

#depth, constant in all dimensions
H=7.1

#wind
u_w = np.zeros(T)
d_w =np.zeros(len(u_w))
d_w[np.where(u_w>0)[0]]=1
d_w[np.where(u_w<0)[0]]=-1

#geometry
Ln = np.array([100000,12100,18500,10800,30300,17800,4000,25000])
b0 = 150
bs = np.array([150,190,300,450,500,800,3000,3000*np.exp(10)])
#dxn = np.array([1000,484,500,540,300,356,250,500]) #quite rude
#dxn = np.array([400,242,370,270,150,178,250,250]) #quite fine
dxn = np.array([400,242,185,135,75,89,125,250]) #very fine

#interpolate discharge to time-varying grid
Tim = np.zeros(T) #the time grid of the varying grid
for i in range(T): Tim[i] = np.sum(DT[:i])/24 

ds = 350#day at which the simulation starts

Q_gu_i = outerpolate(Tim,Q_gu[np.where(Qt==t2008)[0][0]+ds:np.where(Qt==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Azn_i = outerpolate(Tim,Q_Azn[np.where(t_Azn==t2008)[0][0]+ds:np.where(t_Azn==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Gua_i = outerpolate(Tim,Q_Gua[np.where(t_Gua==t2008)[0][0]+ds:np.where(t_Gua==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Ger_i = outerpolate(Tim,Q_Ger[np.where(t_Ger==t2008)[0][0]+ds:np.where(t_Ger==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Agu_i = outerpolate(Tim,Q_Agu[np.where(t_Agu==t2008)[0][0]+ds:np.where(t_Agu==t2008)[0][0]+int(np.sum(DT)/24)+ds])

Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[2,3,4,5])
#Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[0,0,0,0])
Q[:,0] = Q[:,1] #opvallend dat dit nodig is...

#Q = Q_build(Q_gu_i , [np.zeros(T)],[1])
#calculate normalised time step
dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here

#do the actual run
run = 2
os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run))
LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,3,5,6,7],
                     MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0], 0, d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,3,5,6,7]),'all')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_var.txt'),LL[0], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_const.txt'),LL[1], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_nd.txt'),LL[2], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('time.txt'),LL[3], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('raw_out.txt'),LL[4], fmt='%s') 

print('run', run, ' is finished ')

#%%run for 2010 freshwater pulses
#numerical parameters
N=10
Lsc=1000

#time parameters
DT = np.concatenate([np.zeros(75*2)+12,np.zeros(100*12)+2,np.zeros(225*4)+6]) #hours
T=len(DT)

#varying parameter
Ut= 1.15+np.zeros(T)#+0.15*np.sin(4*np.pi/29.53 * (np.arange(0,100,0.25)+13))
soc = np.array([35]*T)
sri = np.array([0]*T) #np.array([0.5]*T)
sf =2*cv*Ut #np.concatenate([2*cv*Ut[:200], 1.5*cv*Ut[200:]])#2*cv*Ut np.array([9e99]*T)#

#depth, constant in all dimensions
H=7.1

#wind
u_w = np.zeros(T)
d_w =np.zeros(len(u_w))
d_w[np.where(u_w>0)[0]]=1
d_w[np.where(u_w<0)[0]]=-1

#geometry
Ln = np.array([100000,12100,18500,10800,30300,17800,4000,25000])
b0 = 150
bs = np.array([150,190,300,450,500,800,3000,3000*np.exp(10)])
#dxn = np.array([1000,484,500,540,300,356,250,500]) #quite rude
#dxn = np.array([400,242,370,270,150,178,250,250]) #quite fine
dxn = np.array([400,242,185,135,75,89,125,250]) #very fine

#interpolate discharge to time-varying grid
Tim = np.zeros(T) #the time grid of the varying grid
for i in range(T): Tim[i] = np.sum(DT[:i])/24 

ds = 650#day at which the simulation starts

Q_gu_i = outerpolate(Tim,Q_gu[np.where(Qt==t2008)[0][0]+ds:np.where(Qt==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Azn_i = outerpolate(Tim,Q_Azn[np.where(t_Azn==t2008)[0][0]+ds:np.where(t_Azn==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Gua_i = outerpolate(Tim,Q_Gua[np.where(t_Gua==t2008)[0][0]+ds:np.where(t_Gua==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Ger_i = outerpolate(Tim,Q_Ger[np.where(t_Ger==t2008)[0][0]+ds:np.where(t_Ger==t2008)[0][0]+int(np.sum(DT)/24)+ds])
Q_Agu_i = outerpolate(Tim,Q_Agu[np.where(t_Agu==t2008)[0][0]+ds:np.where(t_Agu==t2008)[0][0]+int(np.sum(DT)/24)+ds])

Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[2,3,4,5])
#Q = Q_build( Q_gu_i ,[Q_Azn_i,Q_Agu_i,Q_Gua_i,Q_Ger_i],[0,0,0,0])
Q[:,0] = Q[:,1] #opvallend dat dit nodig is...

#Q = Q_build(Q_gu_i , [np.zeros(T)],[1])
#calculate normalised time step
dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here

#do the actual run
run = 3
os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run))
LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,3,5,6,7],
                     MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0]+10, 0, d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,3,5,6,7]),'all')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_var.txt'),LL[0], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_const.txt'),LL[1], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('par_nd.txt'),LL[2], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('time.txt'),LL[3], fmt='%s')
np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/'+str('Guadalquivir')+'/'+str('run')+str(run)+'/'
           +str('raw_out.txt'),LL[4], fmt='%s') 

print('run', run, ' is finished ')


