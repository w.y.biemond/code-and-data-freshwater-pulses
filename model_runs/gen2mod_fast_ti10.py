#time-independent estuarine salt model

runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal 1#
cv=7.28e-5 #empirische constante 1e-4#
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

def MC04_Gal_nd(inp_p, inp_chn, term):
    #meaning input:
        #inp_p: physical parameters: (Ut, soc, Q, L, bs, H)
        #inp_n: numerical paramters: (dx, N, Lsc)
        #init: initial guess for solutions, in the right format - TO INCLUDE LATER
        #term: which terms of the equation are taken into account
        
    Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc = inp_p #1. , 35. , 150. , 100000. , [2000.,1e9] , 10.
    Ln, b0, bs, dxn = inp_chn
    if np.any(Ln%dxn!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln) != len(bs) or len(Ln)!=len(dxn) : print('ERROR: number of domains is not correct')
    
    dln = dxn/Lsc
    nxn = np.array(Ln/dxn+1,dtype=int)
    di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln)):
        di[i] = np.sum(nxn[:i])
    di[-1] = np.sum(nxn)
    
    dl = np.zeros(np.sum(nxn))
    dl[0:nxn[0]] =dln[0]
    for i in range(1,len(nxn)): dl[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = dln[i]
    
    M=N+1 
    #plotting parameters
    nz=5001 #vertical step - only for plot
    px = np.zeros(np.sum(nxn))
    px[0:nxn[0]] = -np.linspace(np.sum(Ln[0:]), np.sum(Ln[0+1:]), nxn[0]) #here i can use the di list
    for i in range(1,len(nxn)): px[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = -np.linspace(np.sum(Ln[i:]), np.sum(Ln[i+1:]), nxn[i])
    px=px/1000
    pz = np.linspace(-H,0,nz+1)
    
    #build parameters: convergence length
    bn = np.zeros(len(Ln))
    bn[0] = 9e99 if bs[0] == b0 else Ln[0]/np.log(bs[0]/b0)
    for i in range(1,len(Ln)):
        bn[i] = 9e99 if bs[i] == bs[i-1] else Ln[i]/np.log(bs[i]/bs[i-1])
        
    #build width
    b, bex = np.zeros(np.sum(nxn)),np.zeros(np.sum(nxn))
    b[0:nxn[0]] = b0 * np.exp(bn[0]**(-1)*(np.linspace(-Ln[0],0,nxn[0])+Ln[0]))
    bex[0:nxn[0]] = [bn[0]]*nxn[0]
    for i in range(1,len(nxn)): bex[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = [bn[i]]*nxn[i]
    for i in range(1,len(nxn)): b[np.sum(nxn[:i]):np.sum(nxn[:i+1])] = b[np.sum(nxn[:i])-1] * np.exp(bn[i]**(-1)*(np.linspace(-Ln[i],0,nxn[i])+Ln[i]))
    print('The maximum width  is ' , int(np.round(np.max(b))), 'm')

    #plot width
    plt.plot(px,b/2,'black')
    plt.plot(px,-b/2,'black')
    plt.axis('off')
    plt.show()

    #buld parameters
    u_bar = Q/(H*b)
    Kh = ch*Ut*b
    Av = cv*Ut*np.array([H]*np.sum(nxn))
    Kv = Av/Sc
    alf = g*Be*H**3/(48*Av)
    
    #lists of ks and ns and pis
    kkp = np.linspace(1,N,N)*np.pi #k*pi
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
    pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
    np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)

    #coefficients
    #new partial slip coefficients
    #'''
    g1 = -1 + (1.5+3*Av/(sf*H)) / (1+ 3 *Av/(sf*H))
    g2 =  -3 / (2+6*Av/(sf*H))
    g3 = (1+4*Av/(sf*H)) / (1+3*Av/(sf*H)) * (9+18*Av/(sf*H)) - 8 - 24*Av/(sf*H)
    g4 = -9 * (1+4*Av/(sf*H)) / (1+3*Av/(sf*H))
    g5 = - 8
    g6 = 4+4*Av/(sf*H) -12*(0.5+Av/(sf*H))**2/(1+3*Av/(sf*H))
    g7 = 4
    g8 = (3+6*Av/(sf*H)) / (1+3*Av/(sf*H))
    #'''
    #g1,g2,g3,g4,g5 = 1/2 , -3/2 , 1, -9 ,-8
    #print(g1[0],g2[0],g3[0],g4[0],g5,g6[0],g7,g8[0])

    #dimensions: x,k,n
    #k=n is not possible here. 
    C1a = u_bar/2 
    
    C2a = u_bar[:,np.newaxis] * (g1[:,np.newaxis]/2 + g2[:,np.newaxis]/6 + g2[:,np.newaxis]/(4*kkp**2)) + (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis])*(g6[:,np.newaxis]/2-g7/4+g8[:,np.newaxis]*(1/6+1/(4*kkp**2)))
    C2b = alf[:,np.newaxis]*soc/Lsc * (g3[:,np.newaxis]/2 + g4[:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g5*(1/8 + 3/(8*kkp**2)) )
    C2c = (u_bar[:,np.newaxis,np.newaxis]*g2[:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 ) 
           + (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis,np.newaxis]) * (g7/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g8[:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
    C2d = soc/Lsc*alf[:,np.newaxis,np.newaxis] * (g4[:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g5* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )
    
    C3a = 2*u_bar[:,np.newaxis]*g2[:,np.newaxis]/(kkp**2)*np.cos(kkp) + (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis]) * (g7+(2*g8[:,np.newaxis]-g7)*np.cos(kkp))/(kkp**2)
    C3b = alf[:,np.newaxis]*soc/Lsc * ( 2*g4[:,np.newaxis]/kkp**2 * np.cos(kkp) - g5/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )
    
    C5a = alf[:,np.newaxis]*soc/Lsc *kkp* ( -9*g5+6*g4[:,np.newaxis]+kkp**2*(-12*g3[:,np.newaxis]-4*g4[:,np.newaxis]+3*g5) ) / (48*kkp**3)
    C5b = alf[:,np.newaxis]*soc*bex[:,np.newaxis]**(-1)*kkp * ( -9*g5+6*g4[:,np.newaxis]+kkp**2*(-12*g3[:,np.newaxis]-4*g4[:,np.newaxis]+3*g5) ) / (48*kkp**3)    
    C5c = alf[:,np.newaxis,np.newaxis]*soc/Lsc*nnp* ( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pkn)/pkn * (g5/8 - g4[:,np.newaxis,np.newaxis]/6 - g3[:,np.newaxis,np.newaxis]/2) 
                            +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[:,np.newaxis,np.newaxis]/6 - g3[:,np.newaxis,np.newaxis]/2))
    C5d = alf[:,np.newaxis,np.newaxis]*soc*nnp*bex[:,np.newaxis,np.newaxis]**(-1)*( (3*g5*np.cos(pkn)-3*g5)/pkn**5 + np.cos(pkn)/pkn**3 * (g4[:,np.newaxis,np.newaxis]-1.5*g5) 
                                + np.cos(pkn)/pkn * (g5/8 - g4[:,np.newaxis,np.newaxis]/6 - g3[:,np.newaxis,np.newaxis]/2)
                                +(3*g5*np.cos(pnk)-3*g5)/pnk**5 + np.cos(pnk)/pnk**3 * (g4[:,np.newaxis,np.newaxis]-1.5*g5) + np.cos(pnk)/pnk * (g5/8 - g4[:,np.newaxis,np.newaxis]/6 - g3[:,np.newaxis,np.newaxis]/2))
    C5e = Lsc*bex[:,np.newaxis]**(-1) * (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis]) * (-g6[:,np.newaxis]/4 + g7/8 + g8[:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
    C5f = (Lsc*bex[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis,np.newaxis]) * nnp
              * ( ((g8[:,np.newaxis,np.newaxis]-g7/2)*np.cos(pkn)-g7/2)/(pkn**3) + ((g8[:,np.newaxis,np.newaxis]-g7/2)*np.cos(pnk)-g7/2)/(pnk**3) 
                 +(-g6[:,np.newaxis,np.newaxis]/2+g7/4-g8[:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g6[:,np.newaxis,np.newaxis]/2+g7/4-g8[:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )

    C6a = Lsc*Kv[:,np.newaxis]*kkp**2/(2*H**2)
    
    C7a = -bex**-1 * Kh
    C7b = -Kh/(2*Lsc)
    
    #in the following terms k=n is possible -  huh why did i say this, no k dependence of course
    C10a = (u_bar-2*bex**(-1)*Kh)
    C10b = -Kh/Lsc
    C10c = bex[:,np.newaxis]**(-1)*alf[:,np.newaxis]*soc * ( 2*g4[:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10d = alf[:,np.newaxis]*soc/Lsc * ( 2*g4[:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10e = (2*u_bar[:,np.newaxis]*g2[:,np.newaxis]) / nnp**2 * np.cos(nnp) + (r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis]) * (g7+ (2*g8[:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)
    C10f = alf[:,np.newaxis]*soc/Lsc * ( 2*g4[:,np.newaxis]/nnp**2*np.cos(nnp) - g5/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10g = Lsc*bex[:,np.newaxis]**(-1)*(r*CD*H*u_w**2*d_w)/(4*Av[:,np.newaxis]) * (g7+(2*g8[:,np.newaxis]-g7)*np.cos(nnp))/(nnp**2)
    
    #set the contribution to the sum to zero of k=n (the diagonal)
    C2c[np.where(np.isnan(C2c))] = 0 
    C2d[np.where(np.isnan(C2d))] = 0
    C5c[np.where(np.isnan(C5c))] = 0
    C5d[np.where(np.isnan(C5d))] = 0
    C5f[np.where(np.isnan(C5f))] = 0

    #build indices for jacobian and solution vector
    di2 = np.zeros((len(di)-2)*2)
    for i in range(1,len(di)-1):
        di2[i*2-2] = di[i]-1
        di2[i*2-1] = di[i]
    di2 = np.array(di2, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    x = np.delete(np.arange(di[-1]),di2)[1:-1] # x coordinates for the points which are not on a aboundary
    xr = x.repeat(N) # x for N values, mostly i in old code
    xr_m = xr*M #M*i in old coe
    xrm_m = (xr-1)*M
    xrp_m = (xr+1)*M
    xr_mj = xr_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2))
    xrp_mj = xrp_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2))
    xrm_mj = xrm_m+np.tile(np.arange(1,M),di[-1]-2-(len(di[1:-1])*2))
    j1 = np.tile(np.arange(N),di[-1]-2-(len(di[1:-1])*2))
    
    #for the k things we need to repeat some arrays
    xr_mj2=np.repeat(xr_m,N)+np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))).repeat(N)
    xr2 = xr.repeat(N)
    j12 = j1.repeat(N)
    xr_m2 = xr_m.repeat(N)
    xrp_m2 = xrp_m.repeat(N)
    xrm_m2 = xrm_m.repeat(N)
    
    k1 = np.tile(j1,N)
    xr_mk=np.repeat(xr_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrp_mk=np.repeat(xrp_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)
    xrm_mk=np.repeat(xrm_m,N)+np.tile(np.tile(np.arange(1,M),(di[-1]-2-(len(di[1:-1])*2))),N)

    x_m = x*M
    xp_m = (x+1)*M
    xm_m = (x-1)*M
    
    #for the boundaries 
    bnd = (di[np.arange(1,len(nxn))]*M+np.arange(M)[:,np.newaxis]).flatten()
    bnd_dl = np.repeat(np.arange(1,len(nxn)),M) #not used yet
    
    #build the jacobian
    def jac_build(ans,terms):
        jac = np.zeros((di[-1]*M,di[-1]*M))
        
        if 1 in terms : #contribution to jacobian due to term T1
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C1a[xr]/(2*dl[xr]) 
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C1a[xr]/(2*dl[xr]) 
              

        if 2 in terms: #contribution to jacobian due to term T2
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C2a[xr,j1]/(2*dl[xr]) - C2b[xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C2a[xr,j1]/(2*dl[xr]) + C2b[xr,j1]/(2*dl[xr]) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrm_m] = (jac[xr_mj,xrm_m] - C2b[xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                           - np.sum([C2d[xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj,xrp_m] = (jac[xr_mj,xrp_m]  + C2b[xr,j1]/(2*dl[xr]) * (ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) 
                                               + np.sum([C2d[xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(4*dl[xr]**2) )
            jac[xr_mj2,xrm_mk] = jac[xr_mj2,xrm_mk] - C2c[xr2,j12,k1]/(2*dl[xr2]) - C2d[xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 
            jac[xr_mj2,xrp_mk] = jac[xr_mj2,xrp_mk] + C2c[xr2,j12,k1]/(2*dl[xr2]) + C2d[xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(4*dl[xr2]**2) 
        
        if 3 in terms: #contribution to jacobian due to term T3
            jac[xr_mj,xrm_m] = jac[xr_mj,xrm_m] - C3a[xr,j1]/(2*dl[xr]) - C3b[xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
            jac[xr_mj,xrp_m] = jac[xr_mj,xrp_m] + C3a[xr,j1]/(2*dl[xr]) + C3b[xr,j1]/dl[xr] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])


        if 5 in terms: #contribution to jacobian due to term T5
            jac[xr_mj, xrm_m] = (jac[xr_mj, xrm_m] + C5a[xr,j1]*ans[xr_mj]/(dl[xr]**2) - C5b[xr,j1]/(2*dl[xr])*ans[xr_mj]
                                   + np.sum([ans[xr_m+n]*C5c[xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0) 
                                   - np.sum([ans[xr_m+n]*C5d[xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj, xr_m] = jac[xr_mj, xr_m] - 2*C5a[xr,j1]*ans[xr_mj]/(dl[xr]**2) -2* np.sum([ans[xr_m+n]*C5c[xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
            jac[xr_mj, xrp_m] = (jac[xr_mj, xrp_m] + C5a[xr,j1]*ans[xr_mj]/(dl[xr]**2) + C5b[xr,j1]/(2*dl[xr])*ans[xr_mj]
                                           + np.sum([ans[xr_m+n]*C5c[xr,j1,n-1]/(dl[xr]**2) for n in range(1,M)],0)
                                           + np.sum([ans[xr_m+n]*C5d[xr,j1,n-1]/(2*dl[xr]) for n in range(1,M)],0) )
            jac[xr_mj,xr_mj] = (jac[xr_mj, xr_mj] + C5a[xr,j1]*(ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) 
                                             + C5b[xr,j1]*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) + C5e[xr,j1] )
            jac[xr_mj2, xr_mk] = (jac[xr_mj2, xr_mk] + C5c[xr2,j12,k1]*(ans[xrp_m2]-2*ans[xr_m2]+ans[xrm_m2])/(dl[xr2]**2)
                                                 + C5d[xr2,j12,k1]*(ans[xrp_m2]-ans[xrm_m2])/(2*dl[xr2]) +C5f[xr2,j12,k1] )

        
        if 6 in terms: #contribution to jacobian due to term T6
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] + C6a[xr,j1]

        if 7 in terms: 
            jac[xr_mj,xrm_mj] = jac[xr_mj,xrm_mj] - C7a[xr]/(2*dl[xr]) + C7b[xr] / (dl[xr]**2)
            jac[xr_mj,xr_mj] = jac[xr_mj,xr_mj] - 2*C7b[xr]/(dl[xr]**2)
            jac[xr_mj,xrp_mj] = jac[xr_mj,xrp_mj] + C7a[xr]/(2*dl[xr]) + C7b[xr] / (dl[xr]**2)

        #contribution to jacobian due to term the average salt equation - always present
        #left
        jac[x_m, xm_m] = (jac[x_m, xm_m] - C10a[x]/(2*dl[x]) + C10b[x]/(dl[x]**2) - 1/(2*dl[x])*np.sum([C10c[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       - 1/(2*dl[x])*np.sum([C10f[x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrm_mj] = jac[xr_m, xrm_mj] - C10e[xr,j1]/(2*dl[xr]) - C10f[xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        #center
        jac[x_m,x_m] = jac[x_m,x_m] - 2/(dl[x]**2)*C10b[x]  -2/(dl[x]**2)*np.sum([C10d[x,n-1]*ans[x_m+n] for n in range(1,M)],0)
        jac[xr_m, xr_mj] = jac[xr_m,xr_mj] + (ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) * C10c[xr,j1] + (ans[xrp_m]-2*ans[xr_m]+ans[xrm_m])/(dl[xr]**2) * C10d[xr,j1] + C10g[xr,j1]
        #right
        jac[x_m, xp_m] = (jac[x_m,xp_m] + C10a[x]/(2*dl[x]) + C10b[x]/(dl[x]**2) + 1/(2*dl[x])*np.sum([C10c[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(dl[x]**2)*np.sum([C10d[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                                       + 1/(2*dl[x])*np.sum([C10f[x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) )
        jac[xr_m, xrp_mj] = jac[xr_m,xrp_mj] + C10e[xr,j1]/(2*dl[xr]) + C10f[xr,j1]/(2*dl[xr])*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
        
        #river boundary - salt is prescribed
        jac[np.arange(M),np.arange(M)] = jac[np.arange(M),np.arange(M)] + 1

        #inner boundary 
        for d in range(1,len(nxn)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di[d]-1)*M+np.arange(M), (di[d]-3)*M+np.arange(M)] =  1/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-2)*M+np.arange(M)] = - 4/(2*dl[di[d]-1])
            jac[(di[d]-1)*M+np.arange(M), (di[d]-1)*M+np.arange(M)] =  3/(2*dl[di[d]-1]) 
            
            jac[(di[d]-1)*M+np.arange(M), di[d]*M+np.arange(M)] =  3/(2*dl[di[d]]) 
            jac[(di[d]-1)*M+np.arange(M), (di[d]+1)*M+np.arange(M)] =  - 4/(2*dl[di[d]])
            jac[(di[d]-1)*M+np.arange(M), (di[d]+2)*M+np.arange(M)] =  1/(2*dl[di[d]])

        #points equal
        jac[bnd, bnd-M] = 1                   
        jac[bnd, bnd] = -1
                
        #sea boundary
        jac[M*(di[-1]-1),M*(di[-1]-1)] = jac[M*(di[-1]-1),M*(di[-1]-1)] - 1
        jac[np.arange(M*(di[-1]-1)+1,M*di[-1]),np.arange(M*(di[-1]-1)+1,M*di[-1])] =  1

        return jac

    
    #solution formulae
    #inner points
    sol = {'se' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, x: (C10a[x]*(S1-S_1)/(2*dl[x]) + C10b[x]*(S1-2*S0+S_1)/(dl[x]**2) + (S1-S_1)/(2*dl[x]) * np.sum([C10c[x, n]*Sn0[n] for n in range(0,N)],0) 
                                                            + (S1-2*S0+S_1)/(dl[x]**2) * np.sum([C10d[x,n]*Sn0[n] for n in range(0,N)],0) + np.sum([C10e[x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)],0) 
                                                            + (S1-S_1)/(2*dl[x]) * np.sum([C10f[x,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)],0)
                                                            + np.sum([C10g[x,n]*Sn0[n] for n in range(0,N)],0) ),
            '1' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x: C1a[x] * ((Sn1[k]-Sn_1[k])/(2*dl[x])) ,
            '2' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x: (C2a[x,k]*(Sn1[k]-Sn_1[k])/(2*dl[x]) + C2b[x,k]*(Sn1[k]-Sn_1[k])/(2*dl[x])*(S1-S_1)/(2*dl[x])
                                                             + np.sum([C2c[x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])
                                                             + np.sum([C2d[x,k,n]*(Sn1[n]-Sn_1[n])/(2*dl[x]) for n in range(0,N)])*(S1-S_1)/(2*dl[x])) ,
             '3' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x: C3a[x,k] * (S1-S_1)/(2*dl[x]) + C3b[x,k] * ((S1-S_1)/(2*dl[x]))**2 ,
             '5' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x:  (C5a[x,k]*(S1-2*S0+S_1)/(dl[x]**2)*Sn0[k] + C5b[x,k]*(S1-S_1)/(2*dl[x])*Sn0[k]  
                                       +np.sum([C5c[x,k,n]*Sn0[n] for n in range(0,N)])*(S1-2*S0+S_1)/(dl[x]**2) 
                                       +np.sum([C5d[x,k,n]*Sn0[n] for n in range(0,N)])*(S1-S_1)/(2*dl[x]) 
                                       + C5e[x,k]*Sn0[k] + np.sum([C5f[x,k,n]*Sn0[n] for n in range(0,N)])) ,
             '6' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x: C6a[x,k]*Sn0[k] , 
             '7' : lambda S_1, S0, S1, Sn_1, Sn0, Sn1, k, x: C7a[x]*(Sn1[k]-Sn_1[k])/(2*dl[x]) + C7b[x]*(Sn1[k] - 2*Sn0[k] + Sn_1[k])/(dl[x]**2) 
             }
    #inner boundary - points and derivatives are equal
    sol_in2 =  {'se': lambda S_1, S0: S_1 - S0,
                'dse': lambda S_3,S_2,S_1,S0,S1,S2,d: (3*S_1-4*S_2+S_3)/(2*dl[di[d]-1]) - (-3*S0+4*S1-S2)/(2*dl[di[d]])
                }
    
    def sol_build(ans, terms): #old-fashioned way... but I think it works fine
    
        so = np.zeros(di[-1]*M)
        
        #river boundary - everything is zero
        so[0] = ans[0]-sri/soc
        so[1:M] = ans[1:M]
        
        #contribution to solution vector due to average salt balance
        so[x_m] = so[x_m] + (C10a[x]*(ans[xp_m]-ans[xm_m])/(2*dl[x]) + C10b[x]*(ans[xp_m] - 2*ans[x_m] + ans[xm_m])/(dl[x]**2) 
                            + (ans[xp_m]-ans[xm_m])/(2*dl[x]) * np.sum([C10c[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                            + (ans[xp_m] - 2*ans[x_m] + ans[xm_m])/(dl[x]**2) * np.sum([C10d[x,n-1]*ans[x_m+n] for n in range(1,M)],0) 
                            + np.sum([C10e[x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0) 
                            + (ans[xp_m]-ans[xm_m])/(2*dl[x]) * np.sum([C10f[x,n-1]*(ans[xp_m+n]-ans[xm_m+n])/(2*dl[x]) for n in range(1,M)],0)
                            + np.sum([C10g[x,n-1]*ans[x_m+n] for n in range(1,M)],0) )
        
        
        if 1 in terms : #contribution to solution vector due to term T1
            so[xr_mj] = so[xr_mj] + C1a[xr] * ((ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]))

        if 2 in terms: #contribution to solution vector due to term T2
            so[xr_mj] = so[xr_mj] + (C2a[xr,j1]*(ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr])
                                    + C2b[xr,j1]*((ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]))*((ans[xrp_m]-ans[xrm_m])/(2*dl[xr]))
                                    +np.sum([C2c[xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    +np.sum([C2d[xr,j1,n-1] * (ans[xrp_m+n]-ans[xrm_m+n]) for n in range(1,M)],0)/(2*dl[xr])
                                    *(ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) )

        if 3 in terms: #contribution to solution vector due to term T3
            so[xr_mj] = so[xr_mj] + C3a[xr,j1] * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) + C3b[xr,j1] * ((ans[xrp_m]-ans[xrm_m])/(2*dl[xr]))**2

        if 5 in terms: #contribution to jacobian due to term T5
            so[xr_mj] = so[xr_mj] + (C5a[xr,j1]*(ans[xrp_m] - 2*ans[xr_m] + ans[xrm_m])/(dl[xr]**2)*ans[xr_mj] 
                                     + C5b[xr,j1]*(ans[xrp_m]-ans[xrm_m])/(2*dl[xr]) * ans[xr_mj]  
                                       +np.sum([C5c[xr,j1,n-1]*ans[xr_m+n] for n in range(1,M)],0) * (ans[xrp_m] - 2*ans[xr_m] + ans[xrm_m])/(dl[xr]**2) 
                                       +np.sum([C5d[xr,j1,n-1]*ans[xr_m+n] for n in range(1,M)],0) * (ans[xrp_m]-ans[xrm_m])/(2*dl[xr])
                                       + C5e[xr,j1]*ans[xr_mj] + np.sum([C5f[xr,j1,n-1]*ans[xr_m+n] for n in range(1,M)],0))

        if 6 in terms: #contribution to solution vector due to term T6
            so[xr_mj] = so[xr_mj] + C6a[xr,j1]*ans[xr_mj]

        if 7 in terms: 
            so[xr_mj] = so[xr_mj] + C7a[xr]*(ans[xrp_mj]-ans[xrm_mj])/(2*dl[xr]) + C7b[xr]*(ans[xrp_mj] - 2*ans[xr_mj] + ans[xrm_mj])/(dl[xr]**2) 

        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn)):
            for j in range(M):
                s_3, s_2, s_1 = ans[(di[d]-3)*M:(di[d]-2)*M], ans[(di[d]-2)*M:(di[d]-1)*M], ans[(di[d]-1)*M:di[d]*M]
                s0, s1, s2 = ans[di[d]*M:(di[d]+1)*M], ans[(di[d]+1)*M:(di[d]+2)*M], ans[(di[d]+2)*M:(di[d]+3)*M]
                so[(di[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],d)
                so[di[d]*M+j] =  sol_in2['se'](s_1[j],s0[j])
                
        #sea boundary
        so[M*(di[-1]-1)] = 1-ans[M*(di[-1]-1)]
        so[M*(di[-1]-1)+1:M*di[-1]] = ans[M*(di[-1]-1)+1:M*di[-1]]

        return so

    #initialise the first run 
    #esponential inital guess 
    Sb = np.zeros(di[-1])
    #Sb[0:di[1]] = 2**(np.linspace(0,Ln[0]/np.sum(Ln[:-1]),di[1])**6)-1    
    #for i in range(1,len(nxn)-1): Sb[di[i]:di[i+1]] =2**(np.linspace(np.sum(Ln[:i])/np.sum(Ln[:-1]),np.sum(Ln[:i+1])/np.sum(Ln[:-1]),nxn[i])**6)-1    
    #Sb[di[-2]:] = 1 #2**(np.linspace(np.sum(Ln[:-1])/np.sum(Ln), 1, nxn[-1])**6)-1 
    Sb[0:di[1]] = 2**(np.linspace(0,Ln[0]/np.sum(Ln),di[1])**6)-1    
    for i in range(1,len(nxn)-1): Sb[di[i]:di[i+1]] =2**(np.linspace(np.sum(Ln[:i])/np.sum(Ln),np.sum(Ln[:i+1])/np.sum(Ln),nxn[i])**6)-1    
    Sb[di[-2]:] = 2**(np.linspace(np.sum(Ln[:-1])/np.sum(Ln), 1, nxn[-1])**6)-1 
    Sn = np.zeros((di[-1],N))
    init = np.zeros((di[-1],M))
    init[:,0] = Sb
    init[:,1:] = Sn
    init=init.flatten()
    
    #now initialise the model with running first without the complicated terms. this step might be problematic for high river discharges
    jaco, solu = jac_build(init,[3,6,7]) , sol_build(init,[3,6,7])
    sss_n =init - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

    sss, ti=init,1
    # do the rest of the iterations
    while np.max(np.abs(sss-sss_n))>10e-3: #check whether the algoritm has converged
        sss = sss_n #update
        jaco, solu = jac_build(sss,[3,6,7]) , sol_build(sss,[3,6,7])
        sss_n =sss - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)
        
        ti = 1+ti
        if ti>25:
            print('ERROR: The initialisation failed')
            break
    
    init = sss_n #now we have a initialisation that is already almost correct for most river discharges
    
    #do the first iteration step
    jaco, solu = jac_build(init,term) , sol_build(init,term)
    sss_n =init - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

    t=1
    print('That was iteration step ', t)
    sss=init
    # do the rest of the iterations
    while np.max(np.abs(sss-sss_n))>10e-6: #check whether the algoritm has converged
        sss = sss_n #update
        #calculate new values
        jaco = jac_build(sss,term) 
        solu = sol_build(sss,term)
        sss_n =sss - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

        #plot the new sigma 
        plt.plot(px ,sss_n.reshape(len(px),M)[:,0], label = '$S$')
        plt.plot(px ,sss_n.reshape(len(px),M)[:,1], label = '$s_1^\prime$')
        plt.plot(px ,sss_n.reshape(len(px),M)[:,2], label = '$s_2^\prime$')
        plt.plot(px ,sss_n.reshape(len(px),M)[:,3], label = '$s_3^\prime$')
        plt.legend()
        plt.show()

        t=1+t
        print('That was iteration step ', t)

        if t>=20: break
       
    print('The algoritm has converged') if t<20 else print('ERROR: no convergence')
    
    #calculate salinity
    sb = np.reshape(sss,(di[-1],M))[:,0] *soc
    sn = np.reshape(sss,(di[-1],M))[:,1:] *soc
    
    #calculate and plot total salinity 
    s_b = np.transpose([np.reshape(sss,(di[-1],M))[:,0]]*(nz+1))
    sn = np.reshape(sss,(di[-1],M))[:,1:]
    zz = np.linspace(-H,0,nz+1)
    s_p = np.array([np.sum([sn[i,n-1]*np.cos(np.pi*n/H*zz) for n in range(1,M)],0) for i in range(di[-1])])
    s = (s_b+s_p)*soc
    s[np.where((s<0) & (s>-0.0001))]= 1e-10 #remove negative values due to numerical precision
    
    #make contourplot
    fig,ax = plt.subplots(figsize=(10,10))
    l1=ax.contourf(px,pz,  s.transpose(), cmap='Spectral_r',levels=(np.linspace(0,soc,11)))
    #ax.quiver(qx,qz,qu.transpose(),qw.transpose(),color='white')
    cb0 = fig.colorbar(l1, ax=ax,orientation='horizontal', pad=0.16)
    cb0.set_label(label='Salinity [psu]',fontsize=16)
    ax.set_xlabel('Along-channel distance [km]',fontsize=16) 
    ax.set_ylabel('Vertical [m]',fontsize=16)    
    #ax.set_xlim(-110,0)
    #ax.vline(-10)
    plt.show()  

    #print('The depth-averaged salt intrusion length is ',-px[np.where(s_b>2/soc)[0][0]]-Ln[-1]/1000)
    #print(s_b[np.where(px==-Ln[-1]/1000)[0][0]][0]*soc)
    print('\n The salt intrusion length is ',-px[np.where(s[:,0]>2)[0][0]]-Ln[-1]/1000,' km')
    print('The minimum salinity is ',np.min(s),' psu')
    print('The salinity at the sea boundary at the bottom is ', s[np.where(px==-Ln[-1]/1000)[0][0],0])
    return sss

'''
#numerical parameters
N=10

Lsc=1000

#physical parameters
Ut = 1
soc = 35
sri=0
Q=200
sf = 2*cv*Ut

u_w = 0
d_w = 1
#geometry
H=25

Ln = np.array([300000,300000,25000])
b0 = 1000
bs = np.array([b0,b0,b0*np.exp(10)])
dxn = np.array([2000,500,250])

tijd = time.time()
MC04_Gal_nd((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), [1,2,3,5,6,7])
print('Doing the simulation takes  '+ str(time.time()-tijd)+' seconds')

#'''