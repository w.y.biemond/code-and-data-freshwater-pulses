#do simulations for the parameter space

#load models and packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py') #load packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ti10.py') #load equilibrium model
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ats10.py') #load code to do adaptive time stepping
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_td10.py') #load model - second verison

#duration = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/defi/Tadj_run0_109.txt')

# =============================================================================
# make backgroudn and peak river discharge. Avoid that the peak is lower than 3 times the background
# =============================================================================
Q_bc_l1 = np.concatenate([np.linspace(16,162,5),np.linspace(162,1211,7)[1:]])
Q_p_l1 = np.linspace(323,4846,15)

Q_bc_l2,Q_p_l2 =[] , []
for i in range(11):
    if 3.1*Q_bc_l1[i]<Q_p_l1[0]:
        Q_bc_l2.extend(np.zeros(len(Q_p_l1))+Q_bc_l1[i])
        Q_p_l2.extend(Q_p_l1)
    else: 
        Q_bc_l2.extend(np.zeros(len(Q_p_l1)-int(i/2))+Q_bc_l1[i])
        '''
        for j in range(11):
            if Q_p_l1[j] < 3*Q_bc_l1[i]:
                Q_p_l2[i,j] = 3*Q_bc_l1[i]
            else: 
               Q_p_l2[i,j] = Q_p_l1[j]
               
        '''
        Q_p_l2.extend(np.linspace(3*Q_bc_l1[i],4846,len(Q_p_l1)-int(i/2)))

#Q_bc_l2 = np.array(Q_bc_l2).flatten()   
#\Q_p_l2 = np.array(Q_p_l2).flatten()   
#==============================================================================

for i in rang(141):
    Qp,Qbc = Q_p_l2[i], Q_bc_l2[i]
    #set up simulation
    #numerical parameters
    N= 5 + int(Qp/600)
    Lsc=1000
    
    #duration of the periods in days. periods = before, during, after
    dura = np.zeros(3)
    dura[0] = 10 #before pulse: always 10 days
    #length pulse
    if Qp<750: dura[1] = 50
    elif Qp<2000: dura[1] = 20
    else: dura[1] = 10
    #length comeback
    if Qbc<50: dura[2] = 700
    elif Qbc<500: dura[2] = 150
    else: dura[2] = 50 

    #time parameters
    DT_p1 = np.zeros(10)+24

    #length pulse
    if Qp<750: DT_p2 = np.concatenate([np.zeros(2*24)+1 , np.zeros(48*6)+4 ]) # 50 days 
    elif Qp<2000: DT_p2 = np.concatenate([np.zeros(2*24*2)+0.5 , np.zeros(18*6)+4 ]) # 20 days
    else: DT_p2 = np.concatenate([np.zeros(2*24*4)+0.25 , np.zeros(8*6)+4 ]) #10 days
    #length comeback
    if Qbc<50: DT_p3 = np.concatenate([ np.zeros(2*24*4)+0.25 , np.zeros(8*6)+4 , np.zeros(690*2)+12 ]) #700 days
    elif Qbc<500: DT_p3 = np.concatenate([ np.zeros(2*24*4)+0.25 , np.zeros(8*6)+4 , np.zeros(140*2)+12 ]) #150 days
    else: DT_p3 = np.concatenate([ np.zeros(2*24*4)+0.25 , np.zeros(8*6)+4 , np.zeros(40*2)+12 ]) #50 days
    '''
    #slower story
    #length pulse
    if Qp<750: DT_p2 = np.concatenate([np.zeros(4*24)+1 , np.zeros(46*6)+4 ]) # 50 days 
    elif Qp<2000: DT_p2 = np.concatenate([np.zeros(4*24*2)+0.5 , np.zeros(16*6)+4 ]) # 20 days
    else: DT_p2 = np.concatenate([np.zeros(4*24*4)+0.25 , np.zeros(6*6)+4 ]) #10 days
    #length comeback
    if Qbc<50: DT_p3 = np.concatenate([ np.zeros(4*24*4)+0.25 , np.zeros(6*6)+4 , np.zeros(690*2)+12 ]) #700 days
    elif Qbc<500: DT_p3 = np.concatenate([ np.zeros(4*24*4)+0.25 , np.zeros(6*6)+4 , np.zeros(140*2)+12 ]) #150 days
    else: DT_p3 = np.concatenate([ np.zeros(4*24*4)+0.25 , np.zeros(6*6)+4 , np.zeros(40*2)+12 ]) #50 days
    '''
    #DT = np.concatenate([np.zeros(int(dura[0]))+24,np.zeros(12+48)+1,np.zeros(int((dura[1]-2.5)*24/6))+6,np.zeros(12+48)+1,np.zeros(int((50-2.5)*4))+6,np.zeros(int(dura[2]-50)*2)+12]) #hours
    #DT = np.concatenate([np.zeros(10)+24, np.zeros(48)+1 , np.zeros(int(dura[1]-2)*4)+6 , np.zeros(48)+1 , np.zeros(3*12)+2,np.zeros(int(dura[2]*2))+12])
    DT = np.concatenate([DT_p1,DT_p2,DT_p3])
    T=len(DT)

    #varying parameter
    Ut = np.zeros(T)+1.#0.75+0.05*j#*1.5**0.5#+0.25*np.sin(4*np.pi/30 * (DT/24*(np.arange(0,T,1)+lag)))
    soc = np.array([35.]*T)
    sri = np.array([0.]*T)
    sf =2*cv*Ut
    
    #depth, constant in all dimensions
    H=10
    u_w = np.zeros(T)
    d_w =np.zeros(len(u_w))

    #geometry - new version as always
    b0 = 1000 #width
    L_sea, mu_sea = 25,np.exp(10)
    L_dom = 100-int(Qp/100)
    if Qbc<100: L_extra = 200
    elif Qbc<500: L_extra = 50
    else: L_extra = 25
    if L_extra==200 and Qbc>17: L_extra=100#changed because of long simulation time
    
    Ln = np.array([L_extra,L_dom,L_sea])*1000
    bs = np.array([b0,b0,b0*mu_sea])
    dxx = 250 if Qp<2000 else 125
    if Qbc<20 and i != 14: dxx = 250 #changed because of long simulation time
    dxn = np.array([dxx*2,dxx,dxx])

    '''
    #build tanh pulse
    dur = int(0.5 * 24)#days
    t = np.linspace(-5,5,dur)
    Qh1 = np.tanh(t)*(Qp-Qbc)/2+(Qp-Qbc)/2+Qbc
    Qh =np.concatenate([np.zeros(int(dura[0]))+Qbc,Qh1,np.zeros(48)+Qp,np.zeros(int((dura[1]-2.5)*24/6))+Qp,np.flip(Qh1),np.zeros(48)+Qbc,np.zeros(int((50-2.5)*4))+Qbc,np.zeros(int(dura[2]-50)*2)+Qbc])
    '''
    Qh = np.concatenate([np.zeros(len(DT_p1))+Qbc , np.zeros(len(DT_p2))+Qp, np.zeros(len(DT_p3))+Qbc])
    
    nxn = np.array(Ln/dxn+1,dtype=int)
    Q = np.repeat(Qh,np.sum(nxn)).reshape((T,np.sum(nxn)))
    #calculate normalised time step
    dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here
    
    #do the real simulation
    run = i #+110
    fold = 'defi2'
    os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run))
    LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,5,3,6,7],
                         np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/preparation/'+str('prep_run')+str(run)+'.txt'),None)
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_var.txt'),LL[0], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_const.txt'),LL[1], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_nd.txt'),LL[2], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('time.txt'),LL[3], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('raw_out.txt'),LL[4], fmt='%s') 
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('salt_mm.txt'),LL[5], fmt='%s') 
    
    print('run', run, ' is finished ')
    



