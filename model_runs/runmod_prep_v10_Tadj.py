#make preparation files for the runs

#load models and packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py') #load packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ti10.py') #load equilibrium model
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ats10.py') #load code to do adaptive time stepping
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_td10.py') #load model - second verison

#duration = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/defi/Tadj_run0_109.txt')

# =============================================================================
# make backgroudn and peak river discharge. Avoid that the peak is lower than 3 times the background
# =============================================================================
Q_bc_l1 = np.concatenate([np.linspace(16,162,5),np.linspace(162,1211,7)[1:]])
Q_p_l1 = np.linspace(323,4846,15)

Q_bc_l2,Q_p_l2 =[] , []
for i in range(11):
    if 3.1*Q_bc_l1[i]<Q_p_l1[0]:
        Q_bc_l2.extend(np.zeros(len(Q_p_l1))+Q_bc_l1[i])
        Q_p_l2.extend(Q_p_l1)
    else: 
        Q_bc_l2.extend(np.zeros(len(Q_p_l1)-int(i/2))+Q_bc_l1[i])
        '''
        for j in range(11):
            if Q_p_l1[j] < 3*Q_bc_l1[i]:
                Q_p_l2[i,j] = 3*Q_bc_l1[i]
            else: 
               Q_p_l2[i,j] = Q_p_l1[j]
               
        '''
        Q_p_l2.extend(np.linspace(3*Q_bc_l1[i],4846,len(Q_p_l1)-int(i/2)))

#Q_bc_l2 = np.array(Q_bc_l2).flatten()   
#\Q_p_l2 = np.array(Q_p_l2).flatten()   
#==============================================================================

for i in range(141):
    Qp,Qbc = Q_p_l2[i], Q_bc_l2[i]
    #set up simulation
    #numerical parameters
    N= 5 + int(Qp/600)
    Lsc=1000

    #time parameters
    DT = np.zeros(100)+12 #hours
    T=len(DT)

    #varying parameter
    Ut = np.zeros(T)+1#0.75+0.05*j#*1.5**0.5#+0.25*np.sin(4*np.pi/30 * (DT/24*(np.arange(0,T,1)+lag)))
    soc = np.array([35]*T)
    sri = np.array([0]*T)
    sf =2*cv*Ut
    
    #depth, constant in all dimensions
    H=10
    u_w = np.zeros(T)
    d_w =np.zeros(len(u_w))

    #geometry - new version as always
    b0 = 1000 #width
    L_sea, mu_sea = 25,np.exp(10)
    L_dom = 100-int(Qp/100)
    if Qbc<100: L_extra = 200
    elif Qbc<500: L_extra = 50
    else: L_extra = 25
    if L_extra==200 and Qbc>17: L_extra=100#changed because of long simulation time
    
    Ln = np.array([L_extra,L_dom,L_sea])*1000
    bs = np.array([b0,b0,b0*mu_sea])
    dxx = 250 if Qp<2000 else 125
    if Qbc<20: dxx = 250 #changed because of long simulation time
    dxn = np.array([dxx*2,dxx,dxx])
    
    def Q_build(Q1,Q2,dom): #river discharge 1, river 2 dicscharge, domain where the new river flos in
    
        #build indices just like in normal code
        nxn = np.array(Ln/dxn+1,dtype=int)
        di = np.zeros(len(Ln)+1,dtype=int) #starting indices of the domains
        for i in range(1,len(Ln)):
            di[i] = np.sum(nxn[:i])
        di[-1] = np.sum(nxn)
    
        Q = np.zeros((T,di[-1]))
        
        for x in range(di[-1]): #for every x
            Q[:,x] = np.repeat(Q1,int(24/DT))
            for d in range(len(dom)): #for every tributary
                if x>di[dom[d]]: #after 2 domains the Dordogne confluences with the Garonne
                    Q[:,x] = Q[:,x] + np.repeat(Q2[d],int(24/DT))
        return Q
    
    #build increasing 
    Qh =np.concatenate([np.linspace(400,Qbc,20),np.zeros(80)+Qbc])
    nxn = np.array(Ln/dxn+1,dtype=int)
    Q = np.repeat(Qh,np.sum(nxn)).reshape((T,np.sum(nxn)))
    
    #dimensionless parameter switch
    #calculate normalised time step
    dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here

    #do the real simulation
    run = i #+110
    fold = 'defi0_05adj/preparation'

    if Qbc < 400:
        np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('prep_run')+str(run)+'.txt',
                   MC04_Gal_nd((Ut[0], soc[0], sri[0], Qbc, 0, d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,5,3,6,7]), fmt='%s')
    else: 
        huh = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,5,3,6,7], 
                             MC04_Gal_nd((Ut[0], soc[0], sri[0], Q[0], 0, d_w[0], sf[0], H, N, Lsc), (Ln, b0, bs, dxn), [1,2,5,3,6,7]),'prep')
        np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('prep_run')+str(run)+'.txt',huh, fmt='%s')

    print('run', run, ' is finished ')
    
