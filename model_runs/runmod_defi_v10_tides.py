#do simulations for the parameter space

#load models and packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py') #load packages
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ti10.py') #load equilibrium model
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_ats10.py') #load code to do adaptive time stepping
runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Gen2 - mod/Model itself 2/gen2mod_fast_td10.py') #load model - second verison

#duration = np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/Generation 2 model/defi/Tadj_run0_109.txt')

# =============================================================================
# make backgroudn and peak river discharge. Avoid that the peak is lower than 3 times the background
# =============================================================================

Qbc_l = np.repeat(np.array([16,50,100,150,200,350,500,650,800,1000,1211]),11)
Ut_l = np.tile(np.linspace(0.75,1.5,11),11)
Le_l = np.repeat(np.array([250,150,100,75,50,50,50,50,25,25,25]),11)
#==============================================================================

#ilist = [0,1,11,12,22,23,33,34,44,45,55,56,66,67,77,78,88,89,99,100,110,111]

for i in range(121):
    Qp,Qbc = 3611, Qbc_l[i]

    #set up simulation
    #numerical parameters
    N= 10
    Lsc=1000

    #duration of the periods in days. periods = before, during, after
    dura = np.zeros(3)
    dura[0] = 10 #before pulse: always 10 days
    dura[1] = 30

    #length comeback
    if Qbc<40: dura[2] = 750
    elif Qbc<490: dura[2] = 150
    else: dura[2] = 50 

    #time parameters
    #DT = np.concatenate([np.zeros(int(dura[0]))+24,np.zeros(12+48)+1,np.zeros(int((dura[1]-2.5)*24/6))+6,np.zeros(12+48)+1,np.zeros(int(dura[2]-2.5)*2)+12]) #hours
    DT = np.concatenate([np.zeros(10)+24, np.zeros(48)+1 , np.zeros(int(dura[1]-2)*4)+6 , np.zeros(48)+1 , np.zeros(int(dura[2]*2))+12])
    T=len(DT)

    #varying parameter
    Ut = np.zeros(T)+Ut_l[i]#0.75+0.05*j#*1.5**0.5#+0.25*np.sin(4*np.pi/30 * (DT/24*(np.arange(0,T,1)+lag)))
    soc = np.array([35]*T)
    sri = np.array([0]*T)
    sf =2*cv*Ut
    
    #depth, constant in all dimensions
    H=10
    u_w = np.zeros(T)
    d_w =np.zeros(len(u_w))
    
    #geometry - new version as always
    b0 = 1000 #width
    L_sea, mu_sea = 25,np.exp(10)
    L_dom = 75
    L_extra = Le_l[i]
    
    Ln = np.array([L_extra,L_dom,L_sea])*1000
    bs = np.array([b0,b0,b0*mu_sea])
    dxx = 250/2 #I suppose this is enough
    dxn = np.array([dxx*2,dxx,dxx])
    
    '''
    #build tanh pulse
    dur = int(0.5 * 24)#days
    t = np.linspace(-5,5,dur)
    Qh1 = np.tanh(t)*(Qp-Qbc)/2+(Qp-Qbc)/2+Qbc
    Qh =np.concatenate([np.zeros(int(dura[0]))+Qbc,Qh1,np.zeros(48)+Qp,np.zeros(int((dura[1]-2.5)*24/6))+Qp,np.flip(Qh1),np.zeros(48)+Qbc,np.zeros(int(dura[2]-2.5)*2)+Qbc])
    '''
    Qh = np.concatenate([np.zeros(10)+Qbc , np.zeros(48+int(dura[1]-2)*4)+Qp, np.zeros(48+int(dura[2]*2))+Qbc])

    
    nxn = np.array(Ln/dxn+1,dtype=int)
    Q = np.repeat(Qh,np.sum(nxn)).reshape((T,np.sum(nxn)))
    #calculate normalised time step
    dt=(DT*3600*Q[0,0])/(np.sum(Ln)*H*b0)#dt should be DT hours here
    
    #do the real simulation
    run = i #+110
    fold = 'defi_tide2'
    os.mkdir('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run))
    LL = gen_mod_timesim((Ut, soc, sri, Q, u_w, d_w, sf, H, N, Lsc), (Ln, b0, bs, dxn), (T,dt,0.5),  [1,2,5,3,6,7],
                         np.loadtxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/preparation/'+str('prep_run')+str(run)+'.txt'),None)
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_var.txt'),LL[0], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_const.txt'),LL[1], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('par_nd.txt'),LL[2], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('time.txt'),LL[3], fmt='%s')
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('raw_out.txt'),LL[4], fmt='%s') 
    np.savetxt('/Users/biemo004/Documents/UU phd Saltisolutions/Output/new_connect/'+fold+'/'+str('run')+str(run)+'/'
               +str('salt_mm.txt'),LL[5], fmt='%s') 
    
    print('run', run, ' is finished ')
    



