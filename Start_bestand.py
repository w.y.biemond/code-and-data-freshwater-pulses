#start
#Ik dacht dat het misschien handig was om een startbestand te hebben. Hierin staan de packages die je kan gebruiken, een paar standaard operaties, locaties waar ik mn files opsla en vandaan importeer

#standard packages
import numpy as np                      #standard operations       
import matplotlib.pyplot as plt         #plot figures
import pandas as pd                     #import datasets
from scipy import optimize   , interpolate 
import scipy as sp          #do fits
import scipy.io                         #import matlab datasets

import matplotlib.animation as ani       #make animations
#import cv2                               #import images as matrices
import time                              #measure time of operation
import os as os                          #making folders etc
#import statsmodels.api as sm            #some statistical tools
#import simplekml                        #to plot on GE
#from netCDF4 import Dataset             #to import netcdf files
#import geopy.distance as gd             #measure distance between two coordinates

#%% 
#make a nice plot

#plt.figure(figsize = (5,5))
#plt.plot(xDATA,yDATA, label="AGH")
#plt.xlabel('xlabel[]', fontsize=15)
#plt.ylabel('ylabel[]', fontsize=15)
##plt.xlim(0,30000)
##plt.ylim(1.5,3)
#plt.grid(True)
##plt.tick_params(axis='both', which='major', labelsize=12)
##plt.legend(scatterpoints=1000, fontsize=10)
#plt.tight_layout()
#plt.legend()
##plt.savefig(loce + "PLOTNAME.png")
#plt.show()

#make a contourplot

#plt.figure(figsize=(7,8))
#plt.contourf(x,y,DATA)#,levels=(np.linspace(-.5,.5,11)))
#cb = plt.colorbar()
#cb.set_label(label='cblabel[]',fontsize=15)
#plt.xlabel('xlable[]',fontsize=15)
#plt.ylabel('ylabel[]',fontsize=15)
#plt.tight_layout()
##plt.savefig(loce + "PLOTNAME.png")
#plt.show()

#make an animation

#def init():
#    return ax.cla(),
#def animate(i):
#    ax.clear()
#    ax.contourf(x2,y2,np.ma.array(dat2[1,i]-10**7, mask=of2),levels=(np.linspace(-2,2,11)))
#    ax.quiver(x2,y2,u,v)
#    ax.set_title('After 0.07 seconds') 
#    ax.set_xlabel('x [m]')
#    ax.set_ylabel('y [m]')
#    return ax,
#fig = plt.figure(figsize=(5.1,3.5))
#fig.tight_layout()
#ax = fig.add_subplot(111)
#anim = ani.FuncAnimation(fig,animate,len(dat2[1]),init_func=init,blit=False)
#anim.save(loce + "VIDNAME.png", fps=10, extra_args=['-vcodec', 'libx264'])

#make a plot with years on x-axis
#fig, ax = plt.subplots()
#ax.plot(ARt-365,ARtemp[:,0])

# format the ticks
#ax.xaxis.set_major_locator(mdates.YearLocator())
#ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
#ax.xaxis.set_minor_locator(mdates.MonthLocator())

# format the coords message box
#ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
#ax.grid(True)

#fig.autofmt_xdate()
#plt.tight_layout()
#plt.show()

#or with 
def fig_formatter(ax):
    months = mdates.MonthLocator()
    myFmt = mdates.DateFormatter('%B')
    ax.xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()
    ax.xaxis.set_minor_locator(months)

#%%
#other often used actions

#flatten
#flat_list = [item for sublist in lijst for item in sublist]

#fit a function
#def testfunc(t, a, b, c):
#    return a*t + b -c**t
#good initial guess is important
#params, params_covariance = sp.optimize.curve_fit(testfunc, t, data)#  , p0=[0.001, 0, 0])
#print(params)


#find value closest to value in list
#min(myList, key=lambda x:abs(x-myNumber))

#get time from EPOCH format:
#pd.to_datetime(EPOCHtime-719529, unit='D')#.date()
#other way around:
#pd.to_datetime('2014-01-01 00:00:00').value/(10**9*3600*24)+719529

#split a list based on opvolgende numbers
#list_split=[]
#temp=[]
#for i in range(len(list_ori)-1):
#    if list_ori[i+1]- list_ori[i] <6:
#        temp.append(list_ori[i])
#    else:
#        temp.append(list_ori[i])
#        list_split.append(temp)
#        temp=[]
#temp.append(list_ori[-1])
#list_split.append(temp)

##nans verwijderen:
#np.nan_to_num( list_with_nan)

#%%
#set locations of files

loci = r"C:\Users\Bouke\Documents\Master Climate Physics\Thesis\Databestanden" #last backslash?
loce = r"C:\Users\Bouke\Documents\Master Climate Physics\Thesis\Output" 

ds= u'\N{DEGREE SIGN}'

#%%
#measure time
#tijd = time.time()
#ACTION
#print('The duration of this was ',time.time()-tijd, 'seconds')



